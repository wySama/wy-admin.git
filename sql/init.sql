CREATE DATABASE  `admin` ;

USE `admin`;

/*Table structure for table `alarm_log` */

DROP TABLE IF EXISTS `alarm_log`;

CREATE TABLE `alarm_log` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `company_id` int NOT NULL DEFAULT '0' COMMENT '单位ID',
  `equipment_id` int NOT NULL DEFAULT '0' COMMENT '设备ID',
  `alarm_id` int NOT NULL DEFAULT '0' COMMENT '告警的序号',
  `alarm_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '告警时刻',
  `alarm_type` int NOT NULL DEFAULT '0' COMMENT '告警类型（0:撤销告警 1:产生告警）',
  `alarm_content` int NOT NULL DEFAULT '0' COMMENT '告警内容',
  `remark` varchar(60) DEFAULT '' COMMENT '备注',
  `active_flag` char(1) NOT NULL DEFAULT 'Y' COMMENT '是否有效, Y:有效 N:无效',
  `creation_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `created_by` int NOT NULL DEFAULT '0' COMMENT '创建人',
  `modify_date` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` int NOT NULL DEFAULT '0' COMMENT '修改人',
  PRIMARY KEY (`id`),
  KEY `idx_alarm_log_company_id` (`company_id`),
  KEY `idx_alarm_log_equipment_id` (`equipment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb3 COMMENT='告警日志表';

/*Data for the table `alarm_log` */

insert  into `alarm_log`(`id`,`company_id`,`equipment_id`,`alarm_id`,`alarm_date`,`alarm_type`,`alarm_content`,`remark`,`active_flag`,`creation_date`,`created_by`,`modify_date`,`modified_by`) values
(1,1,1,0,'2022-02-28 14:57:09',1,0,'','Y','2022-02-28 14:57:09',1,NULL,0),
(2,1,8,1,'2022-02-28 14:57:09',1,1,'','Y','2022-02-28 14:57:09',1,NULL,0),
(3,1,9,2,'2022-02-28 14:57:09',0,2,'','Y','2022-02-28 14:57:09',1,NULL,0),
(4,1,10,3,'2022-02-28 14:57:09',1,3,'','Y','2022-02-28 14:57:09',1,NULL,0),
(5,1,11,4,'2022-02-28 14:57:09',1,4,'','Y','2022-02-28 14:57:09',1,NULL,0),
(6,1,12,5,'2022-02-28 14:57:09',1,5,'','Y','2022-02-28 14:57:09',1,NULL,0),
(7,1,13,6,'2022-02-28 14:57:09',0,6,'','Y','2022-02-28 14:57:09',1,NULL,0),
(8,1,11,7,'2022-02-28 14:57:09',1,7,'','Y','2022-02-28 14:57:09',1,NULL,0),
(9,1,1,8,'2022-02-28 14:57:09',1,8,'','Y','2022-02-28 14:57:09',1,NULL,0),
(10,1,11,9,'2022-02-28 14:57:09',1,9,'','Y','2022-02-28 14:57:09',1,NULL,0),
(11,1,13,10,'2022-02-28 14:57:09',1,10,'','Y','2022-02-28 14:57:09',1,NULL,0),
(12,1,10,11,'2022-02-28 14:57:09',1,11,'','Y','2022-02-28 14:57:09',1,NULL,0),
(13,1,1,12,'2022-02-28 14:57:09',1,12,'','Y','2022-02-28 14:57:09',1,NULL,0),
(14,1,8,13,'2022-02-28 14:57:09',0,13,'','Y','2022-02-28 14:57:09',1,NULL,0),
(15,1,9,14,'2022-02-28 14:57:09',0,14,'','Y','2022-02-28 14:57:09',1,NULL,0),
(16,2,2,0,'2022-02-28 14:57:09',1,0,'','Y','2022-02-28 14:57:09',1,NULL,0),
(17,2,2,1,'2022-02-28 14:57:09',1,1,'','Y','2022-02-28 14:57:09',1,NULL,0),
(18,2,2,2,'2022-02-28 14:57:09',1,2,'','Y','2022-02-28 14:57:09',1,NULL,0),
(19,2,2,3,'2022-02-28 14:57:09',1,3,'','Y','2022-02-28 14:57:09',1,NULL,0),
(20,2,2,4,'2022-02-28 14:57:09',0,4,'','Y','2022-02-28 14:57:09',1,NULL,0),
(21,2,2,5,'2022-02-28 14:57:09',1,5,'','Y','2022-02-28 14:57:09',1,NULL,0),
(22,2,2,6,'2022-02-28 14:57:09',1,6,'','Y','2022-02-28 14:57:09',1,NULL,0),
(23,2,18,7,'2022-02-28 14:57:09',1,7,'','Y','2022-02-28 14:57:09',1,NULL,0),
(24,2,2,8,'2022-02-28 14:57:09',0,8,'','Y','2022-02-28 14:57:09',1,NULL,0),
(25,2,17,9,'2022-02-28 14:57:09',1,9,'','Y','2022-02-28 14:57:09',1,NULL,0),
(26,2,15,10,'2022-02-28 14:57:09',1,10,'','Y','2022-02-28 14:57:09',1,NULL,0),
(27,2,15,11,'2022-02-28 14:57:09',0,11,'','Y','2022-02-28 14:57:09',1,NULL,0),
(28,2,20,12,'2022-02-28 14:57:09',1,12,'','Y','2022-02-28 14:57:09',1,NULL,0),
(29,2,21,13,'2022-02-28 14:57:09',1,13,'测试测试','Y','2022-02-28 14:57:09',1,'2022-02-28 15:56:46',1),
(30,2,22,14,'2022-02-28 14:57:09',1,14,'测试测试','Y','2022-02-28 14:57:09',1,'2022-02-28 15:56:58',1);

/*Table structure for table `common_dict_data` */

DROP TABLE IF EXISTS `common_dict_data`;

CREATE TABLE `common_dict_data` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `dict_code` varchar(60) NOT NULL DEFAULT '' COMMENT 'code',
  `dict_value` varchar(255) NOT NULL DEFAULT '' COMMENT 'value',
  `dict_type` varchar(60) NOT NULL DEFAULT '' COMMENT '类型',
  `dict_sort` int NOT NULL DEFAULT '0' COMMENT '字典排序',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `active_flag` char(1) NOT NULL DEFAULT 'Y' COMMENT '是否有效, Y:有效 N:无效',
  `creation_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `created_by` int NOT NULL DEFAULT '0' COMMENT '创建人',
  `modify_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改日期',
  `modified_by` int NOT NULL DEFAULT '0' COMMENT '修改人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb3 COMMENT='字典数据表';

/*Data for the table `common_dict_data` */

insert  into `common_dict_data`(`id`,`dict_code`,`dict_value`,`dict_type`,`dict_sort`,`remark`,`active_flag`,`creation_date`,`created_by`,`modify_date`,`modified_by`) values
(1,'1','天线1馈线短路告警','ALARM_CONTENT',1,'注1：开机时即检测到馈线短路不告警，开机后馈线持续短路5分钟，或从正常状态变为开路状态且持续5分钟后发出告警；','Y','2022-02-17 14:48:13',1,'2022-02-17 14:49:28',1),
(2,'2','天线1馈线开路告警','ALARM_CONTENT',2,'注2：开机时即检测到馈线开路不告警，馈线从正常状态变为开路状态且持续5分钟后发出告警；','Y','2022-02-17 14:49:19',1,'2022-02-17 14:49:19',1),
(3,'3','天线1馈线损耗过大告警','ALARM_CONTENT',0,'注3：开机时即检测到馈线损耗过大不告警，开机后馈线损耗过大持续5分钟，或从正常状态变为馈线损耗过大状态且持续5分钟后发出告警；','Y','2022-02-17 14:49:57',1,'2022-02-17 14:49:57',1),
(4,'4','天线1卫星授时模块故障告警','ALARM_CONTENT',4,'','Y','2022-02-17 14:50:19',1,'2022-02-17 14:50:51',1),
(5,'5','天线1接收卫星数量异常','ALARM_CONTENT',5,'注4：开机2小时以后，接收任一卫星导航系统卫星数量低于历史接收最大数量的25％且持续1小时后发出告警','Y','2022-02-17 14:50:44',1,'2022-02-17 14:50:57',1),
(6,'6','天线2馈线短路告警','ALARM_CONTENT',6,'注1：开机时即检测到馈线短路不告警，开机后馈线持续短路5分钟，或从正常状态变为开路状态且持续5分钟后发出告警；','Y','2022-02-17 14:51:37',1,'2022-02-17 14:51:37',1),
(7,'7','天线2馈线开路告警','ALARM_CONTENT',7,'注2：开机时即检测到馈线开路不告警，馈线从正常状态变为开路状态且持续5分钟后发出告警；','Y','2022-02-17 14:51:56',1,'2022-02-17 14:51:56',1),
(8,'8','天线2馈线损耗过大告警','ALARM_CONTENT',8,'注3：开机时即检测到馈线损耗过大不告警，开机后馈线损耗过大持续5分钟，或从正常状态变为馈线损耗过大状态且持续5分钟后发出告警；','Y','2022-02-17 14:52:18',1,'2022-02-17 14:52:18',1),
(9,'9','天线2卫星授时模块故障告警','ALARM_CONTENT',9,'','Y','2022-02-17 14:52:37',1,'2022-02-17 14:52:37',1),
(10,'10','天线2接收卫星数量异常','ALARM_CONTENT',10,'开机2小时以后，接收任一卫星导航系统卫星数量低于历史接收最大数量的25％且持续1小时后发出告警；','Y','2022-02-17 14:52:58',1,'2022-02-17 14:52:58',1),
(11,'11','内置铷原子钟失锁告警','ALARM_CONTENT',11,'注5：原子钟从锁定状态变为失锁状态且持续1分钟后发出告警；','Y','2022-02-17 14:53:19',1,'2022-02-17 14:53:19',1),
(12,'12','干扰告警','ALARM_CONTENT',12,'注6：当前主用接收天线存在干扰且持续20分钟后发出告警；','Y','2022-02-17 14:53:39',1,'2022-02-17 14:53:39',1),
(13,'13','欺骗告警','ALARM_CONTENT',13,'注7：当前主用接收天线存在欺骗且持续10分钟后发出告警；','Y','2022-02-17 14:54:00',1,'2022-02-17 14:54:00',1),
(14,'14','卫星授时中断告警','ALARM_CONTENT',14,'注8：所有卫星接收天线接收信号都不能用于卫星授时，导致卫星授时持续中断2小时后发出告警；','Y','2022-02-17 14:54:18',1,'2022-02-17 14:54:18',1),
(15,'15','外部输入IRIG-B信号失锁','ALARM_CONTENT',15,'注9：外部输入IRIG-B信号从锁定状态变为失锁状态且持续1分钟后发出告警。','Y','2022-02-17 14:54:34',1,'2022-02-17 14:54:34',1),
(16,'SYS_OPER_MAX_NUM','20','COMPANY_MAX_NUM',0,'系统操作员最大数量','Y','2022-02-21 10:29:09',1,'2022-02-21 10:29:09',1),
(17,'COMPANY_MAX_NUM','300','COMPANY_MAX_NUM',0,'用户单位最大数量','Y','2022-02-21 10:29:34',1,'2022-02-21 10:29:34',1),
(18,'COMPANY_ADMIN_MAX_NUM','5','COMPANY_ADMIN_MAX_NUM',0,'每个用户单位用户管理员最大数量','Y','2022-02-21 10:32:37',1,'2022-02-21 10:32:37',1),
(19,'SELECTED_QUERY_FREQUENCY','10','SELECTED_QUERY_FREQUENCY',0,'选中设备工作状态查询频度','Y','2022-02-21 10:35:03',1,'2022-02-21 10:35:14',1),
(20,'NOT_SELECTED_QUERY_FREQUENCY','300','NOT_SELECTED_QUERY_FREQUENCY',0,'未选中设备查询频度','Y','2022-02-21 10:36:41',1,'2022-02-21 10:36:41',1),
(21,'OFFLINE_ALARM_DURATION','30','OFFLINE_ALARM_DURATION',0,'离线告警持续时间','Y','2022-02-21 10:40:32',1,'2022-02-21 10:40:32',1);

/*Table structure for table `common_dict_type` */

DROP TABLE IF EXISTS `common_dict_type`;

CREATE TABLE `common_dict_type` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `dict_name` varchar(60) NOT NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(60) NOT NULL DEFAULT '' COMMENT '字典类型',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `active_flag` char(1) NOT NULL DEFAULT 'Y' COMMENT '是否有效, Y:有效 N:无效',
  `creation_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `created_by` int NOT NULL DEFAULT '0' COMMENT '创建人',
  `modify_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改日期',
  `modified_by` int NOT NULL DEFAULT '0' COMMENT '修改人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3 COMMENT='字典类型表';

/*Data for the table `common_dict_type` */

insert  into `common_dict_type`(`id`,`dict_name`,`dict_type`,`remark`,`active_flag`,`creation_date`,`created_by`,`modify_date`,`modified_by`) values
(1,'告警内容','ALARM_CONTENT','告警内容','Y','2022-02-17 14:46:52',0,'2022-02-21 10:23:30',0),
(2,'系统操作员最大数量','SYS_OPER_MAX_NUM','系统操作员最大数量','Y','2022-02-21 10:18:08',0,'2022-02-21 10:23:38',0),
(3,'单位最大数量','COMPANY_MAX_NUM','用户单位最大数量','Y','2022-02-21 10:21:44',0,'2022-02-21 10:31:57',0),
(4,'单位用户管理员最大数量','COMPANY_ADMIN_MAX_NUM','每个用户单位用户管理员最大数量','Y','2022-02-21 10:31:08',0,'2022-02-21 10:31:08',0),
(5,'选中设备工作状态查询频度','SELECTED_QUERY_FREQUENCY','选中设备工作状态查询频度','Y','2022-02-21 10:34:47',0,'2022-02-21 10:34:47',0),
(6,'未选中设备查询频度','NOT_SELECTED_QUERY_FREQUENCY','未选中设备查询频度','Y','2022-02-21 10:36:09',0,'2022-02-21 10:36:09',0),
(7,'离线告警持续时间','OFFLINE_ALARM_DURATION','离线告警持续时间','Y','2022-02-21 10:40:10',0,'2022-02-21 10:40:10',0);

/*Table structure for table `company` */

DROP TABLE IF EXISTS `company`;

CREATE TABLE `company` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `company_name` varchar(255) NOT NULL DEFAULT '' COMMENT '单位名称',
  `company_abbreviation` varchar(64) NOT NULL DEFAULT '' COMMENT '单位简称',
  `leader_name` varchar(64) NOT NULL DEFAULT '' COMMENT '负责人名称',
  `leader_phone` varchar(64) NOT NULL DEFAULT '' COMMENT '负责人手机号',
  `address` varchar(255) NOT NULL DEFAULT '' COMMENT '单位地址',
  `remark` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '备注',
  `active_flag` char(1) NOT NULL DEFAULT 'Y' COMMENT '是否有效, Y:有效 N:无效',
  `creation_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `created_by` int NOT NULL DEFAULT '0' COMMENT '创建人',
  `modify_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改日期',
  `modified_by` int NOT NULL DEFAULT '0' COMMENT '修改人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COMMENT='单位表';

/*Data for the table `company` */

insert  into `company`(`id`,`company_name`,`company_abbreviation`,`leader_name`,`leader_phone`,`address`,`remark`,`active_flag`,`creation_date`,`created_by`,`modify_date`,`modified_by`) values
(1,'南京科技有限公司','南京科技','Ting','18156275488','江苏省南京市','测试','Y','2022-02-16 16:33:25',1,'2022-02-16 16:39:51',1),
(2,'上海智能股份有限公司','上海智能','方丈','18156278888','江苏省南京市','测试','Y','2022-02-16 16:40:58',1,'2022-02-16 16:40:58',1),
(4,'人工智能公司','人工智能','李刚','18156275898','北京市','备注备注备注22','Y','2022-02-18 09:59:20',1,'2022-02-18 16:34:01',1);

/*Table structure for table `equipment` */

DROP TABLE IF EXISTS `equipment`;

CREATE TABLE `equipment` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `company_id` int NOT NULL DEFAULT '0' COMMENT '单位ID',
  `factory_info` varchar(255) DEFAULT '' COMMENT '服务器生产厂家名称（最大80）',
  `equipment_model` varchar(60) DEFAULT '' COMMENT '服务器型号（最大20字节）',
  `equipment_name` varchar(60) DEFAULT '' COMMENT '服务器设备名称（最大40字节）',
  `equipment_number` varchar(60) DEFAULT '' COMMENT '服务器设备编号（最大20字节）',
  `ex_factory_date` date DEFAULT NULL COMMENT '出厂日期格式：YYYY-MM-DD（如2021-01-07）。',
  `ip_address` varchar(60) DEFAULT '' COMMENT 'IP地址',
  `subnet_mask` varchar(60) DEFAULT '' COMMENT '子网掩码',
  `default_gateway` varchar(60) DEFAULT '' COMMENT '默认网关',
  `port_number` varchar(60) DEFAULT '' COMMENT '端口号',
  `installation_date` date DEFAULT NULL COMMENT '安装日期',
  `installation_info` varchar(255) DEFAULT '' COMMENT '安装详情',
  `remark` varchar(100) DEFAULT '' COMMENT '备注',
  `active_flag` char(1) NOT NULL DEFAULT 'Y' COMMENT '是否有效, Y:有效 N:无效',
  `creation_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `created_by` int NOT NULL DEFAULT '0' COMMENT '创建人',
  `modify_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改日期',
  `modified_by` int NOT NULL DEFAULT '0' COMMENT '修改人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb3 COMMENT='设备信息表';

/*Data for the table `equipment` */

insert  into `equipment`(`id`,`company_id`,`factory_info`,`equipment_model`,`equipment_name`,`equipment_number`,`ex_factory_date`,`ip_address`,`subnet_mask`,`default_gateway`,`port_number`,`installation_date`,`installation_info`,`remark`,`active_flag`,`creation_date`,`created_by`,`modify_date`,`modified_by`) values
(1,1,'北京硬件设备公司','GU455551','卫星监测AU-s','AT-65465115','2022-02-01','192.0.0.1','33.545.351.5.2','33.545.351.5.2','8002','2022-02-01','安装详情安装详情安装详情安装详情安装详情','备注备注备注备注备注','Y','2022-02-17 10:41:45',1,'2022-02-17 11:29:33',1),
(2,2,'北京硬件设备公司','GU455552','卫星监测AU-a','AT-65465116','2022-02-02','192.0.0.2','33.545.351.5.2','33.545.351.5.2','8002','2022-02-17','安装详情安装详情安装详情安装详情安装详情',' 备注备注备注备注备注','Y','2022-02-17 11:59:08',1,'2022-02-22 10:03:36',1),
(3,2,'南京硬件设备公司','GU455553','卫星监测AU-b','AT-65465117','2022-02-02','192.0.0.3','33.545.351.5.2','33.545.351.5.2','8002','2022-02-17','安装详情安装详情安装详情安装详情安装详情',' 备注备注备注备注备注','Y','2022-02-17 11:59:08',1,'2022-02-22 10:03:40',1),
(4,4,'苏州硬件设备公司','GU455554','卫星监测AU-c','AT-65465118','2022-02-02','192.0.0.4','33.545.351.5.2','33.545.351.5.2','8002','2022-02-17','安装详情安装详情安装详情安装详情安装详情',' 备注备注备注备注备注','Y','2022-02-17 11:59:08',1,'2022-02-22 10:03:45',1),
(5,4,'上海硬件设备公司','GU455553','卫星监测AU-d','AT-65465119','2022-02-02','192.0.0.5','33.545.351.5.2','33.545.351.5.2','8002','2022-02-17','安装详情安装详情安装详情安装详情安装详情',' 备注备注备注备注备注','Y','2022-02-17 11:59:08',1,'2022-02-22 10:03:47',1),
(6,4,'广州硬件设备公司','GU455554','卫星监测AU-e','AT-65465110','2022-02-02','192.0.0.6','33.545.351.5.2','33.545.351.5.2','8002','2022-02-17','安装详情安装详情安装详情安装详情安装详情',' 备注备注备注备注备注','Y','2022-02-17 11:59:08',1,'2022-02-22 10:03:50',1),
(7,4,'南通硬件设备公司','GU455553','卫星监测AU-f','AT-65465117','2022-02-02','192.0.0.7','33.545.351.5.2','33.545.351.5.2','8002','2022-02-17','安装详情安装详情安装详情安装详情安装详情',' 备注备注备注备注备注','Y','2022-02-17 11:59:08',1,'2022-02-22 10:03:53',1),
(8,1,'海南硬件设备公司','GU455554','卫星监测AU-g','AT-65465128','2022-02-02','192.0.0.8','33.545.351.5.2','33.545.351.5.2','8002','2022-02-17','安装详情安装详情安装详情安装详情安装详情',' 备注备注备注备注备注','Y','2022-02-17 11:59:08',1,'2022-02-22 10:03:56',1),
(9,1,'广州硬件设备公司','GU455553','卫星监测AU-h','AT-65465127','2022-02-02','192.0.0.9','33.545.351.5.2','33.545.351.5.2','8002','2022-02-17','安装详情安装详情安装详情安装详情安装详情',' 备注备注备注备注备注','Y','2022-02-17 11:59:08',1,'2022-02-22 10:03:58',1),
(10,1,'合肥硬件设备公司','GU455554','卫星监测AU-i','AT-65465138','2022-02-02','192.0.0.10','33.545.351.5.2','33.545.351.5.2','8002','2022-02-17','安装详情安装详情安装详情安装详情安装详情',' 备注备注备注备注备注','Y','2022-02-17 11:59:08',1,'2022-02-22 10:04:00',1),
(11,1,'大连硬件设备公司','GU455553','卫星监测AU-g','AT-65465147','2022-02-02','192.0.0.11','33.545.351.5.2','33.545.351.5.2','8002','2022-02-17','安装详情安装详情安装详情安装详情安装详情',' 备注备注备注备注备注','Y','2022-02-17 11:59:08',1,'2022-02-22 10:04:02',1),
(12,1,'武汉硬件设备公司','GU455554','卫星监测AU-k','AT-65465148','2022-02-02','192.0.0.12','33.545.351.5.2','33.545.351.5.2','8002','2022-02-17','安装详情安装详情安装详情安装详情安装详情',' 备注备注备注备注备注','Y','2022-02-17 11:59:08',1,'2022-02-22 10:04:03',1),
(13,1,'益阳硬件设备公司','GU455553','卫星监测AU-l','AT-65465151','2022-02-02','192.0.0.13','33.545.351.5.2','33.545.351.5.2','8002','2022-02-17','安装详情安装详情安装详情安装详情安装详情',' 备注备注备注备注备注','Y','2022-02-17 11:59:08',1,'2022-02-22 10:04:10',1),
(14,2,'尚志硬件设备公司','GU455554','卫星监测AU-m','AT-65465152','2022-02-02','192.0.0.14','33.545.351.5.2','33.545.351.5.2','8002','2022-02-17','安装详情安装详情安装详情安装详情安装详情',' 备注备注备注备注备注','Y','2022-02-17 11:59:08',1,'2022-02-22 10:04:13',1),
(15,2,'南平硬件设备公司','GU455553','卫星监测AU-n','AT-65465153','2022-02-02','192.0.0.15','33.545.351.5.2','33.545.351.5.2','8002','2022-02-17','安装详情安装详情安装详情安装详情安装详情',' 备注备注备注备注备注','Y','2022-02-17 11:59:08',1,'2022-02-22 10:04:15',1),
(16,2,'天长硬件设备公司','GU455554','卫星监测AU-c','AT-65465163','2022-02-02','192.0.0.16','33.545.351.5.2','33.545.351.5.2','8002','2022-02-17','安装详情安装详情安装详情安装详情安装详情',' 备注备注备注备注备注','Y','2022-02-17 11:59:08',1,'2022-02-22 10:04:17',1),
(17,2,'宣城硬件设备公司','GU455553','卫星监测AU-o','AT-65465164','2022-02-02','192.0.0.17','33.545.351.5.2','33.545.351.5.2','8002','2022-02-17','安装详情安装详情安装详情安装详情安装详情',' 备注备注备注备注备注','Y','2022-02-17 11:59:08',1,'2022-02-22 10:04:19',1),
(18,2,'芜湖硬件设备公司','GU455554','卫星监测AU-p','AT-65465165','2022-02-02','192.0.0.18','33.545.351.5.2','33.545.351.5.2','8002','2022-02-17','安装详情安装详情安装详情安装详情安装详情',' 备注备注备注备注备注','Y','2022-02-17 11:59:08',1,'2022-02-22 10:04:21',1),
(19,2,'亳州硬件设备公司','GU455553','卫星监测AU-q','AT-65465166','2022-02-02','192.0.0.19','33.545.351.5.2','33.545.351.5.2','8002','2022-02-17','安装详情安装详情安装详情安装详情安装详情',' 备注备注备注备注备注','Y','2022-02-17 11:59:08',1,'2022-02-22 10:04:23',1),
(20,2,'商丘硬件设备公司','GU455554','卫星监测AU-r','AT-65465167','2022-02-02','192.0.0.20','33.545.351.5.2','33.545.351.5.2','8002','2022-02-17','安装详情安装详情安装详情安装详情安装详情',' 备注备注备注备注备注','Y','2022-02-17 11:59:08',1,'2022-02-22 10:04:28',1),
(21,2,'郑州硬件设备公司','GU455553','卫星监测AU-s','AT-65465168','2022-02-02','192.0.0.21','33.545.351.5.2','33.545.351.5.2','8002','2022-02-17','安装详情安装详情安装详情安装详情安装详情',' 备注备注备注备注备注','Y','2022-02-17 11:59:08',1,'2022-02-22 10:04:31',1),
(22,2,'深圳硬件设备公司','GU455554','卫星监测AU-t','AT-65465169','2022-02-02','192.0.0.22','33.545.351.5.2','33.545.351.5.2','8002','2022-02-17','安装详情安装详情安装详情安装详情安装详情',' 备注备注备注备注备注','Y','2022-02-17 11:59:08',1,'2022-02-22 10:04:38',1);

/*Table structure for table `sys_login_log` */

DROP TABLE IF EXISTS `sys_login_log`;

CREATE TABLE `sys_login_log` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `login_name` varchar(64) NOT NULL DEFAULT '' COMMENT '登录名',
  `login_ip` varchar(128) DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) DEFAULT '' COMMENT '操作系统',
  `login_status` tinyint unsigned NOT NULL DEFAULT '1' COMMENT '登录状态：0失败 1成功',
  `msg` varchar(255) DEFAULT '' COMMENT '提示消息',
  `login_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '访问时间',
  `remark` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8mb3 COMMENT='登录日志表';

/*Data for the table `sys_login_log` */

insert  into `sys_login_log`(`id`,`login_name`,`login_ip`,`login_location`,`browser`,`os`,`login_status`,`msg`,`login_time`,`remark`) values
(3,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-18 10:55:48',''),
(4,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-18 11:31:22',''),
(5,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-18 11:33:29',''),
(6,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-18 11:42:17',''),
(7,'wy1','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',0,'账号或密码错误','2022-02-18 13:45:06',''),
(8,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-18 13:45:11',''),
(9,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-18 16:25:43',''),
(10,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-18 17:52:13',''),
(11,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-21 09:34:25',''),
(12,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-21 12:01:23',''),
(13,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-21 13:32:01',''),
(14,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-22 09:41:50',''),
(15,'wy','127.0.0.1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-22 15:35:22',''),
(16,'CC','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-22 18:32:18',''),
(17,'CC','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-22 18:33:13',''),
(18,'CC','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-22 18:34:18',''),
(19,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-23 09:50:29',''),
(20,'CC','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-23 10:45:37',''),
(21,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-23 13:35:44',''),
(22,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-23 13:36:51',''),
(23,'CC','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-23 13:37:07',''),
(24,'CC','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-23 14:55:34',''),
(25,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-23 14:56:04',''),
(26,'CC','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-23 16:45:28',''),
(27,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-23 18:19:00',''),
(28,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 11:56:10',''),
(29,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 13:42:59',''),
(30,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 15:03:54',''),
(31,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 15:04:22',''),
(32,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 15:07:15',''),
(33,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 15:07:18',''),
(34,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 15:07:21',''),
(35,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 15:10:42',''),
(36,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 15:11:06',''),
(37,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 15:15:23',''),
(38,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 15:16:22',''),
(39,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 15:31:04',''),
(40,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 15:31:42',''),
(41,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 16:13:01',''),
(42,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 16:13:30',''),
(43,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 16:22:47',''),
(44,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 16:27:41',''),
(45,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 16:40:31',''),
(46,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 16:40:34',''),
(47,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 16:42:54',''),
(48,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 16:42:56',''),
(49,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 16:43:21',''),
(50,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 16:46:08',''),
(51,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 16:52:48',''),
(52,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 16:53:13',''),
(53,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 16:54:45',''),
(54,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 16:55:16',''),
(55,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 16:55:52',''),
(56,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 16:55:57',''),
(57,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 16:56:09',''),
(58,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 16:57:30',''),
(59,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 16:58:51',''),
(60,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 16:59:09',''),
(61,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 16:59:44',''),
(62,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 17:15:38',''),
(63,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 17:16:03',''),
(64,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 17:17:59',''),
(65,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 17:18:31',''),
(66,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 17:19:48',''),
(67,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 17:23:24',''),
(68,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 17:25:19',''),
(69,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 17:26:32',''),
(70,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 17:28:22',''),
(71,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 17:37:06',''),
(72,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 17:37:34',''),
(73,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 17:37:41',''),
(74,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 17:37:45',''),
(75,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 17:37:58',''),
(76,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 17:42:26',''),
(77,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 17:42:46',''),
(78,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 17:42:58',''),
(79,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 17:43:21',''),
(80,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 17:43:45',''),
(81,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 18:05:26',''),
(82,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 18:05:41',''),
(83,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 18:06:25',''),
(84,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 18:08:03',''),
(85,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 18:08:06',''),
(86,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 18:08:26',''),
(87,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 18:08:28',''),
(88,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 18:08:31',''),
(89,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 18:10:13',''),
(90,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 18:10:15',''),
(91,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 18:10:31',''),
(92,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 18:10:42',''),
(93,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 18:10:46',''),
(94,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 18:10:48',''),
(95,'CC','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 18:11:03',''),
(96,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 18:11:05',''),
(97,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 18:12:05',''),
(98,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 18:14:59',''),
(99,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 18:26:50',''),
(100,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-24 18:28:04',''),
(101,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-25 15:03:55',''),
(102,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-28 10:29:56',''),
(103,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-02-28 13:54:20',''),
(104,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-03-01 09:40:13',''),
(105,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-03-01 09:40:21',''),
(106,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-03-01 13:34:55',''),
(107,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-03-01 16:35:25',''),
(108,'wy','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-03-01 17:16:46',''),
(109,'wy','127.0.0.1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-03-01 17:26:20',''),
(110,'wy','127.0.0.1','内网IP','Chrome 9','Windows 10',1,'登录成功','2022-03-01 17:45:13','');

/*Table structure for table `sys_menu` */

DROP TABLE IF EXISTS `sys_menu`;

CREATE TABLE `sys_menu` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `parent_id` int unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
  `menu_name` varchar(20) NOT NULL DEFAULT '' COMMENT '名称',
  `menu_type` tinyint unsigned NOT NULL DEFAULT '0' COMMENT '类型(1目录 2菜单 3按钮)',
  `sort` tinyint unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `url` varchar(100) DEFAULT '' COMMENT '路由地址',
  `perms` varchar(50) DEFAULT '' COMMENT '权限标识',
  `icon` varchar(50) DEFAULT '' COMMENT '图标',
  `remark` varchar(100) DEFAULT '' COMMENT '备注',
  `active_flag` char(1) NOT NULL DEFAULT 'Y' COMMENT '是否有效, Y:有效 N:无效',
  `creation_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `created_by` int NOT NULL DEFAULT '0' COMMENT '创建人',
  `modify_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改日期',
  `modified_by` int NOT NULL DEFAULT '0' COMMENT '修改人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb3 COMMENT='菜单表';

/*Data for the table `sys_menu` */

insert  into `sys_menu`(`id`,`parent_id`,`menu_name`,`menu_type`,`sort`,`url`,`perms`,`icon`,`remark`,`active_flag`,`creation_date`,`created_by`,`modify_date`,`modified_by`) values
(1,0,'系统管理',1,1,'#','-','#icon-admin-shezhi1','','Y','2022-02-11 11:17:31',1,'2022-02-15 11:36:00',1),
(2,1,'用户管理',2,1,'/sys/user/index','sys:user:index','#icon-admin-group','','Y','2022-02-14 18:35:08',1,'2022-02-15 13:35:20',1),
(3,1,'角色管理',2,2,'/sys/role/index','sys:role:index','#icon-admin-jiaoseguanli','','Y','2022-02-15 09:49:14',1,'2022-02-15 13:39:09',1),
(4,1,'菜单管理',2,3,'/sys/menu/index','sys:menu:index','#icon-admin-menu','','Y','2022-02-15 09:50:07',1,'2022-02-15 13:36:30',1),
(5,2,'用户查询',3,1,'/api/sys/querySysUserPage','sys:user:list','','','Y','2022-02-15 14:45:47',1,'2022-02-15 14:48:28',1),
(6,2,'用户新增',3,2,'/api/sys/saveSysUser','sys:user:add','','','Y','2022-02-15 14:49:53',1,'2022-02-15 14:51:29',1),
(7,2,' 用户修改',3,3,'/api/sys/updateSysUser','sys:user:edit','','','Y','2022-02-15 15:11:37',1,'2022-02-15 15:11:37',1),
(8,2,'用户删除',3,4,'api/sys/deleteSysMenu','sys:user:remove','','','Y','2022-02-15 15:51:20',1,'2022-02-15 15:51:20',1),
(9,3,'角色查询',3,1,'/api/sys/querySysRolePage','sys:role:list','','','Y','2022-02-15 16:37:44',1,'2022-02-15 16:43:12',1),
(10,3,'角色新增',3,2,'/api/sys/saveSysRole','sys:role:add','','','Y','2022-02-15 16:40:25',1,'2022-02-15 16:40:25',1),
(11,3,' 角色修改',3,3,'/api/sys/updateSysMenu','sys:role:edit','','','Y','2022-02-15 16:42:49',1,'2022-02-15 16:42:49',1),
(12,3,'角色删除',3,4,'api/sys/deleteSysRole','sys:role:remove','','','Y','2022-02-15 16:45:19',1,'2022-02-15 16:45:19',1),
(13,4,'菜单查询',3,1,'/api/sys/getSysMenuDTOS','sys:menu:list','','','Y','2022-02-15 16:49:13',1,'2022-02-15 16:49:13',1),
(14,4,'菜单新增',3,2,'/api/sys/saveSysMenu','sys:menu:add','','','Y','2022-02-15 16:49:53',1,'2022-02-15 16:49:53',1),
(15,4,'菜单修改',3,3,'/api/sys/updateSysMenu','sys:menu:edit','','','Y','2022-02-15 16:51:26',1,'2022-02-15 16:51:26',1),
(16,4,'菜单删除',3,4,'/api/sys/deleteSysMenu','sys:menu:remove','','','Y','2022-02-15 16:52:44',1,'2022-02-15 16:52:44',1),
(17,2,'重置密码',3,5,'/api/sys/resetPwdByRandom','sys:user:resetPwdByRandom','','','Y','2022-02-16 14:28:10',1,'2022-02-16 14:28:10',1),
(18,0,'业务管理',1,2,'#','-','#icon-admin-yewuguanli','','Y','2022-02-16 15:59:38',1,'2022-02-16 16:06:28',1),
(19,18,'单位管理',2,1,'/business/company/index','business:company:index','#icon-admin-icon_danwei','','Y','2022-02-16 16:02:16',1,'2022-02-17 18:12:29',1),
(20,19,'单位查询',3,1,'/api/company/queryCompanyPage','business:company:list','','','Y','2022-02-16 16:45:22',1,'2022-02-16 16:45:22',1),
(21,19,'单位新增',3,2,'/api/company/saveCompany','business:company:add','','','Y','2022-02-16 16:46:03',1,'2022-02-16 16:46:03',1),
(22,19,'单位修改',3,3,'/api/company/updateCompany','business:company:edit','','','Y','2022-02-16 16:46:58',1,'2022-02-16 16:46:58',1),
(23,19,'单位删除',3,4,'/api/company/deleteCompany','business:company:remove','','','Y','2022-02-16 16:47:56',1,'2022-02-17 09:45:18',1),
(24,18,'设备管理',2,2,'/business/equipment/index','business:equipment:index','#icon-admin-shebeiguanli','','Y','2022-02-17 09:35:28',1,'2022-02-17 12:03:52',1),
(25,24,'设备查询',3,1,'/api/equipment/queryEquipmentPage','business:equipment:list','','','Y','2022-02-17 09:41:18',1,'2022-02-17 09:41:18',1),
(26,24,'设备新增',3,2,'/api/equipment/saveEquipment','business:equipment:add','','','Y','2022-02-17 09:43:53',1,'2022-02-17 09:43:53',1),
(27,24,'设备修改',3,3,'/api/equipment/updateEquipment','business:equipment:edit','','','Y','2022-02-17 09:44:50',1,'2022-02-17 09:44:50',1),
(28,24,'设备删除',3,4,'/api/equipment/deleteEquipment','business:equipment:remove','','','Y','2022-02-17 09:45:53',1,'2022-02-17 09:45:53',1),
(29,0,'工具管理',1,3,'#','-','#icon-admin-gongju','','Y','2022-02-17 14:24:05',1,'2022-02-17 14:35:19',1),
(30,29,'字典管理',2,1,'/tool/dict/index','tool:dict:index','#icon-admin-zidianguanli','','Y','2022-02-17 14:28:14',1,'2022-02-17 14:38:30',1),
(31,30,'字典查询',3,1,'/api/tool/queryCommonDictDataPage','tool:dict:list','','','Y','2022-02-17 14:30:20',1,'2022-02-17 14:31:53',1),
(32,30,'字典新增',3,2,'/api/tool/saveDictData','tool:dict:add','','','Y','2022-02-17 14:31:38',1,'2022-02-17 14:31:38',1),
(33,30,'字典修改',3,3,'/api/tool/updateDictData','tool:dict:edit','','','Y','2022-02-17 14:32:40',1,'2022-02-17 14:32:40',1),
(34,30,'字典删除',3,4,'/api/tool/deleteDictData','tool:dict:remove','','','Y','2022-02-17 14:33:12',1,'2022-02-17 14:33:12',1),
(35,0,'日志管理',1,6,'#','-','#icon-admin-rizhiguanli','','Y','2022-02-17 17:32:37',1,'2022-02-17 17:57:46',1),
(36,35,'登录日志',2,1,'/log/loginlog/index','log:loginlog:list','#icon-admin-denglurizhi','','Y','2022-02-17 17:34:06',1,'2022-02-17 17:57:57',1),
(37,35,'操作日志',2,2,'/log/operlog/index','log:operlog:list','#icon-admin-caozuorizhi','','Y','2022-02-17 18:15:37',1,'2022-02-18 16:26:39',1),
(38,36,'日志删除',3,2,'/api/log/batchRemove','log:loginlog:remove','','','Y','2022-02-18 10:40:31',1,'2022-02-18 10:40:31',1),
(39,18,'工作状态',2,3,'/business/working/index','business:working:index','#icon-admin-xitongzhuangtai','','Y','2022-02-21 18:24:04',1,'2022-02-28 10:35:24',1),
(40,35,'告警日志',2,3,'/log/alarmlog/index','log:alarmlog:list','#icon-admin-yidongyunkongzhitaiicon69','','Y','2022-02-28 10:35:02',1,'2022-02-28 10:35:30',1),
(41,40,'导出',3,1,'/api/log/exportAlarmLog','business:alarmlog:export','','','Y','2022-03-01 17:40:28',1,'2022-03-01 17:40:37',1),
(42,19,'导出',3,5,'/api/company/exportCompany','business:company:export','','','Y','2022-03-01 17:41:52',1,'2022-03-01 17:41:52',1),
(43,24,'导出',3,5,'/api/equipment/exportEquipment','business:equipment:export','','','Y','2022-03-01 17:42:45',1,'2022-03-01 17:42:45',1);

/*Table structure for table `sys_oper_log` */

DROP TABLE IF EXISTS `sys_oper_log`;

CREATE TABLE `sys_oper_log` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `oper_id` int unsigned NOT NULL COMMENT '操作人员ID',
  `oper_name` varchar(50) DEFAULT '' COMMENT '操作人员名称',
  `company_id` int NOT NULL DEFAULT '0' COMMENT '单位ID',
  `title` varchar(50) DEFAULT '' COMMENT '模块标题',
  `business_type` int DEFAULT '0' COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `oper_status` int DEFAULT '0' COMMENT '操作状态（0正常 1异常）',
  `msg` varchar(255) DEFAULT '' COMMENT '提示消息',
  `oper_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '操作时间',
  `oper_ip` varchar(128) DEFAULT '' COMMENT 'IP地址',
  `oper_location` varchar(255) DEFAULT '' COMMENT '地点',
  `browser` varchar(50) DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) DEFAULT '' COMMENT '操作系统',
  `request` varchar(1000) DEFAULT '' COMMENT '请求参数',
  `response` varchar(1000) DEFAULT '' COMMENT '返回参数',
  PRIMARY KEY (`id`),
  KEY `idx_sys_oper_log_oper_id` (`oper_id`),
  KEY `idx_sys_oper_log_company_id` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb3 COMMENT='操作日志表';

/*Data for the table `sys_oper_log` */

insert  into `sys_oper_log`(`id`,`oper_id`,`oper_name`,`company_id`,`title`,`business_type`,`oper_status`,`msg`,`oper_date`,`oper_ip`,`oper_location`,`browser`,`os`,`request`,`response`) values
(1,1,'wy',0,'单位管理',2,0,'','2022-02-18 16:28:18','0:0:0:0:0:0:0:1','','','','{\"activeFlag\":\"Y\",\"address\":\"北京市\",\"companyAbbreviation\":\"人工智能\",\"companyName\":\"人工智能公司\",\"id\":4,\"leaderName\":\"李刚\",\"leaderPhone\":\"18156275898\",\"remark\":\"备注备注备注\",\"userId\":1}','{\"code\":200,\"data\":{\"activeFlag\":\"Y\",\"address\":\"北京市\",\"companyAbbreviation\":\"人工智能\",\"companyName\":\"人工智能公司\",\"id\":4,\"leaderName\":\"李刚\",\"leaderPhone\":\"18156275898\",\"modifiedBy\":1,\"remark\":\"备注备注备注\"},\"msg\":\"success\"}'),
(2,1,'wy',0,'单位管理',2,0,'','2022-02-18 16:35:01','0:0:0:0:0:0:0:1','','','','{\"activeFlag\":\"Y\",\"address\":\"北京市\",\"companyAbbreviation\":\"人工智能\",\"companyName\":\"人工智能公司\",\"id\":4,\"leaderName\":\"李刚\",\"leaderPhone\":\"18156275898\",\"remark\":\"备注备注备注22\",\"userId\":1}','{\"code\":200,\"data\":{\"activeFlag\":\"Y\",\"address\":\"北京市\",\"companyAbbreviation\":\"人工智能\",\"companyName\":\"人工智能公司\",\"id\":4,\"leaderName\":\"李刚\",\"leaderPhone\":\"18156275898\",\"modifiedBy\":1,\"remark\":\"备注备注备注22\"},\"msg\":\"success\"}'),
(3,1,'wy',0,'用户管理',2,0,'SUCCESS','2022-02-18 17:54:29','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"address\":\"南京\",\"avatar\":\"\",\"city\":\"\",\"creationDate\":\"2022-02-15 17:04:14\",\"district\":\"\",\"id\":2,\"loginName\":\"test\",\"nickname\":\"\",\"phone\":\"18156275752\",\"province\":\"\",\"roleIds\":[4],\"sex\":1,\"userName\":\"test\"}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(4,1,'wy',0,'角色管理',1,0,'SUCCESS','2022-02-18 18:00:57','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"menuIds\":[20,25],\"remark\":\"查看\",\"roleCode\":\"query-admin\",\"roleName\":\"查看\",\"userId\":1}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(5,1,'wy',0,'字典类型',1,0,'SUCCESS','2022-02-21 10:18:08','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"dictName\":\"sys_oper_max_num\",\"dictType\":\"sys_oper_max_num\",\"remark\":\"系统操作员最大数量\"}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(6,1,'wy',0,'字典类型',1,0,'SUCCESS','2022-02-21 10:21:44','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"dictName\":\"COMPANY_MAX_NUM\",\"dictType\":\"COMPANY_MAX_NUM\",\"remark\":\"用户单位最大数量\"}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(7,1,'wy',0,'数据字典',1,0,'SUCCESS','2022-02-21 10:29:09','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"dictCode\":\"SYS_OPER_MAX_NUM\",\"dictSort\":0,\"dictType\":\"COMPANY_MAX_NUM\",\"dictValue\":\"20\",\"remark\":\"系统操作员最大数量\",\"userId\":1}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(8,1,'wy',0,'数据字典',1,0,'SUCCESS','2022-02-21 10:29:34','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"dictCode\":\"COMPANY_MAX_NUM\",\"dictSort\":0,\"dictType\":\"COMPANY_MAX_NUM\",\"dictValue\":\"300\",\"remark\":\"用户单位最大数量\",\"userId\":1}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(9,1,'wy',0,'字典类型',1,0,'SUCCESS','2022-02-21 10:31:08','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"dictName\":\"单位用户管理员最大数量\",\"dictType\":\"COMPANY_ADMIN_MAX_NUM\",\"remark\":\"每个用户单位用户管理员最大数量\"}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(10,1,'wy',0,'数据字典',1,0,'SUCCESS','2022-02-21 10:32:37','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"dictCode\":\"COMPANY_ADMIN_MAX_NUM\",\"dictSort\":0,\"dictType\":\"COMPANY_ADMIN_MAX_NUM\",\"dictValue\":\"5\",\"remark\":\"每个用户单位用户管理员最大数量\",\"userId\":1}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(11,1,'wy',0,'字典类型',1,0,'SUCCESS','2022-02-21 10:34:47','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"dictName\":\"选中设备工作状态查询频度\",\"dictType\":\"SELECTED_QUERY_FREQUENCY\",\"remark\":\"选中设备工作状态查询频度\"}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(12,1,'wy',0,'数据字典',1,0,'SUCCESS','2022-02-21 10:35:03','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"dictCode\":\"SELECTED_QUERY_FREQUENCY\",\"dictSort\":0,\"dictType\":\"SELECTED_QUERY_FREQUENCY\",\"dictValue\":\"5\",\"remark\":\"选中设备工作状态查询频度\",\"userId\":1}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(13,1,'wy',0,'数据字典',2,0,'SUCCESS','2022-02-21 10:35:14','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"dictCode\":\"SELECTED_QUERY_FREQUENCY\",\"dictSort\":0,\"dictType\":\"SELECTED_QUERY_FREQUENCY\",\"dictValue\":\"10\",\"id\":19,\"remark\":\"选中设备工作状态查询频度\",\"userId\":1}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(14,1,'wy',0,'字典类型',1,0,'SUCCESS','2022-02-21 10:36:10','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"dictName\":\"未选中设备查询频度\",\"dictType\":\"NOT_SELECTED_QUERY_FREQUENCY\",\"remark\":\"未选中设备查询频度\"}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(15,1,'wy',0,'数据字典',1,0,'SUCCESS','2022-02-21 10:36:41','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"dictCode\":\"NOT_SELECTED_QUERY_FREQUENCY\",\"dictSort\":0,\"dictType\":\"NOT_SELECTED_QUERY_FREQUENCY\",\"dictValue\":\"300\",\"remark\":\"未选中设备查询频度\",\"userId\":1}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(16,1,'wy',0,'字典类型',1,0,'SUCCESS','2022-02-21 10:40:10','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"dictName\":\"离线告警持续时间\",\"dictType\":\"OFFLINE_ALARM_DURATION\",\"remark\":\"离线告警持续时间\"}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(17,1,'wy',0,'数据字典',1,0,'SUCCESS','2022-02-21 10:40:32','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"dictCode\":\"OFFLINE_ALARM_DURATION\",\"dictSort\":0,\"dictType\":\"OFFLINE_ALARM_DURATION\",\"dictValue\":\"30\",\"remark\":\"离线告警持续时间\",\"userId\":1}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(18,1,'wy',0,'菜单管理',1,0,'SUCCESS','2022-02-21 18:24:04','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"icon\":\"\",\"menuName\":\"工作状态\",\"menuType\":2,\"parentId\":18,\"perms\":\"\",\"sort\":3,\"url\":\"/business/working/index\",\"userId\":1}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(19,1,'wy',0,'菜单管理',2,0,'SUCCESS','2022-02-21 18:24:35','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"icon\":\"\",\"id\":39,\"menuName\":\"工作状态\",\"menuType\":2,\"parentId\":18,\"parentMenuName\":\"业务管理\",\"perms\":\"business:working:index\",\"remark\":\"\",\"sort\":3,\"url\":\"/business/working/index\",\"userId\":1}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(20,1,'wy',0,'设备管理',2,0,'SUCCESS','2022-02-22 10:02:33','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"companyId\":2,\"defaultGateway\":\"33.545.351.5.2\",\"equipmentModel\":\"GU455553\",\"equipmentName\":\"卫星监测AU-b\",\"equipmentNumber\":\"AT-65465117\",\"exFactoryDate\":\"2022-02-02\",\"factoryInfo\":\"南京硬件设备公司\",\"id\":3,\"installationDate\":\"2022-02-17\",\"installationInfo\":\"安装详情安装详情安装详情安装详情安装详情\",\"ipAddress\":\"192.0.0.1\",\"portNumber\":\"8002\",\"remark\":\" 备注备注备注备注备注\",\"subnetMask\":\"33.545.351.5.2\",\"userId\":1}','{\"code\":200,\"data\":{\"activeFlag\":\"Y\",\"companyId\":2,\"defaultGateway\":\"33.545.351.5.2\",\"equipmentModel\":\"GU455553\",\"equipmentName\":\"卫星监测AU-b\",\"equipmentNumber\":\"AT-65465117\",\"exFactoryDate\":\"2022-02-02\",\"factoryInfo\":\"南京硬件设备公司\",\"id\":3,\"installationDate\":\"2022-02-17\",\"installationInfo\":\"安装详情安装详情安装详情安装详情安装详情\",\"ipAddress\":\"192.0.0.1\",\"modifiedBy\":1,\"portNumber\":\"8002\",\"remark\":\" 备注备注备注备注备注\",\"subnetMask\":\"33.545.351.5.2\"},\"msg\":\"success\"}'),
(21,1,'wy',0,'设备管理',2,0,'SUCCESS','2022-02-22 10:02:38','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"companyId\":4,\"defaultGateway\":\"33.545.351.5.2\",\"equipmentModel\":\"GU455554\",\"equipmentName\":\"卫星监测AU-c\",\"equipmentNumber\":\"AT-65465118\",\"exFactoryDate\":\"2022-02-02\",\"factoryInfo\":\"苏州硬件设备公司\",\"id\":4,\"installationDate\":\"2022-02-17\",\"installationInfo\":\"安装详情安装详情安装详情安装详情安装详情\",\"ipAddress\":\"192.0.0.1\",\"portNumber\":\"8002\",\"remark\":\" 备注备注备注备注备注\",\"subnetMask\":\"33.545.351.5.2\",\"userId\":1}','{\"code\":200,\"data\":{\"activeFlag\":\"Y\",\"companyId\":4,\"defaultGateway\":\"33.545.351.5.2\",\"equipmentModel\":\"GU455554\",\"equipmentName\":\"卫星监测AU-c\",\"equipmentNumber\":\"AT-65465118\",\"exFactoryDate\":\"2022-02-02\",\"factoryInfo\":\"苏州硬件设备公司\",\"id\":4,\"installationDate\":\"2022-02-17\",\"installationInfo\":\"安装详情安装详情安装详情安装详情安装详情\",\"ipAddress\":\"192.0.0.1\",\"modifiedBy\":1,\"portNumber\":\"8002\",\"remark\":\" 备注备注备注备注备注\",\"subnetMask\":\"33.545.351.5.2\"},\"msg\":\"success\"}'),
(22,1,'wy',0,'设备管理',2,0,'SUCCESS','2022-02-22 10:02:43','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"companyId\":4,\"defaultGateway\":\"33.545.351.5.2\",\"equipmentModel\":\"GU455553\",\"equipmentName\":\"卫星监测AU-d\",\"equipmentNumber\":\"AT-65465119\",\"exFactoryDate\":\"2022-02-02\",\"factoryInfo\":\"上海硬件设备公司\",\"id\":5,\"installationDate\":\"2022-02-17\",\"installationInfo\":\"安装详情安装详情安装详情安装详情安装详情\",\"ipAddress\":\"192.0.0.1\",\"portNumber\":\"8002\",\"remark\":\" 备注备注备注备注备注\",\"subnetMask\":\"33.545.351.5.2\",\"userId\":1}','{\"code\":200,\"data\":{\"activeFlag\":\"Y\",\"companyId\":4,\"defaultGateway\":\"33.545.351.5.2\",\"equipmentModel\":\"GU455553\",\"equipmentName\":\"卫星监测AU-d\",\"equipmentNumber\":\"AT-65465119\",\"exFactoryDate\":\"2022-02-02\",\"factoryInfo\":\"上海硬件设备公司\",\"id\":5,\"installationDate\":\"2022-02-17\",\"installationInfo\":\"安装详情安装详情安装详情安装详情安装详情\",\"ipAddress\":\"192.0.0.1\",\"modifiedBy\":1,\"portNumber\":\"8002\",\"remark\":\" 备注备注备注备注备注\",\"subnetMask\":\"33.545.351.5.2\"},\"msg\":\"success\"}'),
(23,1,'wy',0,'设备管理',2,0,'SUCCESS','2022-02-22 10:02:48','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"companyId\":4,\"defaultGateway\":\"33.545.351.5.2\",\"equipmentModel\":\"GU455553\",\"equipmentName\":\"卫星监测AU-f\",\"equipmentNumber\":\"AT-65465117\",\"exFactoryDate\":\"2022-02-02\",\"factoryInfo\":\"南通硬件设备公司\",\"id\":7,\"installationDate\":\"2022-02-17\",\"installationInfo\":\"安装详情安装详情安装详情安装详情安装详情\",\"ipAddress\":\"192.0.0.1\",\"portNumber\":\"8002\",\"remark\":\" 备注备注备注备注备注\",\"subnetMask\":\"33.545.351.5.2\",\"userId\":1}','{\"code\":200,\"data\":{\"activeFlag\":\"Y\",\"companyId\":4,\"defaultGateway\":\"33.545.351.5.2\",\"equipmentModel\":\"GU455553\",\"equipmentName\":\"卫星监测AU-f\",\"equipmentNumber\":\"AT-65465117\",\"exFactoryDate\":\"2022-02-02\",\"factoryInfo\":\"南通硬件设备公司\",\"id\":7,\"installationDate\":\"2022-02-17\",\"installationInfo\":\"安装详情安装详情安装详情安装详情安装详情\",\"ipAddress\":\"192.0.0.1\",\"modifiedBy\":1,\"portNumber\":\"8002\",\"remark\":\" 备注备注备注备注备注\",\"subnetMask\":\"33.545.351.5.2\"},\"msg\":\"success\"}'),
(24,1,'wy',0,'设备管理',2,0,'SUCCESS','2022-02-22 10:02:53','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"companyId\":4,\"defaultGateway\":\"33.545.351.5.2\",\"equipmentModel\":\"GU455554\",\"equipmentName\":\"卫星监测AU-e\",\"equipmentNumber\":\"AT-65465110\",\"exFactoryDate\":\"2022-02-02\",\"factoryInfo\":\"广州硬件设备公司\",\"id\":6,\"installationDate\":\"2022-02-17\",\"installationInfo\":\"安装详情安装详情安装详情安装详情安装详情\",\"ipAddress\":\"192.0.0.1\",\"portNumber\":\"8002\",\"remark\":\" 备注备注备注备注备注\",\"subnetMask\":\"33.545.351.5.2\",\"userId\":1}','{\"code\":200,\"data\":{\"activeFlag\":\"Y\",\"companyId\":4,\"defaultGateway\":\"33.545.351.5.2\",\"equipmentModel\":\"GU455554\",\"equipmentName\":\"卫星监测AU-e\",\"equipmentNumber\":\"AT-65465110\",\"exFactoryDate\":\"2022-02-02\",\"factoryInfo\":\"广州硬件设备公司\",\"id\":6,\"installationDate\":\"2022-02-17\",\"installationInfo\":\"安装详情安装详情安装详情安装详情安装详情\",\"ipAddress\":\"192.0.0.1\",\"modifiedBy\":1,\"portNumber\":\"8002\",\"remark\":\" 备注备注备注备注备注\",\"subnetMask\":\"33.545.351.5.2\"},\"msg\":\"success\"}'),
(25,1,'wy',0,'用户管理',2,0,'SUCCESS','2022-02-22 18:32:57','127.0.0.1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"address\":\"南京\",\"avatar\":\"\",\"city\":\"\",\"creationDate\":\"2022-02-15 17:04:14\",\"district\":\"\",\"id\":2,\"loginName\":\"test\",\"nickname\":\"\",\"phone\":\"18156275752\",\"province\":\"\",\"roleIds\":[4,1],\"sex\":1,\"userName\":\"test\"}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(26,1,'wy',0,'用户管理',2,0,'SUCCESS','2022-02-22 18:33:35','127.0.0.1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"address\":\"南京\",\"avatar\":\"\",\"city\":\"\",\"creationDate\":\"2022-02-15 17:04:14\",\"district\":\"\",\"id\":2,\"loginName\":\"test\",\"nickname\":\"\",\"phone\":\"18156275752\",\"province\":\"\",\"roleIds\":[4],\"sex\":1,\"userName\":\"test\"}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(27,1,'wy',0,'用户管理',2,0,'SUCCESS','2022-02-22 18:33:46','127.0.0.1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"address\":\"\",\"avatar\":\"\",\"birthDate\":\"2022-02-01\",\"city\":\"\",\"creationDate\":\"2022-02-16 14:10:24\",\"district\":\"\",\"id\":3,\"loginName\":\"CC\",\"nickname\":\"\",\"phone\":\"18156275755\",\"province\":\"\",\"roleIds\":[4,1],\"sex\":0,\"userName\":\"CC\"}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(28,1,'wy',0,'角色管理',2,0,'SUCCESS','2022-02-22 18:34:53','127.0.0.1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"id\":1,\"menuIds\":[1,2,5,6,7,8,17,3,9,10,11,12,4,13,14,15,16,18],\"remark\":\"\\t超级管理员\",\"roleCode\":\"admin\",\"roleName\":\"\\t超级管理员\",\"userId\":1}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(29,1,'wy',0,'角色管理',2,0,'SUCCESS','2022-02-22 18:35:46','127.0.0.1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"id\":1,\"menuIds\":[1,2,5,6,7,8,17,3,9,10,11,12,4,13,14,15,16,18,19,20,21,22,23,24,25,26,27,28,39,29,30,35,36,38,37],\"remark\":\"\\t超级管理员\",\"roleCode\":\"admin\",\"roleName\":\"\\t超级管理员\",\"userId\":1}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(30,1,'wy',0,'菜单管理',1,0,'SUCCESS','2022-02-28 10:35:02','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"icon\":\"icon-admin-yidongyunkongzhitaiicon69\",\"menuName\":\"告警日志\",\"menuType\":2,\"parentId\":35,\"perms\":\"log:alarmlog:list\",\"sort\":3,\"url\":\"/log/alarmlog/index\",\"userId\":1}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(31,1,'wy',0,'菜单管理',2,0,'SUCCESS','2022-02-28 10:35:24','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"icon\":\"#icon-admin-xitongzhuangtai\",\"id\":39,\"menuName\":\"工作状态\",\"menuType\":2,\"parentId\":18,\"parentMenuName\":\"业务管理\",\"perms\":\"business:working:index\",\"remark\":\"\",\"sort\":3,\"url\":\"/business/working/index\",\"userId\":1}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(32,1,'wy',0,'菜单管理',2,0,'SUCCESS','2022-02-28 10:35:30','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"icon\":\"#icon-admin-yidongyunkongzhitaiicon69\",\"id\":40,\"menuName\":\"告警日志\",\"menuType\":2,\"parentId\":35,\"parentMenuName\":\"日志管理\",\"perms\":\"log:alarmlog:list\",\"remark\":\"\",\"sort\":3,\"url\":\"/log/alarmlog/index\",\"userId\":1}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(33,1,'wy',0,'登录日志',2,0,'SUCCESS','2022-02-28 15:50:53','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10','{\"id\":29,\"remark\":\"12313131\",\"userId\":1}','{\"code\":200,\"data\":{\"activeFlag\":\"Y\",\"alarmContent\":13,\"alarmDate\":\"2022-02-28T14:57:09\",\"alarmId\":13,\"alarmType\":1,\"companyId\":2,\"createdBy\":1,\"creationDate\":\"2022-02-28T14:57:09\",\"equipmentId\":21,\"id\":29,\"modifiedBy\":1,\"remark\":\"12313131\"},\"msg\":\"success\"}'),
(34,1,'wy',0,'登录日志',2,0,'SUCCESS','2022-02-28 15:51:50','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10','{\"id\":30,\"remark\":\"111\",\"userId\":1}','{\"code\":200,\"data\":{\"activeFlag\":\"Y\",\"alarmContent\":14,\"alarmDate\":\"2022-02-28T14:57:09\",\"alarmId\":14,\"alarmType\":1,\"companyId\":2,\"createdBy\":1,\"creationDate\":\"2022-02-28T14:57:09\",\"equipmentId\":22,\"id\":30,\"modifiedBy\":1,\"remark\":\"111\"},\"msg\":\"success\"}'),
(35,1,'wy',0,'登录日志',2,0,'SUCCESS','2022-02-28 15:56:46','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10','{\"id\":29,\"remark\":\"测试测试\",\"userId\":1}','{\"code\":200,\"data\":{\"activeFlag\":\"Y\",\"alarmContent\":13,\"alarmDate\":\"2022-02-28T14:57:09\",\"alarmId\":13,\"alarmType\":1,\"companyId\":2,\"createdBy\":1,\"creationDate\":\"2022-02-28T14:57:09\",\"equipmentId\":21,\"id\":29,\"modifiedBy\":1,\"modifyDate\":\"2022-02-28T15:56:46\",\"remark\":\"测试测试\"},\"msg\":\"success\"}'),
(36,1,'wy',0,'登录日志',2,0,'SUCCESS','2022-02-28 15:56:58','0:0:0:0:0:0:0:1','内网IP','Chrome 9','Windows 10','{\"id\":30,\"remark\":\"测试测试\",\"userId\":1}','{\"code\":200,\"data\":{\"activeFlag\":\"Y\",\"alarmContent\":14,\"alarmDate\":\"2022-02-28T14:57:09\",\"alarmId\":14,\"alarmType\":1,\"companyId\":2,\"createdBy\":1,\"creationDate\":\"2022-02-28T14:57:09\",\"equipmentId\":22,\"id\":30,\"modifiedBy\":1,\"modifyDate\":\"2022-02-28T15:56:58\",\"remark\":\"测试测试\"},\"msg\":\"success\"}'),
(37,1,'wy',0,'角色管理',3,0,'SUCCESS','2022-03-01 17:28:42','127.0.0.1','内网IP','Chrome 9','Windows 10','{\"id\":[\"5\"]}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(38,1,'wy',0,'角色管理',2,0,'SUCCESS','2022-03-01 17:28:49','127.0.0.1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"id\":4,\"menuIds\":[1,2,5,3,9,20,25],\"remark\":\"Test\",\"roleCode\":\"Test\",\"roleName\":\"Test\",\"userId\":1}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(39,1,'wy',0,'菜单管理',1,0,'SUCCESS','2022-03-01 17:40:28','127.0.0.1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"icon\":\"\",\"menuName\":\"导出\",\"menuType\":3,\"parentId\":40,\"perms\":\"business:alarmlog:export\",\"url\":\"/api/log/exportAlarmLog\",\"userId\":1}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(40,1,'wy',0,'菜单管理',2,0,'SUCCESS','2022-03-01 17:40:38','127.0.0.1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"icon\":\"\",\"id\":41,\"menuName\":\"导出\",\"menuType\":3,\"parentId\":40,\"parentMenuName\":\"告警日志\",\"perms\":\"business:alarmlog:export\",\"remark\":\"\",\"sort\":1,\"url\":\"/api/log/exportAlarmLog\",\"userId\":1}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(41,1,'wy',0,'菜单管理',1,0,'SUCCESS','2022-03-01 17:41:53','127.0.0.1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"icon\":\"\",\"menuName\":\"导出\",\"menuType\":3,\"parentId\":19,\"perms\":\"business:company:export\",\"sort\":5,\"url\":\"/api/company/exportCompany\",\"userId\":1}','{\"code\":200,\"data\":{},\"msg\":\"success\"}'),
(42,1,'wy',0,'菜单管理',1,0,'SUCCESS','2022-03-01 17:42:46','127.0.0.1','内网IP','Chrome 9','Windows 10','{\"activeFlag\":\"Y\",\"icon\":\"\",\"menuName\":\"导出\",\"menuType\":3,\"parentId\":24,\"perms\":\"business:equipment:export\",\"sort\":5,\"url\":\"/api/equipment/exportEquipment\",\"userId\":1}','{\"code\":200,\"data\":{},\"msg\":\"success\"}');

/*Table structure for table `sys_role` */

DROP TABLE IF EXISTS `sys_role`;

CREATE TABLE `sys_role` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_name` varchar(60) NOT NULL DEFAULT '' COMMENT '角色名称',
  `role_code` varchar(60) NOT NULL DEFAULT '' COMMENT '角色标识',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `sort` tinyint unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `active_flag` char(1) NOT NULL DEFAULT 'Y' COMMENT '是否有效, Y:有效 N:无效',
  `creation_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `created_by` int NOT NULL DEFAULT '0' COMMENT '创建人',
  `modify_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改日期',
  `modified_by` int NOT NULL DEFAULT '0' COMMENT '修改人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3 COMMENT='角色表';

/*Data for the table `sys_role` */

insert  into `sys_role`(`id`,`role_name`,`role_code`,`remark`,`sort`,`active_flag`,`creation_date`,`created_by`,`modify_date`,`modified_by`) values
(1,'	超级管理员','admin','	超级管理员',0,'Y','2022-02-11 11:18:40',1,'2022-02-14 18:27:42',1),
(2,'系统操作员','systemOperator','系统操作员',0,'Y','2022-02-15 14:35:24',1,'2022-02-15 14:36:39',1),
(3,'用户管理员','userAdministrator','用户管理员',0,'Y','2022-02-15 14:42:32',1,'2022-02-15 14:42:32',1),
(4,'Test','Test','Test',0,'Y','2022-02-15 16:56:42',1,'2022-03-01 17:28:49',1);

/*Table structure for table `sys_role_menu` */

DROP TABLE IF EXISTS `sys_role_menu`;

CREATE TABLE `sys_role_menu` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id` int unsigned NOT NULL COMMENT '角色ID',
  `menu_id` int unsigned NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8mb3 COMMENT='角色关联菜单表';

/*Data for the table `sys_role_menu` */

insert  into `sys_role_menu`(`id`,`role_id`,`menu_id`) values
(52,1,1),
(53,1,2),
(54,1,5),
(55,1,6),
(56,1,7),
(57,1,8),
(58,1,17),
(59,1,3),
(60,1,9),
(61,1,10),
(62,1,11),
(63,1,12),
(64,1,4),
(65,1,13),
(66,1,14),
(67,1,15),
(68,1,16),
(69,1,18),
(70,1,19),
(71,1,20),
(72,1,21),
(73,1,22),
(74,1,23),
(75,1,24),
(76,1,25),
(77,1,26),
(78,1,27),
(79,1,28),
(80,1,39),
(81,1,29),
(82,1,30),
(83,1,35),
(84,1,36),
(85,1,38),
(86,1,37),
(87,4,1),
(88,4,2),
(89,4,5),
(90,4,3),
(91,4,9),
(92,4,20),
(93,4,25);

/*Table structure for table `sys_user` */

DROP TABLE IF EXISTS `sys_user`;

CREATE TABLE `sys_user` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `login_name` varchar(64) NOT NULL DEFAULT '' COMMENT '登录名',
  `pwd` varchar(64) NOT NULL DEFAULT '' COMMENT '密码',
  `nickname` varchar(64) NOT NULL DEFAULT '' COMMENT '昵称',
  `user_name` varchar(64) NOT NULL DEFAULT '' COMMENT '姓名',
  `sex` tinyint unsigned NOT NULL DEFAULT '1' COMMENT '性别(0女 1男)',
  `avatar` varchar(100) DEFAULT '' COMMENT '头像',
  `birth_date` date DEFAULT NULL COMMENT '出生日期',
  `phone` varchar(20) NOT NULL DEFAULT '' COMMENT '手机号码',
  `province` varchar(10) DEFAULT '' COMMENT '省',
  `city` varchar(10) DEFAULT '' COMMENT '市',
  `district` varchar(10) DEFAULT '' COMMENT '区',
  `address` varchar(50) DEFAULT '' COMMENT '地址',
  `company_id` int NOT NULL DEFAULT '0' COMMENT '单位ID',
  `dept_id` int NOT NULL DEFAULT '0' COMMENT '部门ID',
  `user_type` tinyint unsigned NOT NULL DEFAULT '1' COMMENT '用户类型(0内置用户 1注册用户)',
  `active_flag` char(1) NOT NULL DEFAULT 'Y' COMMENT '是否有效, Y:有效 N:无效',
  `creation_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `created_by` int NOT NULL DEFAULT '0' COMMENT '创建人',
  `modify_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改日期',
  `modified_by` int NOT NULL DEFAULT '0' COMMENT '修改人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COMMENT='用户表';

/*Data for the table `sys_user` */

insert  into `sys_user`(`id`,`login_name`,`pwd`,`nickname`,`user_name`,`sex`,`avatar`,`birth_date`,`phone`,`province`,`city`,`district`,`address`,`company_id`,`dept_id`,`user_type`,`active_flag`,`creation_date`,`created_by`,`modify_date`,`modified_by`) values
(1,'wy','ddd056e3116ef3c8972a11c62a770a31','','wy',1,'','2022-01-30','18288888888','','','','南京',0,0,0,'Y','2022-02-11 11:22:04',0,'2022-02-16 14:25:11',0),
(2,'test','b5cc2fb8a659398211d8d6502552636c','','test',1,'',NULL,'18288888889','','','','南京',0,0,1,'Y','2022-02-15 17:04:14',0,'2022-02-18 17:54:23',1),
(3,'CC','dfa93cc5a878db20ac42e635db4d6c4f','','CC',0,'','2022-02-01','18288888887','','','','',0,0,1,'Y','2022-02-16 14:10:24',0,'2022-02-16 14:29:22',1);

/*Table structure for table `sys_user_role` */

DROP TABLE IF EXISTS `sys_user_role`;

CREATE TABLE `sys_user_role` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int unsigned NOT NULL COMMENT '用户ID',
  `role_id` int unsigned NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb3 COMMENT='用户关联角色表';

/*Data for the table `sys_user_role` */

insert  into `sys_user_role`(`id`,`user_id`,`role_id`) values
(8,2,4),
(9,3,4),
(10,3,1);

