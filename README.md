# admin

#### 介绍
后台管理系统,采用的是非前后端分离,使用Spring Boot快速开发,Shiro 做权限管理,
vue + element-ui 渲染页面,分页采用vxe-grid... 
已经完成模块有用户管理、菜单管理、角色管理、单位管理、设备管理、日志管理

并且支持互斥登录,采用Netty 推送消息。

#### 技术选型
核心框架：Spring Boot  2.5
权限框架：Apache Shiro 1.8 
持久层框架： MyBatis-plus 3.3
数据库连接池：Alibaba Druid
网络框架   : Netty 4.1.68
日志管理：logback
模板引擎：Thymeleaf
JS框架： VUE v2.6.11
CSS框架： element-ui 2.15.7
分页插件：vxe-grid 3.4.15



 
 
![](./images/login.png) 

![](./images/mutually_exclusive_login.png) 

![](./images/user.png) 
  
![](./images/role.png) 

![](./images/menu.png) 
 

 


 
