new Vue({
    el: '#app',
    data() {
        return {
            ws: null,
            baseForm: {
                id: '',
                sex: '',
                birthDate: '',
                phone: '',
                province: '',
                city: '',
                district: '',
                address: '',
                avatar: ''
            },
        }
    },
    created() {

    },
    methods: {
        formatDate({cellValue}) {
            return XEUtils.toDateString(cellValue, 'yyyy-MM-dd HH:ss:mm')
        },

        formatToDateTime(row) {
            const daterc = row.cellValue;
            if (daterc != null) {
                var date = new Date(daterc);
                var year = date.getFullYear();
                /* 在日期格式中，月份是从0开始，11结束，因此要加0
                 * 使用三元表达式在小于10的前面加0，以达到格式统一  如 09:11:05
                 * */
                var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
                var day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
                var hours = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
                var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
                var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
                // 拼接
                return year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
            }
        },

        formatBusinessType(row) {
            const type = row.cellValue;

            var businessType = '';
            switch (type) {
                case 0:
                    businessType = '其它';
                    break;
                case 1:
                    businessType = '新增';
                    break;
                case 2:
                    businessType = '修改';
                    break;
                case 3:
                    businessType = '删除';
                    break;
                case 4:
                    businessType = '重置密码';
                    break;
                case 5:
                    businessType = '导出';
                    break;
                case 6:
                    businessType = '导入';
                    break;
                case 7:
                    businessType = '强退';
                    break;
            }
            return businessType;

        },

        checkPhone(rule, value, callback) {
            let reg = /^1[345789]\d{9}$/;
            if (!reg.test(value)) {
                callback(new Error('请输入11位手机号'))
            } else {
                callback()
            }
        },

        setWs(newWs){
            this.ws= newWs;
        },

        initWebSocket(userId) {
            var websocket = null;
            //判断浏览器是否支持websocket
            if ('WebSocket' in window) {
                websocket = new WebSocket("ws://localhost:8888/webSocket");
                setWs(websocket);

                websocket.onopen = function () {
                    console.log("客户端连接成功");
                    var param = {
                        uid: userId,
                        type: 'home'
                    };
                    websocket.send(JSON.stringify(param));
                };
                websocket.onerror = function () {
                    console.log("客户端连接失败");
                    websocket.send("客户端连接失败");
                };
                websocket.onclose = function () {
                    console.log("客户端连接关闭");
                    websocket.send("客户端连接关闭");
                };
                websocket.onmessage = function (e) {
                    console.log("onmessage-----------------", e);
                };
                //监听窗口关闭事件，当窗口关闭时，主动去关闭websocket连接，防止连接还没断开就关闭窗口，server端会抛异常。
                window.onbeforeunload = function () {
                    closeWebSocket();
                }
            } else {
                console.log('当前浏览器 Not support websocket')
            }
        },

        //关闭WebSocket连接
        closeWebSocket() {
            console.log("关闭WebSocket连接");
            websocket.close();
        }
        ,
    },


});