new Vue({
    el: '#app',
    data() {
        const validateOldPass = (rule, value, callback) => {
            this.loading = true;
            const params = this.pwdForm;
            axios.post('/api/sys/validateOldPass', params)
                .then((res) => {
                    this.loading = false;
                if (res.code === 200) {
                    callback();
                }else{
                    callback(new Error("原密码错误"));
                }
            });
        };

        // 效验新密码是否一致
        const validatePass = (rule, value, callback) => {
            // value代表第三个框的确认密码confirmPwd
            if (value !== this.pwdForm.newPwd) {
                callback(new Error("两次输入的密码不一致"));
            } else {
                callback();
            }
        };


        return {
            userId: null,
            avatar: '',
            canFullScreen: true,
            isFullScreen: false,
            isCollapse: true,
            tipsDialog: false,
            userCenterDialog: false,
            editableTabsValue: '',
            editableTabs: [],
            sysUser: loginResVO.sysUser,
            menus: loginResVO.menus,
            actionUrl: 'common/file/upload',
            userCenterModel: 'first',
            baseForm: {
                id: '',
                sex: '',
                birthDate: '',
                phone: '',
                province: '',
                city: '',
                district: '',
                address: '',
                avatar: ''
            },
            baseFormRules: {
                sex: [
                    {required: true, message: '请选择', trigger: 'blur'}
                ],
                birthDate: [
                    {required: true, message: '请选择', trigger: 'blur'}
                ],
                phone: [
                    {required: true, message: '请输入', trigger: 'blur'},
                    {min: 11, max: 11, message: '手机号码格式不正确', trigger: 'blur'}
                ]
            },
            pwdForm: {
                id: null,
                oldPwd: '',
                newPwd: '',
                confirmPwd: ''
            },
            pwdFormRules: {
                oldPwd: [
                    {required: true, message: '请输入', trigger: 'blur'},
                    {min: 2, max: 10, message: '长度在 2 到 10 个字符', trigger: 'blur'},
                    { validator: validateOldPass, trigger: "blur" }
                ],
                newPwd: [
                    {required: true, message: '请输入', trigger: 'blur'},
                    {min: 2, max: 10, message: '长度在 2 到 10 个字符', trigger: 'blur'}
                ],
                confirmPwd: [
                    {required: true, message: '请输入', trigger: 'blur'},
                    {min: 2, max: 10, message: '长度在 2 到 10 个字符', trigger: 'blur'},
                    { validator: validatePass, trigger: "blur" }
                ]
            },
            regionList: []
        }
    },
    mounted(){
        //监听键盘按键事件
        let self = this;
        this.$nextTick(function () {
            document.addEventListener('keyup', function (e) {
                //此处填写你的业务逻辑即可
                if (e.keyCode == 27) {
                    self.mutePlay();
                }
            })
        })
    },
    created() {
        this.userId = this.sysUser.id;
        this.avatar = this.sysUser.avatar;
        localStorage.setItem('admin-sysUser', JSON.stringify(this.sysUser));
        this.initWebSocket();
    },
    methods: {
        initWebSocket(){
            if(window.WebSocket){
                var _this = this;
                webSocket = new WebSocket("ws://localhost:8888/webSocket");
                webSocket.onopen=function(){
                    console.log("客户端连接成功");
                    var param = {
                        userId : _this.userId,
                        type: 'home'
                    };
                    _this.sendWebSocketMsg(param);
                };
                webSocket.onerror=function(){
                    console.log("客户端连接失败");
                };
                webSocket.onclose=function(){
                    console.log("客户端连接关闭");
                };
                webSocket.onmessage=function(e){
                    console.log("onmessage-----------------", e.data);
                    var res = JSON.parse(e.data);
                    if (res.code){
                        if (res.type === 1) {
                            console.log("该账号异地登录，您被强制下线");
                            _this.openReLoginMsg(res.msg);
                            //window.location.href = "/logout";
                        }else  if (res.type === 2) {
                            _this.openAlarmMsg(res.msg);
                            _this.palyAudio();
                        }
                    }

                };
                //监听窗口关闭事件，当窗口关闭时，主动去关闭websocket连接，防止连接还没断开就关闭窗口，server端会抛异常。
                window.onbeforeunload = function () {
                    console.log("关闭WebSocket连接");
                    webSocket.close();
                }
            }else{
                alert("该环境不支持websocket")
            }
        },
        sendWebSocketMsg(param) {
            if (window.WebSocket) {
                if (webSocket.readyState == WebSocket.OPEN) {
                    webSocket.send(JSON.stringify(param));
                } else {
                    console.log("与服务器连接尚未建立")
                }
            }
        },
        mutePlay(){
            console.log("静音播放");
            this.tipsDialog = false;
            audio = document.getElementById("myAudio");
            audio.muted = true;
            audio.play();
        },
        palyAudio(){
            audio = document.getElementById("myAudio");
            audio.muted = false;
            audio.play();
            $(audio).unbind("ended").bind("ended", function(){
                audio.play();
            });

        },
        openAlarmMsg(msg) {
            this.$confirm(msg, '告警提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning',
                closeOnPressEscape: false,
                center: false
            }).then(() => {
                 console.log("确定");
                 audio.pause();
            }).catch(() => {
                console.log("取消");
                audio.pause();
            });
        },
        openReLoginMsg(msg) {
            this.$alert(msg, '温馨提示', {
                confirmButtonText: '确定',
                callback: action => {
                window.location.href = "/login";
                }
            });
        },
        handleCommand(cmd) {
            if (cmd == 'logout') {
                axios.post('logout').then((res) => {
                    if (res.code === 200) {
                        localStorage.clear();
                        // parent.location.href ='';
                    }
                });
            } else if (cmd == 'userCenter') {
                this.userCenterDialog = true;
                const sysUser = JSON.parse(localStorage.getItem("admin-sysUser"));
                this.baseForm.id = sysUser.id;
                this.baseForm.sex = sysUser.sex;
                this.baseForm.birthDate = sysUser.birthDate;
                this.baseForm.phone = sysUser.phone;
                this.baseForm.province = sysUser.province;
                this.baseForm.city = sysUser.city;
                this.baseForm.district = sysUser.district;
                this.baseForm.address = sysUser.address;
                this.baseForm.avatar = sysUser.avatar;

                this.pwdForm = {
                    id: sysUser.id,
                    oldPwd: '',
                    newPwd: '',
                    confirmPwd: ''
                }


            }
        },
        addTab(tabName, tabUrl) {
            let isExist = false;
            let tabs = this.editableTabs;
            tabs.forEach((tab, index) => {
                if (tab.title === tabName) {
                    isExist = true;
                    return;
                }
            });
            if (isExist) {
                this.editableTabsValue = tabUrl;
                return;
            }
            tabs.push({
                title: tabName,
                name: tabUrl,
                content: tabUrl
            });
            this.editableTabsValue = tabUrl;
        },
        removeTab(tabName) {
            let tabs = this.editableTabs;
            let activeName = this.editableTabsValue;
            if (activeName === tabName) {
                tabs.forEach((tab, index) => {
                    if (tab.name === tabName) {
                        let nextTab = tabs[index + 1] || tabs[index - 1];
                        if (nextTab) {
                            activeName = nextTab.name;
                        }
                    }
                });
            }
            this.editableTabsValue = activeName;
            this.editableTabs = tabs.filter(tab => tab.name !== tabName);
        },
        handleClick(tab, event) {
            if (this.userCenterModel === 'second' ){
                this.pwdForm.oldPwd = '';
                this.pwdForm.newPwd = '';
                this.pwdForm.confirmPwd = '';
            }
        },
        handleAvatarSuccess(idx, res, file, name) {
            if (res.code == '0000') {
                this.baseForm.avatar = res.data.url;
            }
        },
        beforeAvatarUpload(file) {
            const isLt2M = file.size / 1024 / 1024 < 2;
            if (!isLt2M) {
                this.$message.error('上传头像图片大小不能超过2MB!');
            }
            return isLt2M;
        },
        baseFormSave(formName) {
            this.$refs[formName].validate((valid) => {
                if (valid) {
                    this.loading = true;
                    const params = this.baseForm;
                    //console.log('params ==' + JSON.stringify(params));
                    axios.post('/api/sys/updateSysUser', params)
                        .then((res) => {
                        this.loading = false;
                        if (res.code === 200) {
                            const sysUser = JSON.parse(localStorage.getItem("admin-sysUser"));
                            sysUser.id = this.baseForm.id;
                            sysUser.sex = this.baseForm.sex;
                            sysUser.birthDate = this.baseForm.birthDate;
                            sysUser.phone = this.baseForm.phone;
                            sysUser.province = this.baseForm.province;
                            sysUser.city = this.baseForm.city;
                            sysUser.district = this.baseForm.district;
                            sysUser.address = this.baseForm.address;
                            sysUser.avatar = this.baseForm.avatar;
                            localStorage.setItem('admin-sysUser', JSON.stringify(sysUser));

                            this.$message({
                                message: '修改成功',
                                type: 'success'
                            });
                            this.userCenterDialog = false;
                        }
                    });
                } else {
                    console.log('error submit!!');
            return false;
        }
        });


        },
        pwdFormSave(formName) {
            this.$refs[formName].validate((valid) => {
                if (valid) {
                    this.loading = true;
                    const params = this.pwdForm;
                    console.log("params=" + JSON.stringify(params));
                    axios.post('/api/sys/resetPwd', params)
                        .then((res) => {
                        this.loading = false;
                        if (res.code === 200) {
                            this.$message({
                                message: '修改密码成功',
                                type: 'success'
                            });
                            this.userCenterDialog = false;
                        }
                    });

                } else {
                    console.log('error submit!!');
                    return false;
                }
            });

        },
        toggleCollapse () {
            this.isCollapse = !this.isCollapse
        },
        //全屏设置
        toggleFullScreen(){
            var doc = document, de = doc.documentElement;
            if (de  && !this.isFullScreen) {
                if (de.requestFullscreen) {
                    de.requestFullscreen();
                    this.isFullScreen = true;
                } else {
                    if (de.msRequestFullscreen) {
                        de.msRequestFullscreen();
                        this.isFullScreen = true;
                    } else {
                        if (de.mozRequestFullScreen) {
                            de.mozRequestFullScreen();
                            this.isFullScreen = true;
                        } else {
                            if (de.webkitRequestFullscreen) {
                                de.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
                                this.isFullScreen = true;
                            }
                        }
                    }
                }
            } else {
                if (this.isFullScreen) {
                    if (doc.exitFullscreen) {
                        doc.exitFullscreen();
                        this.isFullScreen = false;
                    } else {
                        if (doc.msExitFullscreen) {
                            doc.msExitFullscreen();
                            this.isFullScreen = false;
                        } else {
                            if (doc.mozCancelFullScreen) {
                                doc.mozCancelFullScreen();
                                this.isFullScreen = false;
                            } else {
                                if (doc.webkitExitFullscreen) {
                                    doc.webkitExitFullscreen();
                                    this.isFullScreen = false;
                                }
                            }
                        }
                    }
                }
            }
        },




    },


});