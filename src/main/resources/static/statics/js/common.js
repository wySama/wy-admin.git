
function formatDate({cellValue}) {
    return XEUtils.toDateString(cellValue, 'yyyy-MM-dd HH:ss:mm')
}

function formatToDateTime(row) {
    const daterc = row.cellValue;
    if (daterc != null) {
        var date = new Date(daterc);
        var year = date.getFullYear();
        /* 在日期格式中，月份是从0开始，11结束，因此要加0
         * 使用三元表达式在小于10的前面加0，以达到格式统一  如 09:11:05
         * */
        var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
        var day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
        var hours = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
        var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
        // 拼接
        return year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
    }
}


function formatBusinessType(row) {
    const type = row.cellValue;

    var businessType= '';
    switch (type){
        case 0:
            businessType= '其它';
            break;
        case 1:
            businessType= '新增';
            break;
        case 2:
            businessType= '修改';
            break;
        case 3:
            businessType= '删除';
            break;
        case 4:
            businessType= '重置密码';
            break;
        case 5:
            businessType= '导出';
            break;
        case 6:
            businessType= '导入';
            break;
        case 7:
            businessType= '强退';
            break;
    }
    return businessType;

}


function checkPhone(rule, value, callback) {
    let reg = /^1[345789]\d{9}$/;
    if (!reg.test(value)) {
        callback(new Error('请输入11位手机号'))
    } else {
        callback()
    }
}