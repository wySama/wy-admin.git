// 响应时间
axios.defaults.timeout = 40000;
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8'

axios.interceptors.request.use(config => {
    let token = JSON.parse(localStorage.getItem("kanfit-token"));
    if (token) {
        config.headers.token = token;
    }
    return config;
}, function (error) {
    return Promise.reject(error);
});

axios.interceptors.response.use(response => {
    if (response.data.code == '9999') {
        console.log("9999");
        parent.location.href ='/';
    }
    if(response.request.responseType == 'arraybuffer') {
        console.log("arraybuffer");
        return response;
    }
    //console.log("response.data" + JSON.stringify(response.data));
    return response.data;
});

// 公共POST请求
let  exportPost = (url, params, fileName) => {
    return new Promise((resolve, reject) => {
        axios({
                method: 'post',
                url: url,
                data: params,
                responseType: 'blob'
              }).then((res) => {
                    var blob = new Blob([res], {
                        type: "application/msexcel;charset=utf-8",
                    });
                    const downloadElement = document.createElement('a');
                    const href = window.URL.createObjectURL(blob);
                    downloadElement.href = href;
                    downloadElement.download = fileName;
                    document.body.appendChild(downloadElement);
                    downloadElement.click();
                    document.body.removeChild(downloadElement);
                    window.URL.revokeObjectURL(href);
                }, err => {
                        reject(err)
                    }).catch((error) => {
                        console.log('服务器宕机了 - _ -');
                        reject(error)
                    } )
    })
}

