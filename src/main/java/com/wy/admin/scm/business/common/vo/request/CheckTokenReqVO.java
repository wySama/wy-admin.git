package com.wy.admin.scm.business.common.vo.request;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "检查Token请求参数")
public class CheckTokenReqVO {

    @ApiModelProperty(value = "token")
    private String token;



}
