package com.wy.admin.scm.business.system.vo.request;

import lombok.Data;

import java.io.Serializable;


@Data
public class UpdatePwdReqVO implements Serializable {


    private Integer id;

    private String oldPwd;
    private String newPwd;
    private String confirmPwd;
}
