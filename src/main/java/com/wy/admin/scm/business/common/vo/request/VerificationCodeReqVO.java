package com.wy.admin.scm.business.common.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


@Data
@ApiModel(value = "验证码检验请求参数")
public class VerificationCodeReqVO implements Serializable {

    private static final long serialVersionUID = -1609954971902880901L;
    @ApiModelProperty(value = "手机号")
    private String mobile;

    @ApiModelProperty(value = "验证码")
    private String yzm;

    @ApiModelProperty(value = "用户Id")
    private Integer userId;


}
