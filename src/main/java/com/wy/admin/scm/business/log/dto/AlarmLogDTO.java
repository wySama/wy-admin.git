package com.wy.admin.scm.business.log.dto;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.wy.admin.scm.business.common.component.AlarmTypeConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;


@Data
@EqualsAndHashCode
@ColumnWidth(25)
public class AlarmLogDTO implements Serializable {
    private static final long serialVersionUID = -4220389250938578351L;

    @ExcelIgnore
    private Integer id;

    /**
     * 单位ID
     */
    @ExcelIgnore
    private Integer companyId;

    /**
     * 设备ID
     */
    @ExcelProperty("设备ID")
    private Integer equipmentId;

    /**
     * 告警的序号
     */
    @ExcelProperty("告警的序号")
    private Integer alarmId;

    /**
     * IP地址
     */
    @ExcelProperty("IP地址")
    private String ipAddress;


    /**
     * 告警时刻
     */
    @DateTimeFormat("yyyy-MM-dd HH:mm:ss")
    @ExcelProperty("告警时刻")
    private LocalDateTime alarmDate;

    /**
     * 告警类型（0:撤销告警 1:产生告警）
     */
    @ExcelProperty(value = "告警类型", converter = AlarmTypeConverter.class)
    private Integer alarmType;

    /**
     * 告警内容
     */
    @ExcelIgnore
    private Integer alarmContent;

    /**
     * 告警内容
     */
    @ExcelProperty("告警内容")
    private String alarmContentText;

    /**
     * 修改人
     */
    @ExcelProperty("处理人")
    private String modifiedByName;


    /**
     * 修改日期
     */
    @DateTimeFormat("yyyy-MM-dd HH:mm:ss")
    @ExcelProperty("处理时间")
    private LocalDateTime modifyDate;

    /**
     * 备注
     */
    @ExcelProperty("备注")
    private String remark;

    /**
     * 是否有效, Y:有效 N:无效
     */
    @ExcelIgnore
    private String activeFlag;

    /**
     * 创建日期
     */
    @ExcelIgnore
    private LocalDateTime creationDate;

    /**
     * 创建人
     */
    @ExcelIgnore
    private Integer createdBy;


    /**
     * 修改人
     */
    @ExcelIgnore
    private Integer modifiedBy;


}
