package com.wy.admin.scm.business.equipment.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * <p>
 * 设备信息表
 * </p>
 *
 * @author wuyong
 * @since 2022-02-16
 */
@Data
@TableName("admin.equipment")
public class Equipment extends Model<Equipment> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 单位ID
     */
    private Integer companyId;

    /**
     * 服务器生产厂家名称（最大80）
     */
    private String factoryInfo;

    /**
     * 服务器型号（最大20字节）
     */
    private String equipmentModel;

    /**
     * 服务器设备名称（最大40字节）
     */
    private String equipmentName;

    /**
     * 服务器设备编号（最大20字节）
     */
    private String equipmentNumber;

    /**
     * 出厂日期格式：YYYY-MM-DD（如2021-01-07）。
     */
    private String exFactoryDate;

    /**
     * IP地址
     */
    private String ipAddress;

    /**
     * 子网掩码
     */
    private String subnetMask;

    /**
     * 默认网关
     */
    private String defaultGateway;

    /**
     * 端口号
     */
    private String portNumber;

    /**
     * 安装日期
     */
    private LocalDate installationDate;

    /**
     * 安装详情
     */
    private String installationInfo;

    /**
     * 备注
     */
    private String remark;

    /**
     * 是否有效, Y:有效 N:无效
     */
    private String activeFlag;

    /**
     * 创建日期
     */
    private LocalDateTime creationDate;

    /**
     * 创建人
     */
    private Integer createdBy;

    /**
     * 修改日期
     */
    private LocalDateTime modifyDate;

    /**
     * 修改人
     */
    private Integer modifiedBy;


}
