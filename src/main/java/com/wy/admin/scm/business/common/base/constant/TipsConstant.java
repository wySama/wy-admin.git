package com.wy.admin.scm.business.common.base.constant;

public class TipsConstant {

    public static final String SYSUSER_NOT_EXIT = "该用户不存在";
    public static final String SYSROLE_NOT_EXIT = "该角色不存在";
    public static final String SYSUSER_LOGIN_NAME_EXIT = "该用户名已存在";
    public static final String OLD_PWD_NOT_NULL = "原密码不能为空";
    public static final String OLD_PWD_ERROR = "原密码错误";
    public static final String THE_TWO_PASSWORDS_ARE_INCONSISTENT = "两次输入的密码不一致";
    public static final String PWD_ERROR = "密码错误";
    public static final String ORDER_NOT_EXIT = "该订单不存在";
    public static final String KANFIT_ACTION_NOT_EXIT = "该动作不存在";
    public static final String DICT_DATA_EXIT = "该数据存在";
    public static final String DICT_NOT_DATA_EXIT = "该数据存在";
    public static final String SYSMENU_NAME_EXIT = "该菜单名称已存在";
    public static final String PLEASE_DELETE_THE_CHILD_NODE_FIRST = "请先删除子节点";

    public static final String  USER_JCAPTCHA_ERROR = "验证码错误";

    public static final String  USRE_NAME_OR_PASSWARD_ERROR = "账号或密码错误";

    public static final String  USRE_NAME_AND_PASSWARD_IS_NOT_NULL = "用户名和密码不能为空";

    public static final String  NO_PERMISSION = "没有权限";

    public static final String  EXPORT_EXCEPTION = "导出异常";



}
