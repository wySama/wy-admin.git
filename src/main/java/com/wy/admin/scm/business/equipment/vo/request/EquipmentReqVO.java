package com.wy.admin.scm.business.equipment.vo.request;

import com.wy.admin.scm.business.common.model.Pagination;
import lombok.Data;

import java.io.Serializable;

@Data
public class EquipmentReqVO  implements Serializable {

    private static final long serialVersionUID = 5094209713305774702L;

    private Integer companyId;
    /**
     * 服务器生产厂家名称（最大80）
     */
    private String factoryInfo;

    /**
     * 服务器设备名称（最大40字节）
     */
    private String equipmentName;

    /**
     * 服务器设备编号（最大20字节）
     */
    private String equipmentNumber;

    /**
     * IP地址
     */
    private String ipAddress;


    private String activeFlag;

    private Pagination pagination;
}
