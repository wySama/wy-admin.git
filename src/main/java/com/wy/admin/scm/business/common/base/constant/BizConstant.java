package com.wy.admin.scm.business.common.base.constant;

/**
 * 系统常量
 *
 * @author w
 * @date 2021-03-05
 */
public class BizConstant {

    /**
     * 用户token
     */
    public static final String USER_TOKEN = "user:token:";

    /**
     * 当前cookie名称
     */
    public static final String COOKIE_NAME = "kanfit.session.id";
}
