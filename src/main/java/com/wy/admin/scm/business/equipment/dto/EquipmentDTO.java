package com.wy.admin.scm.business.equipment.dto;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.wy.admin.scm.business.common.component.ActiveFlagConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode
@ColumnWidth(25)
public class EquipmentDTO implements Serializable {
    private static final long serialVersionUID = -355819581877187675L;

    @ExcelIgnore
    private Integer userId;

    @ExcelIgnore
    private Integer id;

    /**
     * 单位ID
     */
    @ExcelIgnore
    private Integer companyId;


    /**
     * 单位名称
     */
    @ExcelProperty("单位名称")
    private String companyName;

    /**
     * 设备生产厂家名称（最大80）
     */
    @ExcelProperty("生产厂家")
    private String factoryInfo;

    /**
     * 设备型号（最大20字节）
     */
    @ExcelProperty("设备型号")
    private String equipmentModel;

    /**
     * 服务器设备名称（最大40字节）
     */
    @ExcelProperty("设备名称")
    private String equipmentName;

    /**
     * 服务器设备编号（最大20字节）
     */
    @ExcelProperty("设备编号")
    private String equipmentNumber;

    /**
     * 出厂日期 格式：YYYY-MM-DD（如2021-01-07）。
     */
    @ExcelProperty("出厂日期")
    private String exFactoryDate;

    /**
     * IP地址
     */
    @ExcelProperty("IP地址")
    private String ipAddress;

    /**
     * 子网掩码
     */
    @ExcelProperty("子网掩码")
    private String subnetMask;

    /**
     * 默认网关
     */
    @ExcelProperty("默认网关")
    private String defaultGateway;

    /**
     * 端口号
     */
    @ExcelProperty("端口号")
    private String portNumber;

    /**
     * 安装日期
     */
    @ExcelProperty("安装日期")
    private String installationDate;

    /**
     * 安装详情
     */
    @ExcelProperty("安装详情")
    private String installationInfo;

    /**
     * 备注
     */
    @ExcelProperty("备注")
    private String remark;

    /**
     * 是否有效, Y:有效 N:无效
     */
    @ExcelProperty(value = "是否启用", converter = ActiveFlagConverter.class)
    private String activeFlag;

    /**
     * 创建日期
     */
    @ExcelProperty("创建日期")
    private String creationDateStr;

    /**
     * 创建人名称
     */
    @ExcelProperty("创建人名称")
    private String createdByName;

    /**
     * 修改日期
     */
    @ExcelProperty("修改日期")
    private String modifyDateStr;

    /**
     * 修改人名称
     */
    @ExcelProperty("修改人名称")
    private String modifiedByName;


}
