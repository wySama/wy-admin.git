package com.wy.admin.scm.business.tool.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wy.admin.scm.business.common.base.constant.StringConstant;
import com.wy.admin.scm.business.common.base.constant.TipsConstant;
import com.wy.admin.scm.business.common.dto.DropdownDTO;
import com.wy.admin.scm.business.common.model.Pagination;
import com.wy.admin.scm.business.tool.dto.DictDataDTO;
import com.wy.admin.scm.business.tool.mapper.CommonDictDataMapper;
import com.wy.admin.scm.business.tool.mapstruct.ToolCovertMapper;
import com.wy.admin.scm.business.tool.model.CommonDictData;
import com.wy.admin.scm.business.tool.service.CommonDictDataService;
import com.wy.admin.scm.business.tool.vo.request.QueryDictDataReqVO;
import com.wy.admin.scm.core.framework.lang.enums.CommonReturnEnum;
import com.wy.admin.scm.core.framework.lang.exception.BusinessException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 字典数据表 服务实现类
 * </p>
 *
 * @author wuyong
 * @since 2022-02-17
 */
@Service
public class CommonDictDataServiceImpl extends ServiceImpl<CommonDictDataMapper, CommonDictData> implements CommonDictDataService {


    @Autowired
    private ToolCovertMapper toolCovertMapper;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * 分页查询数据字典
     */
    public IPage<CommonDictData> queryCommonDictDataPage(QueryDictDataReqVO reqVO) {
        Pagination pagination = reqVO.getPagination();
        Page<CommonDictData> page = new Page<>(pagination.getCurrent(), pagination.getPageSize());

        LambdaQueryWrapper<CommonDictData> queryChainWrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(reqVO.getDictCode())) {
            queryChainWrapper.eq(CommonDictData::getDictCode, reqVO.getDictCode());
        }
        if (StringUtils.isNotBlank(reqVO.getDictType())) {
            queryChainWrapper.eq(CommonDictData::getDictType, reqVO.getDictType());
        }
        if (StringUtils.isNotBlank(reqVO.getActiveFlag())) {
            queryChainWrapper.eq(CommonDictData::getActiveFlag, reqVO.getActiveFlag());
        }
        queryChainWrapper.orderByDesc(CommonDictData::getId);
        return this.baseMapper.selectPage(page, queryChainWrapper);
    }

    public CommonDictData getByDictTypeAndCode(String code, String type) {
        return new LambdaQueryChainWrapper<>(baseMapper)
                .eq(CommonDictData::getDictCode, code)
                .eq(CommonDictData::getDictType, type)
                .eq(CommonDictData::getActiveFlag, StringConstant.Y)
                .one();

    }

    /**
     * 保存数据字典
     */
    public void saveDictData(DictDataDTO reqVO) {
        CommonDictData dbCommonDictData = this.getByDictTypeAndCode(reqVO.getDictCode(), reqVO.getDictType());
        if (null != dbCommonDictData) {
            throw new BusinessException(CommonReturnEnum.CHECK_ERROR, TipsConstant.DICT_DATA_EXIT);
        }
        CommonDictData commonDictData = toolCovertMapper.covertCommonDictData(reqVO);
        this.save(commonDictData);
    }

    /**
     * 修改数据字典
     */
    public void updateDictData(DictDataDTO reqVO) {
        CommonDictData dbCommonDictData = this.getById(reqVO.getId());
        if (null == dbCommonDictData) {
            throw new BusinessException(CommonReturnEnum.CHECK_ERROR, TipsConstant.DICT_NOT_DATA_EXIT);
        }
        CommonDictData commonDictData = toolCovertMapper.covertCommonDictData(reqVO);
        this.updateById(commonDictData);
    }

    /**
     * 获取数据字典详情
     */
    public DictDataDTO getDictDataDetail(Integer id) {
        CommonDictData dbCommonDictData = this.getById(id);
        if (null == dbCommonDictData) {
            throw new BusinessException(CommonReturnEnum.CHECK_ERROR, TipsConstant.DICT_NOT_DATA_EXIT);
        }
        return toolCovertMapper.covertDictDataDTO(dbCommonDictData);
    }


    /**
     * 删除数据字典
     */
    public void deleteDictData(Integer id) {
        this.removeById(id);
    }


    public List<CommonDictData> getCommonDictDataListByType(String type) {
        return new LambdaQueryChainWrapper<>(baseMapper)
                .eq(CommonDictData::getActiveFlag, StringConstant.Y)
                .eq(CommonDictData::getDictType, type)
                .list();


    }


    /**
     * 根据类型获取数据字典下拉数据
     *
     * @param type
     * @return
     */
    public Map<String, String> getDictDataMap(String type) {
        Map<String, String> resMap = (Map) redisTemplate.opsForValue().get("alarmContentMap");
        if (MapUtils.isEmpty(resMap)) {
            resMap = new HashMap<>();
            List<CommonDictData> commonDictDataList = getCommonDictDataListByType(type);
            if (CollectionUtils.isNotEmpty(commonDictDataList)) {
                resMap = commonDictDataList.stream().collect(Collectors.toMap(CommonDictData::getDictCode, CommonDictData::getDictValue));
            }
            redisTemplate.opsForValue().set("alarmContentMap", resMap);
        }
        return resMap;
    }


    /**
     * 根据类型获取数据字典下拉数据
     *
     * @param type
     * @return
     */
    public Map<String, List<DropdownDTO<String>>> getDictDataDropdownList(String type) {
        List<DropdownDTO<String>> dictDataList = (List<DropdownDTO<String>>) redisTemplate.opsForValue().get("alarmContentList");
        if (CollectionUtils.isEmpty(dictDataList)) {
            dictDataList = new ArrayList<>();
            List<CommonDictData> commonDictDataList = getCommonDictDataListByType(type);
            if (CollectionUtils.isNotEmpty(commonDictDataList)) {
                for (CommonDictData item : commonDictDataList) {
                    DropdownDTO<String> dropdownDTO = new DropdownDTO<>();
                    dropdownDTO.setLabel(item.getDictValue());
                    dropdownDTO.setValue(item.getDictCode());
                    dictDataList.add(dropdownDTO);
                }
            }
            redisTemplate.opsForValue().set("alarmContentList", dictDataList);
        }
        Map<String, List<DropdownDTO<String>>> res = new HashMap<>();
        res.put("alarmContentList", dictDataList);
        return res;
    }


}
