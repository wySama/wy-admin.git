package com.wy.admin.scm.business.common.base.constant;

public class StringConstant {

    public static final String Y = "Y";

    public static final String N = "N";

    public static final String OK = "OK";

    public static final String LOGIN_SUCCESS = "登录成功";

    public static final String IOS = "IOS";

    public static final String ANDROID = "ANDROID";

    public static final String ONE = "1";

    public static final String ZERO = "0";

    public static final String FIVE = "5";

    public static final String TWENTY_FIVE = "25";

    public static final String ONE_HUNDRED = "100";

    public static final String WEEK = "WEEK";

    public static final String MONTH = "MONTH";

    public static final String YEAR = "YEAR";

    public static final String EMPTY = "";

}
