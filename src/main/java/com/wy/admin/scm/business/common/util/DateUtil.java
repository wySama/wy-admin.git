package com.wy.admin.scm.business.common.util;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.WeekFields;

public class DateUtil {

    public static final String FMT_DATE_1 = "yyyy-MM-dd";

    public static final String FMT_TIME_1 = "yyyy-MM-dd HH:mm:ss";

    public static final String FMT_TIME_2 = "yyyy-MM-dd HH:mm";

    public static final String FMT_TIME_3 = "yyyyMMddHHmmss";

    public static final String FMT_DATE_3 = "MMddHH";

    public static final String FMT_TIME_4 = "yyyy-MM-dd HH:mm:ss.SSS";

    public static final String FMT_TIME_5 = "yyyyMMdd";

    public static final String FMT_TIME_6 = "yyyy-MM-dd HH";

    public static final String FMT_DATE_7 = "yyyy.MM.dd";

    public static final String MinTime = "T00:00:00";

    public static final String MaxTime = "T23:59:59.999999999";


    /**
     * 转换类型 string to LocalDate
     */
    public static LocalDate parseDate(String localDate, String type) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(type);
        return LocalDate.parse(localDate, formatter);
    }

    /**
     * 转换类型 string to LocalDateTime
     */
    public static LocalDateTime parseDateTime(String time, String type) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(type);
        return LocalDateTime.parse(time, formatter);
    }


    /**
     * 转换类型 string to LocalDateTime
     */
    public static String getDateTimeString(LocalDateTime time) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FMT_TIME_1);
        return formatter.format(time);
    }

    /**
     * 转换类型 LocalDate to String
     *
     * @param date date
     * @return LocalDate
     */
    public static String getDateString(LocalDate date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FMT_DATE_1);
        return formatter.format(date);
    }



    /**
     * 功能描述 秒转时分秒
     *
     * @param sumSecond 总秒数
     * @return java.lang.String 返回 “ 01:01:01 ” 格式的时间
     * @author qinda
     * @date 2020/10/29
     */
    public static String sumSecondToTime(int sumSecond) {
        if (sumSecond <= 0) {
            return "00:00:00";
        }
        int h = sumSecond / 3600;
        int m = (sumSecond - h * 3600) / 60;
        int s = sumSecond - h * 3600 - m * 60;
        String time = "%02d:%02d:%02d";
        time = String.format(time, h, m, s);
        return time;
    }

    public static LocalDateTime getTodayMinTime() {
        return LocalDateTime.now().with(LocalTime.MIN);
    }

    public static LocalDateTime getTodayMaxTime() {
        return LocalDateTime.now().with(LocalTime.MAX);
    }


    /**
     * @Description:本周的开始时间
     * @Param: [today, isFirst: true 表示开始时间，false表示结束时间]
     */
    public static LocalDateTime getStartOrEndDayTimeOfWeek(LocalDate today, boolean isFirst) {
        String time = MinTime;
        LocalDate resDate = LocalDate.now();
        if (today == null) {
            today = resDate;
        }
        DayOfWeek week = today.getDayOfWeek();
        int value = week.getValue();
        if (isFirst) {
            resDate = today.minusDays(value - 1);
        } else {
            resDate = today.plusDays(7 - value);
            time = MaxTime;
        }
        return LocalDateTime.parse(resDate.toString() + time);
    }


    /**
     * @Description:本周的开始时间
     * @Param: [today, isFirst: true 表示开始时间，false表示结束时间]
     */
    public static LocalDate getStartOrEndDayOfWeek(LocalDate today, boolean isFirst) {
        LocalDate resDate = LocalDate.now();
        if (today == null) {
            today = resDate;
        }
        DayOfWeek week = today.getDayOfWeek();
        int value = week.getValue();
        if (isFirst) {
            resDate = today.minusDays(value - 1);
        } else {
            resDate = today.plusDays(7 - value);

        }
        return resDate;
    }


    /**
     * 获取当前时间第几周
     */
    public static Integer getWeeks(LocalDateTime currentDate) {
        WeekFields weekFields = WeekFields.of(DayOfWeek.MONDAY, 1);
        return currentDate.get(weekFields.weekOfYear());
    }


}