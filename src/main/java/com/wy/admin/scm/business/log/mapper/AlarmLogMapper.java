package com.wy.admin.scm.business.log.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wy.admin.scm.business.log.dto.AlarmLogDTO;
import com.wy.admin.scm.business.log.model.AlarmLog;
import com.wy.admin.scm.business.log.vo.request.AlarmLogReqVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 告警日志表 Mapper 接口
 * </p>
 *
 * @author wuyong
 * @since 2022-02-25
 */
public interface AlarmLogMapper extends BaseMapper<AlarmLog> {

    IPage<AlarmLogDTO> queryAlarmLogPage(@Param("page") Page<AlarmLogDTO> page, @Param("req") AlarmLogReqVO reqVO);

    List<AlarmLogDTO> queryAlarmLogList(@Param("req") AlarmLogReqVO reqVO);


}
