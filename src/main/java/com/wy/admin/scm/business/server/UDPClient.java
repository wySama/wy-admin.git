package com.wy.admin.scm.business.server;

import com.wy.admin.scm.business.common.base.enums.DataFrameTypeEnum;
import com.wy.admin.scm.business.common.util.UdpUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/*
 * 客户端
 */
@Slf4j
public class UDPClient {

    /**
     * 0：设备信息查询；
     * 1：配置信息查询；
     * 2：工作状态查询；
     * 3：工作日志查询。
     */
    public static void main(String[] args) throws IOException {
        // 1.创建服务器端DatagramSocket，指定端口
        DatagramSocket socket = new DatagramSocket(8800);
        // 2.创建数据报，用于接收客户端发送的数据
        byte[] data = new byte[1024];// 创建字节数组，指定接收的数据包的大小
        DatagramPacket packet = new DatagramPacket(data, data.length);
        // 3.接收客户端发送的数据
        socket.receive(packet);// 此方法在接收到数据报之前会一直阻塞
        // 4.读取数据
        String info = new String(data, 0, packet.getLength());
        log.info("我是客户端，服务器说：" + info);


        String sendMsg = "115江南制造长1112设备名称410012021-01-07";
        String request = DataFrameTypeEnum.EQUIPMENT_INFO_REPORT.getCode() + sendMsg.getBytes().length + sendMsg;
        request = request + UdpUtil.checkXor(request.getBytes());
        log.info("query request : " + request);

        // 1.定义客户端的地址、端口号、数据
        InetAddress address = packet.getAddress();
        int port = packet.getPort();
        byte[] data2 = request.getBytes();
        // 2.创建数据报，包含响应的数据信息
        DatagramPacket packet2 = new DatagramPacket(data2, data2.length, address, port);
        // 3.响应客户端
        socket.send(packet2);
        // 4.关闭资源
        socket.close();

    }
}
