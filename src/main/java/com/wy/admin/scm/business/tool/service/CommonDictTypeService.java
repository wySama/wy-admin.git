package com.wy.admin.scm.business.tool.service;

import com.wy.admin.scm.business.tool.model.CommonDictType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 字典类型表 服务类
 * </p>
 *
 * @author wuyong
 * @since 2022-02-17
 */
public interface CommonDictTypeService extends IService<CommonDictType> {

}
