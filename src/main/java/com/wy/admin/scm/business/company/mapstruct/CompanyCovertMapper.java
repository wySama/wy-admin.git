package com.wy.admin.scm.business.company.mapstruct;


import com.wy.admin.scm.business.common.component.LocalDateTimeFormat;
import com.wy.admin.scm.business.company.dto.CompanyDTO;
import com.wy.admin.scm.business.company.model.Company;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = LocalDateTimeFormat.class)
public interface CompanyCovertMapper {


    CompanyDTO covertCompanyDTO(Company company);

    @Mapping(target = "modifiedBy", source = "userId")
    Company covertCompany(CompanyDTO companyDTO);
}
