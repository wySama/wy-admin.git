package com.wy.admin.scm.business.tool.service.impl;

import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.wy.admin.scm.business.common.base.constant.StringConstant;
import com.wy.admin.scm.business.common.dto.DropdownDTO;
import com.wy.admin.scm.business.tool.mapstruct.ToolCovertMapper;
import com.wy.admin.scm.business.tool.model.CommonDictType;
import com.wy.admin.scm.business.tool.mapper.CommonDictTypeMapper;
import com.wy.admin.scm.business.tool.service.CommonDictTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wy.admin.scm.business.tool.vo.request.AddCommonDictTypeReqVO;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 字典类型表 服务实现类
 * </p>
 *
 * @author wuyong
 * @since 2022-02-17
 */
@Service
public class CommonDictTypeServiceImpl extends ServiceImpl<CommonDictTypeMapper, CommonDictType> implements CommonDictTypeService {

    @Autowired
    private ToolCovertMapper toolCovertMapper;

    /**
     * 字典类型DropdownDTO
     */
    public Map<String, List<DropdownDTO<String>>> getDictTypeDropdownList() {
        Map<String, List<DropdownDTO<String>>> res = new HashMap<>();
        List<CommonDictType> commonDictTypeList = new LambdaQueryChainWrapper<>(baseMapper).eq(CommonDictType::getActiveFlag, StringConstant.Y)
                .orderByDesc(CommonDictType::getId)
                .list();
        List<DropdownDTO<String>> dropdownDTOList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(commonDictTypeList)) {
            commonDictTypeList.forEach(item -> {
                DropdownDTO<String> dropdownDTO = new DropdownDTO<>();
                dropdownDTO.setLabel(item.getDictName());
                dropdownDTO.setValue(item.getDictType());
                dropdownDTOList.add(dropdownDTO);
            });
        }
        res.put("dictTypeList", dropdownDTOList);
        return res;

    }

    /**
     * 保存字典类型
     */
    public void saveDictType(AddCommonDictTypeReqVO reqVO){
        CommonDictType commonDictType =    toolCovertMapper.covertCommonDictType(reqVO );
        this.save(commonDictType);

    }


}
