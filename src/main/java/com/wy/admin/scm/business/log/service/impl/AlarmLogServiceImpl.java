package com.wy.admin.scm.business.log.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.conditions.update.LambdaUpdateChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wy.admin.scm.business.common.model.Pagination;
import com.wy.admin.scm.business.common.service.impl.CommonService;
import com.wy.admin.scm.business.log.dto.AlarmLogDTO;
import com.wy.admin.scm.business.log.dto.UpdateAlarmLogDTO;
import com.wy.admin.scm.business.log.mapper.AlarmLogMapper;
import com.wy.admin.scm.business.log.model.AlarmLog;
import com.wy.admin.scm.business.log.service.AlarmLogService;
import com.wy.admin.scm.business.log.vo.request.AlarmLogReqVO;
import com.wy.admin.scm.business.tool.service.impl.CommonDictDataServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 告警日志表 服务实现类
 * </p>
 *
 * @author wuyong
 * @since 2022-02-25
 */
@Service
public class AlarmLogServiceImpl extends ServiceImpl<AlarmLogMapper, AlarmLog> implements AlarmLogService {

    @Autowired
    private CommonDictDataServiceImpl commonDictDataService;

    @Autowired
    private CommonService commonService;

    /**
     * 分页查询告警日志
     */
    public IPage<AlarmLogDTO> queryAlarmLogPage(AlarmLogReqVO reqVO) {
        Pagination pagination = reqVO.getPagination();
        Page<AlarmLogDTO> page = new Page<>(pagination.getCurrent(), pagination.getPageSize());
        IPage<AlarmLogDTO> alarmLogDTOIPage = this.baseMapper.queryAlarmLogPage(page, reqVO);
        this.setAlarmContentText(alarmLogDTOIPage.getRecords());
        return alarmLogDTOIPage;
    }

    /**
     * 获取导出数据
     */
    public List<AlarmLogDTO> queryAlarmLogList(AlarmLogReqVO reqVO) {
        List<AlarmLogDTO> alarmLogDTOList = this.baseMapper.queryAlarmLogList(reqVO);
        this.setAlarmContentText(alarmLogDTOList);
        return alarmLogDTOList;
    }


    /**
     * 导出告警日志
     */
    public void exportAlarmLog(HttpServletResponse response, AlarmLogReqVO reqVO) {
        List<AlarmLogDTO> list = queryAlarmLogList(reqVO);
        commonService.commonExport(response, AlarmLogDTO.class, list);
    }


    private void setAlarmContentText(List<AlarmLogDTO> alarmLogDTOList) {
        if (CollectionUtils.isNotEmpty(alarmLogDTOList)) {
            Map<String, String> alarmContentMap = commonDictDataService.getDictDataMap("ALARM_CONTENT");
            for (AlarmLogDTO dto : alarmLogDTOList) {
                String alarmContent = MapUtils.getString(alarmContentMap, dto.getAlarmContent().toString());
                dto.setAlarmContentText(alarmContent);
            }
        }
    }

    public AlarmLog handleAlarm(UpdateAlarmLogDTO reqVO) {
        new LambdaUpdateChainWrapper<>(baseMapper)
                .set(AlarmLog::getRemark, reqVO.getRemark())
                .set(AlarmLog::getModifiedBy, reqVO.getUserId())
                .set(AlarmLog::getModifyDate, LocalDateTime.now())
                .eq(AlarmLog::getId, reqVO.getId())
                .update();

        return this.getById(reqVO.getId());
    }

}
