package com.wy.admin.scm.business.common.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel(value = "分页返回参数")
public class PageResult<T> implements Serializable {
    private static final long serialVersionUID = -5446804970818293855L;

    @ApiModelProperty(value = "集合")
    private List<T> records;

    @ApiModelProperty(value = "分页参数")
    private Pagination pagination;

    public PageResult() {
    }

    public PageResult(List<T> records) {
        this.records = records;
    }

    public PageResult(List<T> records, Pagination pagination) {
        this.records = records;
        this.pagination = pagination;
    }
}
