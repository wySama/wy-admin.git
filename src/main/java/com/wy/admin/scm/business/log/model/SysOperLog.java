package com.wy.admin.scm.business.log.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>
 * 操作日志表
 * </p>
 *
 * @author wuyong
 * @since 2022-02-18
 */
@Data
@TableName("admin.sys_oper_log")
public class SysOperLog extends Model<SysOperLog> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 操作人员ID
     */
    private Integer operId;

    /**
     * 操作人员名称
     */
    private String operName;

    /**
     * 单位ID
     */
    private Integer companyId;

    /**
     * 模块标题
     */
    private String title;

    /**
     * 业务类型（0其它 1新增 2修改 3删除）
     */
    private Integer businessType;

    /**
     * 操作状态（0正常 1失败）
     */
    private Integer operStatus;

    /**
     * 提示消息
     */
    private String msg;

    /**
     * 操作时间
     */
    private LocalDateTime operDate;

    /**
     * IP地址
     */
    private String operIp;

    /**
     * 地点
     */
    private String operLocation;

    /**
     * 浏览器类型
     */
    private String browser;

    /**
     * 操作系统
     */
    private String os;

    /**
     * 请求参数
     */
    private String request;

    /**
     * 返回参数
     */
    private String response;


}
