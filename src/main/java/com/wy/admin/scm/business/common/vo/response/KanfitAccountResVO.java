package com.wy.admin.scm.business.common.vo.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


@Data
@ApiModel(value = "官方账号")
public class KanfitAccountResVO implements Serializable {
    private static final long serialVersionUID = -5237096383202323866L;

    @ApiModelProperty(value = "官方微信号")
    private String kanfitWechatId;


}
