package com.wy.admin.scm.business.log.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class UpdateAlarmLogDTO implements Serializable {
    private static final long serialVersionUID = -4220389250938578351L;

    private Integer userId;
    private Integer id;

    /**
     * 备注
     */
    private String remark;
}
