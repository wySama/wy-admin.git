package com.wy.admin.scm.business.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wy.admin.scm.business.system.model.SysUserRole;

/**
 * <p>
 * 用户关联角色表 Mapper 接口
 * </p>
 *
 * @author wuyong
 * @since 2021-12-16
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
