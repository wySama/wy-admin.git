package com.wy.admin.scm.business.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wy.admin.scm.business.system.model.SysMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 菜单表 Mapper 接口
 * </p>
 *
 * @author wuyong
 * @since 2021-12-16
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    List<SysMenu> selectByUserId(@Param("userId") Integer userId);

}
