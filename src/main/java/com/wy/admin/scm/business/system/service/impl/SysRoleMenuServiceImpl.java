package com.wy.admin.scm.business.system.service.impl;

import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.conditions.update.LambdaUpdateChainWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wy.admin.scm.business.system.mapper.SysRoleMenuMapper;
import com.wy.admin.scm.business.system.model.SysRoleMenu;
import com.wy.admin.scm.business.system.service.SysRoleMenuService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 角色关联菜单表 服务实现类
 * </p>
 *
 * @author wuyong
 * @since 2021-12-16
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements SysRoleMenuService {


    public void saveSysRoleMenu(Integer roleId, List<Integer> menuIds) {
        if (CollectionUtils.isEmpty(menuIds)) {
            return;
        }
        List<SysRoleMenu> sysRoleMenus = new ArrayList<>();
        menuIds.forEach(item -> {
            SysRoleMenu sysRoleMenu = new SysRoleMenu();
            sysRoleMenu.setRoleId(roleId);
            sysRoleMenu.setMenuId(item);
            sysRoleMenus.add(sysRoleMenu);
        });
        this.saveBatch(sysRoleMenus);
    }


    /**
     * 修改角色关联菜单
     *
     * @param roleId
     * @param menuIds
     */
    public void updateSysRoleMenu(Integer roleId, List<Integer> menuIds) {
        //删除原来的用户角色
        this.removeRoleMenuByRoleId(roleId);
        this.saveSysRoleMenu(roleId, menuIds);
    }


    public void removeRoleMenuByRoleId(Integer roleId) {
        new LambdaUpdateChainWrapper<>(baseMapper)
                .eq(SysRoleMenu::getRoleId, roleId)
                .remove();

    }


    /**
     * 根据角色获取菜单id集合
     *
     * @param roleId
     * @return
     */
    public List<Integer> getMenusByRoleId(Integer roleId) {
        List<Integer> menuIds = new ArrayList<>();
        List<SysRoleMenu> sysRoleMenus = new LambdaQueryChainWrapper<>(baseMapper)
                .eq(SysRoleMenu::getRoleId, roleId)
                .list();

        if (CollectionUtils.isNotEmpty(sysRoleMenus)) {
            menuIds = sysRoleMenus.stream().map(SysRoleMenu::getMenuId).distinct().collect(Collectors.toList());
        }

        return menuIds;
    }


}
