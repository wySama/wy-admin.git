package com.wy.admin.scm.business.log.mapper;

import com.wy.admin.scm.business.log.model.SysLoginLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 登录日志表 Mapper 接口
 * </p>
 *
 * @author wuyong
 * @since 2022-02-17
 */
public interface SysLoginLogMapper extends BaseMapper<SysLoginLog> {

}
