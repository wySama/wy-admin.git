package com.wy.admin.scm.business.log.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wy.admin.scm.business.common.model.Pagination;
import com.wy.admin.scm.business.log.mapper.SysLoginLogMapper;
import com.wy.admin.scm.business.log.model.SysLoginLog;
import com.wy.admin.scm.business.log.service.SysLoginLogService;
import com.wy.admin.scm.business.log.vo.request.SysLoginLogReqVO;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 登录日志表 服务实现类
 * </p>
 *
 * @author wuyong
 * @since 2022-02-17
 */
@Service
public class SysLoginLogServiceImpl extends ServiceImpl<SysLoginLogMapper, SysLoginLog> implements SysLoginLogService {


    /**
     * 分页查询登录日志
     */
    public IPage<SysLoginLog> querySysLoginLogPage(SysLoginLogReqVO reqVO) {
        Pagination pagination = reqVO.getPagination();
        Page<SysLoginLog> page = new Page<>(pagination.getCurrent(), pagination.getPageSize());

        LambdaQueryWrapper<SysLoginLog> queryChainWrapper = new LambdaQueryWrapper<>();

        if (null != reqVO.getLoginStatus()) {
            queryChainWrapper.eq(SysLoginLog::getLoginStatus, reqVO.getLoginStatus());
        }
        if (StringUtils.isNotBlank(reqVO.getLoginName())) {
            queryChainWrapper.eq(SysLoginLog::getLoginName, reqVO.getLoginName());
        }
        if (StringUtils.isNotBlank(reqVO.getLoginIp())) {
            queryChainWrapper.eq(SysLoginLog::getLoginIp, reqVO.getLoginIp());
        }
        if (StringUtils.isNotBlank(reqVO.getLoginName())) {
            queryChainWrapper.eq(SysLoginLog::getLoginName, reqVO.getLoginName());
        }
        if (StringUtils.isNotBlank(reqVO.getStartTime())) {
            queryChainWrapper.gt(SysLoginLog::getLoginTime, reqVO.getStartTime());
        }
        if (StringUtils.isNotBlank(reqVO.getEndTime())) {
            queryChainWrapper.lt(SysLoginLog::getLoginTime, reqVO.getEndTime());
        }

        queryChainWrapper.orderByDesc(SysLoginLog::getId);
        return this.baseMapper.selectPage(page, queryChainWrapper);
    }


    /**
     * 删除登录日志
     */
    public void batchRemove(SysLoginLogReqVO reqVO) {
        if (CollectionUtils.isNotEmpty(reqVO.getIds())) {
            this.removeByIds(reqVO.getIds());
        }
    }


}
