package com.wy.admin.scm.business.tool.mapstruct;

import com.wy.admin.scm.business.tool.dto.DictDataDTO;
import com.wy.admin.scm.business.tool.model.CommonDictData;
import com.wy.admin.scm.business.tool.model.CommonDictType;
import com.wy.admin.scm.business.tool.vo.request.AddCommonDictTypeReqVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface ToolCovertMapper {


    @Mappings(
            {
                    @Mapping(target = "createdBy", source = "userId"),
                    @Mapping(target = "modifiedBy", source = "userId")
            }
    )
    CommonDictData covertCommonDictData(DictDataDTO reqVO);


    DictDataDTO covertDictDataDTO(CommonDictData commonDictData);


    @Mappings(
            {
                    @Mapping(target = "createdBy", source = "userId"),
                    @Mapping(target = "modifiedBy", source = "userId")
            }
    )
    CommonDictType covertCommonDictType(AddCommonDictTypeReqVO reqVO);








}
