package com.wy.admin.scm.business.company.vo.request;

import com.wy.admin.scm.business.common.model.Pagination;
import lombok.Data;

import java.io.Serializable;

@Data
public class CompanyReqVO implements Serializable {

    private static final long serialVersionUID = 1074521810541214112L;
    /**
     * 单位名称
     */
    private String companyName;

    /**
     * 单位简称
     */
    private String companyAbbreviation;

    private String activeFlag;

    private Pagination pagination;

}
