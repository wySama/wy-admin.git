package com.wy.admin.scm.business.system.vo.request;

import lombok.Data;

/**
 * 用户查询DTO
 *
 * @author w
 * @date 2021-03-05
 */
@Data
public class SaveSysMenuReqVO {

    private Integer userId;

    /**
     * 名称
     */
    private String menuName;

    /**
     * URL
     */
    private String url;

    /**
     * 父ID
     */
    private Integer parentId;

    /**
     * 类型(1菜单 2按钮)
     */
    private Integer menuType;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 权限标识
     */
    private String perms;

    /**
     * 图标
     */
    private String icon;

    /**
     * 备注
     */
    private String remark;

    /**
     * 是否有效, Y:有效 N:无效
     */
    private String activeFlag;
}
