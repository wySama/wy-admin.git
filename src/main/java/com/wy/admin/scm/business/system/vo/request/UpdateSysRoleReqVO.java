package com.wy.admin.scm.business.system.vo.request;

import lombok.Data;

import java.util.List;

/**
 * 用户查询DTO
 *
 * @author w
 * @date 2021-03-05
 */
@Data
public class UpdateSysRoleReqVO {

    private Integer id;

    private Integer userId;

    /**
     * 名称
     */
    private String roleName;


    /**
     * 角色标识
     */
    private String roleCode;

    /**
     * 备注
     */
    private String remark;

    /**
     * 是否有效, Y:有效 N:无效
     */
    private String activeFlag;


    private List<Integer> menuIds;
}
