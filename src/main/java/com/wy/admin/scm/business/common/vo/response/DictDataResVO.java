package com.wy.admin.scm.business.common.vo.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


@Data
@ApiModel(value = "数据字典返回信息")
public class DictDataResVO implements Serializable {
    private static final long serialVersionUID = -5237096383202323866L;

    @ApiModelProperty(value = "课程难度")
    private List<String> courseHardLevelList;

    @ApiModelProperty(value = "排序")
    private List<String> courseSortList;

    @ApiModelProperty(value = "部位")
    private List<String> bodyPartsList;

    @ApiModelProperty(value = "症状")
    private List<String> symptomList;

    @ApiModelProperty(value = "目标")
    private List<String> targetTypeList;


}
