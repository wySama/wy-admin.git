package com.wy.admin.scm.business.system.controller;

import com.alibaba.fastjson.JSONObject;
import com.wy.admin.scm.business.system.vo.request.LoginReqVO;
import com.wy.admin.scm.core.framework.model.JsonResult;
import com.wy.admin.scm.core.framework.shiro.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginRestController {


    @Autowired
    private LoginService loginService;

    @PostMapping("/login")
    public JsonResult<JSONObject> login(@RequestBody LoginReqVO reqVO) {
        loginService.userLogin(reqVO.getLoginName(), reqVO.getPwd());
        return new JsonResult<>(new JSONObject());
    }

    @PostMapping("/logout")
    public JsonResult<JSONObject> logout() {
        return new JsonResult<>(new JSONObject());
    }


}
