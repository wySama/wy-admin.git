package com.wy.admin.scm.business.equipment.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wy.admin.scm.business.equipment.dto.EquipmentDTO;
import com.wy.admin.scm.business.equipment.model.Equipment;
import com.wy.admin.scm.business.equipment.vo.request.EquipmentReqVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 设备信息表 Mapper 接口
 * </p>
 *
 * @author wuyong
 * @since 2022-02-16
 */
public interface EquipmentMapper extends BaseMapper<Equipment> {

    IPage<EquipmentDTO> queryEquipmentPage(@Param("page") Page<EquipmentDTO> page, @Param("req") EquipmentReqVO reqVO);

    List<EquipmentDTO> queryEquipmentList(@Param("req") EquipmentReqVO reqVO);


}
