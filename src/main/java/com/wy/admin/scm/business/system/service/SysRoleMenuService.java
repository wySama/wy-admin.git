package com.wy.admin.scm.business.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wy.admin.scm.business.system.model.SysRoleMenu;

/**
 * <p>
 * 角色关联菜单表 服务类
 * </p>
 *
 * @author wuyong
 * @since 2021-12-16
 */
public interface SysRoleMenuService extends IService<SysRoleMenu> {

}
