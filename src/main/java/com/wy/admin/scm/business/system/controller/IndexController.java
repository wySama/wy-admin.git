package com.wy.admin.scm.business.system.controller;

import com.wy.admin.scm.business.common.base.constant.TemplatesConstant;
import com.wy.admin.scm.business.system.service.impl.SysUserServiceImpl;
import com.wy.admin.scm.business.system.vo.response.LoginResVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class IndexController {


    @Autowired
    private SysUserServiceImpl sysUserService;

    @GetMapping(value = "/unauthorized")
    public String unauthorized() {
        return TemplatesConstant.LOGIN;
    }

    @GetMapping(value = "/login")
    public String login() {
        return TemplatesConstant.LOGIN;
    }


    @RequestMapping("/")
    public String login(HttpServletRequest request, Model model) {
        LoginResVO loginResVO = sysUserService.getLoginResVO();
        model.addAttribute("loginResVO", loginResVO);
        return "home";
    }

    @GetMapping("/{var1}/{var2}/index")
    public String index(HttpServletRequest request, @PathVariable("var1") String var1, @PathVariable("var2") String var2) {
        return var1 + "/" + var2;
    }
}
