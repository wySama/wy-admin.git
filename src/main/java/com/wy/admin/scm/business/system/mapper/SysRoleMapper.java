package com.wy.admin.scm.business.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wy.admin.scm.business.system.model.SysRole;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author wuyong
 * @since 2021-12-16
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
