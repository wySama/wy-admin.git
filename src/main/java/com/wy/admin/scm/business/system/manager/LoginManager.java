package com.wy.admin.scm.business.system.manager;//package com.wy.admin.scm.business.system.manager;
//
//import com.alibaba.fastjson.JSON;
//import com.wy.admin.scm.business.common.model.UserToken;
//import com.wy.admin.scm.business.system.dto.SysMenuDTO;
//import com.wy.admin.scm.business.system.dto.SysUserDetailDTO;
//import com.wy.admin.scm.business.system.mapstruct.SystemCovertMapper;
//import com.wy.admin.scm.business.system.model.SysUser;
//import com.wy.admin.scm.business.system.service.impl.SysMenuServiceImpl;
//import com.wy.admin.scm.business.system.service.impl.SysUserServiceImpl;
//import com.wy.admin.scm.business.system.vo.request.LoginReqVO;
//import com.wy.admin.scm.business.system.vo.response.LoginResVO;
//import com.wy.admin.scm.common.base.BizConstant;
//import com.wy.admin.scm.common.base.TipsConstant;
//import com.wy.admin.scm.common.util.CookieUtils;
//import com.wy.admin.scm.common.util.JwtUtils;
//import com.wy.admin.scm.common.util.RedisUtils;
//import com.wy.admin.scm.core.framework.lang.enums.CommonReturnEnum;
//import com.wy.admin.scm.core.framework.lang.exception.BusinessException;
//import org.mindrot.jbcrypt.BCrypt;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.List;
//
//@Service
//public class LoginManager {
//
//    @Autowired
//    private SysUserServiceImpl sysUserService;
//
//    @Autowired
//    private SystemCovertMapper systemCovertMapper;
//
//    @Autowired
//    private SysMenuServiceImpl sysMenuService;
//
//
//    public LoginResVO login(LoginReqVO reqVO, HttpServletResponse response) {
//        SysUser sysUser = sysUserService.selectByLoginName(reqVO.getLoginName());
//        if (null == sysUser) {
//            throw new BusinessException(CommonReturnEnum.CHECK_ERROR, TipsConstant.SYSUSER_NOT_EXIT);
//        }
//
//        if (!BCrypt.checkpw(reqVO.getPwd(), sysUser.getPwd())) {
//            throw new BusinessException(CommonReturnEnum.CHECK_ERROR, TipsConstant.PWD_ERROR);
//        }
//        LoginResVO loginResVO = new LoginResVO();
//
//        SysUserDetailDTO sysUserDetailVO = systemCovertMapper.covertSysUserDetailVO(sysUser);
//        loginResVO.setSysUser(sysUserDetailVO);
//
//        List<SysMenuDTO> sysMenuVOS = sysMenuService.selectByUserId(sysUser.getId());
//        loginResVO.setSysMenu(sysMenuVOS);
//
//        //生成token
//        UserToken userToken = new UserToken();
//        userToken.setId(sysUser.getId());
//        userToken.setTimestamp(System.currentTimeMillis());
//        loginResVO.setToken(JwtUtils.build(JSON.toJSONString(userToken)));
//
//        CookieUtils.setCookie(response, BizConstant.COOKIE_NAME, loginResVO.getToken());
//        //用户信息写入redis
//        RedisUtils.set(BizConstant.USER_TOKEN + loginResVO.getToken(), userToken, 1800);
//
//        return loginResVO;
//    }
//
//    public void logout(HttpServletRequest request, HttpServletResponse response) {
//        RedisUtils.delete(BizConstant.USER_TOKEN + request.getHeader("token"));
//        CookieUtils.deleteCookie(response, BizConstant.COOKIE_NAME);
//    }
//}
