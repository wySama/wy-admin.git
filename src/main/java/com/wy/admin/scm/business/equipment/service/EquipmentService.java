package com.wy.admin.scm.business.equipment.service;

import com.wy.admin.scm.business.equipment.model.Equipment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 设备信息表 服务类
 * </p>
 *
 * @author wuyong
 * @since 2022-02-16
 */
public interface EquipmentService extends IService<Equipment> {

}
