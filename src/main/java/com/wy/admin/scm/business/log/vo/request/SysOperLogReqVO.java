package com.wy.admin.scm.business.log.vo.request;

import com.wy.admin.scm.business.common.model.Pagination;
import lombok.Data;

@Data
public class SysOperLogReqVO {
    /**
     * 操作人员名称
     */
    private String operName;

    /**
     * 单位ID
     */
    private Integer companyId;


    /**
     * 模块标题
     */
    private String title;

    /**
     * 操作状态（0异常 1正常 ）
     */
    private Integer operStatus;

    /**
     * 开始时间
     */
    private String startTime;

    /**
     * 结束时间
     */
    private String endTime;

    private Pagination pagination;



}
