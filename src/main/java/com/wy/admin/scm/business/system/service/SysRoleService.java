package com.wy.admin.scm.business.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wy.admin.scm.business.system.model.SysRole;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author wuyong
 * @since 2021-12-16
 */
public interface SysRoleService extends IService<SysRole> {

}
