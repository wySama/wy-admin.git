package com.wy.admin.scm.business.tool.service;

import com.wy.admin.scm.business.tool.model.CommonDictData;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 字典数据表 服务类
 * </p>
 *
 * @author wuyong
 * @since 2022-02-17
 */
public interface CommonDictDataService extends IService<CommonDictData> {

}
