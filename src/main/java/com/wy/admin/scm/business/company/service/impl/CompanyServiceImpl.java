package com.wy.admin.scm.business.company.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wy.admin.scm.business.common.base.constant.StringConstant;
import com.wy.admin.scm.business.common.dto.DropdownDTO;
import com.wy.admin.scm.business.common.model.Pagination;
import com.wy.admin.scm.business.common.service.impl.CommonService;
import com.wy.admin.scm.business.company.dto.CompanyDTO;
import com.wy.admin.scm.business.company.mapper.CompanyMapper;
import com.wy.admin.scm.business.company.mapstruct.CompanyCovertMapper;
import com.wy.admin.scm.business.company.model.Company;
import com.wy.admin.scm.business.company.service.CompanyService;
import com.wy.admin.scm.business.company.vo.request.CompanyReqVO;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 单位表 服务实现类
 * </p>
 *
 * @author wuyong
 * @since 2022-02-16
 */
@Service
public class CompanyServiceImpl extends ServiceImpl<CompanyMapper, Company> implements CompanyService {

    @Autowired
    private CompanyCovertMapper companyCovertMapper;


    @Autowired
    private CommonService commonService;


    /**
     * 分页获取单位
     */
    public IPage<CompanyDTO> queryCompanyPage(CompanyReqVO reqVO) {
        Pagination pagination = reqVO.getPagination();
        Page<CompanyDTO> page = new Page<>(pagination.getCurrent(), pagination.getPageSize());
        return this.baseMapper.queryCompanyPage(page, reqVO);
    }

    /**
     * 获取导出数据
     */
    private List<CompanyDTO> queryCompanyList(CompanyReqVO reqVO) {
        return this.baseMapper.queryCompanyList(reqVO);
    }

    /**
     * 导出单位
     */
    public void exportCompany(HttpServletResponse response, CompanyReqVO reqVO) {
        List<CompanyDTO> list = queryCompanyList(reqVO);
        commonService.commonExport(response, CompanyDTO.class, list);
    }


    public CompanyDTO getCompanyDetail(Integer id) {
        Company company = this.getById(id);
        return companyCovertMapper.covertCompanyDTO(company);
    }

    public Company saveCompany(CompanyDTO companyDTO) {
        Company company = companyCovertMapper.covertCompany(companyDTO);
        company.setCreatedBy(companyDTO.getUserId());
        this.save(company);
        return company;
    }


    public Company updateCompany(CompanyDTO companyDTO) {
        Company company = companyCovertMapper.covertCompany(companyDTO);
        this.updateById(company);
        return company;
    }


    public void deleteCompany(Integer id) {
        //TODO
        this.removeById(id);
    }


    private List<Company> getCompanyList() {
        return new LambdaQueryChainWrapper<>(baseMapper)
                .eq(Company::getActiveFlag, StringConstant.Y)
                .list();
    }

    /**
     * 获取所有角色下拉框选项
     */
    public Map<String, List<DropdownDTO<Integer>>> getCompanyDropdownList() {
        Map<String, List<DropdownDTO<Integer>>> res = new HashMap<>();
        List<DropdownDTO<Integer>> companyList = new ArrayList<>();
        List<Company> companys = getCompanyList();
        if (CollectionUtils.isNotEmpty(companys)) {
            companys.forEach(item -> {
                DropdownDTO<Integer> dropdownDTO = new DropdownDTO<>();
                dropdownDTO.setLabel(item.getCompanyAbbreviation());
                dropdownDTO.setValue(item.getId());
                companyList.add(dropdownDTO);

            });
        }
        res.put("companyList", companyList);
        return res;
    }


}
