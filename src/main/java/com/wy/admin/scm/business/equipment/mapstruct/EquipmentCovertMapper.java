package com.wy.admin.scm.business.equipment.mapstruct;

import com.wy.admin.scm.business.common.component.LocalDateTimeFormat;
import com.wy.admin.scm.business.equipment.dto.EquipmentDTO;
import com.wy.admin.scm.business.equipment.model.Equipment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = LocalDateTimeFormat.class)
public interface EquipmentCovertMapper {

    @Mapping(target = "exFactoryDate", source = "exFactoryDate", qualifiedByName = "localDateToString")
    @Mapping(target = "installationDate", source = "installationDate", qualifiedByName = "localDateToString")
    EquipmentDTO covertEquipmentDTO(Equipment equipment);

    @Mapping(target = "modifiedBy", source = "userId")
    @Mapping(target = "exFactoryDate", source = "exFactoryDate", qualifiedByName = "stringToLocalDate")
    @Mapping(target = "installationDate", source = "installationDate", qualifiedByName = "stringToLocalDate")
    Equipment covertEquipment(EquipmentDTO equipmentDTO);

}
