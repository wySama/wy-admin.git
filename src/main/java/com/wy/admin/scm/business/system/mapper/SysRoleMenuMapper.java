package com.wy.admin.scm.business.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wy.admin.scm.business.system.model.SysRoleMenu;

/**
 * <p>
 * 角色关联菜单表 Mapper 接口
 * </p>
 *
 * @author wuyong
 * @since 2021-12-16
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}
