package com.wy.admin.scm.business.log.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>
 * 告警日志表
 * </p>
 *
 * @author wuyong
 * @since 2022-02-25
 */
@Data
@TableName("admin.alarm_log")
public class AlarmLog extends Model<AlarmLog> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 单位ID
     */
    private Integer companyId;

    /**
     * 设备ID
     */
    private Integer equipmentId;

    /**
     * 告警的序号
     */
    private Integer alarmId;

    /**
     * 告警时刻
     */
    private LocalDateTime alarmDate;

    /**
     * 告警类型（0:撤销告警 1:产生告警）
     */
    private Integer alarmType;

    /**
     * 告警内容
     */
    private Integer alarmContent;

    /**
     * 备注
     */
    private String remark;

    /**
     * 是否有效, Y:有效 N:无效
     */
    private String activeFlag;

    /**
     * 创建日期
     */
    private LocalDateTime creationDate;

    /**
     * 创建人
     */
    private Integer createdBy;

    /**
     * 修改日期
     */
    private LocalDateTime modifyDate;

    /**
     * 修改人
     */
    private Integer modifiedBy;


}
