package com.wy.admin.scm.business.tool.mapper;

import com.wy.admin.scm.business.tool.model.CommonDictData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 字典数据表 Mapper 接口
 * </p>
 *
 * @author wuyong
 * @since 2022-02-17
 */
public interface CommonDictDataMapper extends BaseMapper<CommonDictData> {

}
