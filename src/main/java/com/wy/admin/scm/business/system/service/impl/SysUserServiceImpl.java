package com.wy.admin.scm.business.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.conditions.update.LambdaUpdateChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wy.admin.scm.business.common.base.constant.NumberConstant;
import com.wy.admin.scm.business.common.base.constant.StringConstant;
import com.wy.admin.scm.business.common.base.constant.TipsConstant;
import com.wy.admin.scm.business.common.model.Pagination;
import com.wy.admin.scm.business.common.util.CommonStringUtils;
import com.wy.admin.scm.business.common.util.CommonUtils;
import com.wy.admin.scm.business.system.dto.SysMenuDTO;
import com.wy.admin.scm.business.system.dto.SysRoleDTO;
import com.wy.admin.scm.business.system.dto.SysUserDetailDTO;
import com.wy.admin.scm.business.system.mapper.SysUserMapper;
import com.wy.admin.scm.business.system.mapstruct.SystemCovertMapper;
import com.wy.admin.scm.business.system.model.SysUser;
import com.wy.admin.scm.business.system.service.SysUserService;
import com.wy.admin.scm.business.system.vo.request.SaveSysUserReqVO;
import com.wy.admin.scm.business.system.vo.request.SysUserReqVO;
import com.wy.admin.scm.business.system.vo.request.UpdatePwdReqVO;
import com.wy.admin.scm.business.system.vo.response.LoginResVO;
import com.wy.admin.scm.business.system.vo.response.SysUserResVO;
import com.wy.admin.scm.core.framework.lang.enums.CommonReturnEnum;
import com.wy.admin.scm.core.framework.lang.exception.BusinessException;
import com.wy.admin.scm.core.framework.util.ShiroUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author wuyong
 * @since 2021-12-16
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {
    @Autowired
    private SystemCovertMapper systemCovertMapper;

    @Autowired
    private SysUserRoleServiceImpl sysUserRoleService;

    @Autowired
    private SysRoleServiceImpl sysRoleService;

    @Autowired
    private SysMenuServiceImpl sysMenuService;


    public SysUser selectById(Integer id) {
        return new LambdaQueryChainWrapper<>(baseMapper)
                .eq(SysUser::getId, id)
//                .eq(SysUser::getActiveFlag, StringConstant.Y)
                .one();
    }

    public String getLoginName(Integer id) {
        SysUser sysUser = this.selectById(id);
        return null != sysUser ? sysUser.getLoginName() : StringConstant.EMPTY;
    }

    public SysUser selectByLoginName(String loginName) {
        return new LambdaQueryChainWrapper<>(baseMapper)
                .eq(SysUser::getLoginName, loginName)
                .eq(SysUser::getActiveFlag, StringConstant.Y)
                .one();
    }


    public IPage<SysUserResVO> querySysUserPage(SysUserReqVO reqVO) {
        Pagination pagination = reqVO.getPagination();
        Page<SysUserResVO> page = new Page<>(pagination.getCurrent(), pagination.getPageSize());

        IPage<SysUserResVO> resVOIPage = this.baseMapper.selectUserList(page, reqVO);
        if (resVOIPage.getTotal() > NumberConstant.ZERO) {
            for (SysUserResVO sysUserResVO : resVOIPage.getRecords()) {
                List<String> roleNameList = sysUserResVO.getRoleList().stream().map(SysRoleDTO::getRoleName).collect(Collectors.toList());
                sysUserResVO.setRoleNameList(CollectionUtils.isNotEmpty(roleNameList) ? StringUtils.join(roleNameList, ",") : StringConstant.EMPTY);
            }
        }
        return resVOIPage;
    }

    /**
     * 保存用户详情
     */
    @Transactional
    public SysUser saveSaveSysUserReqVO(SaveSysUserReqVO reqVO) {
        SysUser dbSysUser = this.selectByLoginName(reqVO.getLoginName());
        if (null != dbSysUser) {
            throw new BusinessException(CommonReturnEnum.CHECK_ERROR, TipsConstant.SYSUSER_LOGIN_NAME_EXIT);
        }

        SysUser sysUser = new SysUser();
        sysUser.setLoginName(reqVO.getLoginName());
        sysUser.setUserName(reqVO.getUserName());
        sysUser.setPhone(reqVO.getPhone());
        sysUser.setSex(reqVO.getSex());
        sysUser.setBirthDate(reqVO.getBirthDate());
        sysUser.setUserType(NumberConstant.ONE);
        sysUser.setAddress(reqVO.getAddress());
        String pwd = CommonUtils.password("123456");
        sysUser.setPwd(pwd);
        sysUser.setActiveFlag(StringConstant.Y);
        this.save(sysUser);

        sysUserRoleService.saveSysUserRole(sysUser.getId(), reqVO.getRoleIds());
        return sysUser;

    }


    /**
     * 获取用户详情
     */
    @Transactional
    public SysUserDetailDTO getSysUserDetail(Integer userId) {
        SysUser sysUser = this.selectById(userId);
        if (sysUser == null) {
            throw new BusinessException(CommonReturnEnum.CHECK_ERROR, TipsConstant.SYSUSER_NOT_EXIT);
        }
        SysUserDetailDTO sysUserDetailDTO = systemCovertMapper.covertSysUserDetailVO(sysUser);

        List<Integer> roleIds = sysUserRoleService.getRoleIdsByUserId(userId);
        sysUserDetailDTO.setRoleIds(roleIds);

        return sysUserDetailDTO;
    }

    /**
     * 修改用户详情
     */
    public void updateSysUser(SysUserDetailDTO reqVO) {
        SysUser sysUser = this.selectById(reqVO.getId());
        if (sysUser == null) {
            throw new BusinessException(CommonReturnEnum.CHECK_ERROR, TipsConstant.SYSUSER_NOT_EXIT);
        }
        sysUser = systemCovertMapper.covertSysUser(reqVO);
        this.updateById(sysUser);

        sysUserRoleService.updateSysUserRole(sysUser.getId(), reqVO.getRoleIds());
    }

    @Transactional
    public void deleteSysUser(Integer id) {
        this.removeById(id);
        sysUserRoleService.removeUserRoleByUserId(id);
    }


    public LoginResVO getLoginResVO() {
        // 取身份信息
        SysUser user = ShiroUtils.getSysUser();
        if (null == user){
            return null;
        }
        SysUserDetailDTO sysUserDetailVO = systemCovertMapper.covertSysUserDetailVO(user);
        // 角色列表
        Set<String> roles = sysRoleService.getUserRoleListByUserId(user.getId());
        // 功能列表
        List<SysMenuDTO> sysMenuVOS = sysMenuService.selectByUserId(user.getId());

        LoginResVO loginResVO = new LoginResVO();
        loginResVO.setSysUser(sysUserDetailVO);
        loginResVO.setRoles(roles);
        loginResVO.setMenus(sysMenuVOS);
        return loginResVO;
    }


    /**
     * 校验原密码
     */
    public void validateOldPass(UpdatePwdReqVO reqVO) {
        SysUser sysUser = this.selectById(reqVO.getId());
        if (sysUser == null) {
            throw new BusinessException(CommonReturnEnum.CHECK_ERROR, TipsConstant.SYSUSER_NOT_EXIT);
        }

        if (StringUtils.isBlank(reqVO.getOldPwd())) {
            throw new BusinessException(CommonReturnEnum.CHECK_ERROR, TipsConstant.OLD_PWD_NOT_NULL);
        }

        String pwd = CommonUtils.password(reqVO.getOldPwd());
        if (!sysUser.getPwd().equals(pwd)) {
            throw new BusinessException(CommonReturnEnum.CHECK_ERROR, TipsConstant.OLD_PWD_ERROR);
        }

    }


    /**
     * 重置密码
     */
    public void resetPwd(UpdatePwdReqVO reqVO) {
        this.validateOldPass(reqVO);
        if (!reqVO.getConfirmPwd().equals(reqVO.getNewPwd())) {
            throw new BusinessException(CommonReturnEnum.CHECK_ERROR, TipsConstant.THE_TWO_PASSWORDS_ARE_INCONSISTENT);
        }
        String pwd = CommonUtils.password(reqVO.getNewPwd());
        this.updatePwd(reqVO.getId(), pwd, reqVO.getId());
    }

    private void updatePwd(Integer id, String pwd, Integer userId) {
        new LambdaUpdateChainWrapper<>(baseMapper)
                .set(SysUser::getPwd, pwd)
                .set(SysUser::getModifiedBy, userId)
                .eq(SysUser::getId, id)
                .update();
    }


    public String resetPwdByRandom(Integer id, Integer userId) {
        String pwd = CommonStringUtils.generateShortPwd();
        String password = CommonUtils.password(pwd);
        this.updatePwd(id, password, userId);
        return pwd;
    }


}
