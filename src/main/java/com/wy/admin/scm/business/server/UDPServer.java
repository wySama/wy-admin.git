package com.wy.admin.scm.business.server;

import com.wy.admin.scm.business.common.base.enums.DataFrameTypeEnum;
import com.wy.admin.scm.business.common.util.UdpUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

@Slf4j
public class UDPServer {


    /**
     * 0：设备信息查询；
     * 1：配置信息查询；
     * 2：工作状态查询；
     * 3：工作日志查询。
     */
    public static void main(String[] args) throws IOException {
        String request = DataFrameTypeEnum.QUERY_COMMAND.getCode() + "000000000000000100000000"+"01111110";
//        request = request + UdpUtil.checkXor(request.getBytes());
        log.info("query request : " + request);

        DatagramPacket receiver;
        try (DatagramSocket socket = new DatagramSocket( 8800)) {
            byte[] data = request.getBytes();
            InetAddress address = InetAddress.getByName("192.168.31.179");
            int port = 58516;
            DatagramPacket packet = new DatagramPacket(data, data.length, address, port);
            socket.send(packet);

            byte[] buffer = new byte[1024];
            receiver = new DatagramPacket(buffer, buffer.length);
            socket.receive(receiver);
        }
        String res = new String(receiver.getData(), 0, receiver.getLength());
        log.info("query response : " + res);
    }


}
