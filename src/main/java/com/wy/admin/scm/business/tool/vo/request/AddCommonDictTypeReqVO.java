package com.wy.admin.scm.business.tool.vo.request;

import lombok.Data;

import java.io.Serializable;

@Data
public class AddCommonDictTypeReqVO  implements Serializable {

    private Integer userId;

    /**
     * 字典名称
     */
    private String dictName;

    /**
     * 字典类型
     */
    private String dictType;

    /**
     * 备注
     */
    private String remark;

    /**
     * 是否有效, Y:有效 N:无效
     */
    private String activeFlag;
}
