package com.wy.admin.scm.business.log.controller;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.wy.admin.scm.business.common.base.enums.BusinessType;
import com.wy.admin.scm.business.log.dto.AlarmLogDTO;
import com.wy.admin.scm.business.log.dto.UpdateAlarmLogDTO;
import com.wy.admin.scm.business.log.model.AlarmLog;
import com.wy.admin.scm.business.log.model.SysLoginLog;
import com.wy.admin.scm.business.log.model.SysOperLog;
import com.wy.admin.scm.business.log.service.impl.AlarmLogServiceImpl;
import com.wy.admin.scm.business.log.service.impl.SysLoginLogServiceImpl;
import com.wy.admin.scm.business.log.service.impl.SysOperLogServiceImpl;
import com.wy.admin.scm.business.log.vo.request.AlarmLogReqVO;
import com.wy.admin.scm.business.log.vo.request.SysLoginLogReqVO;
import com.wy.admin.scm.business.log.vo.request.SysOperLogReqVO;
import com.wy.admin.scm.core.framework.aspect.Log;
import com.wy.admin.scm.core.framework.model.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 登录日志表 前端控制器
 * </p>
 *
 * @author wuyong
 * @since 2022-02-17
 */
@RestController
@RequestMapping("/api/log")
public class LogRestController {


    @Autowired
    private SysLoginLogServiceImpl sysLoginLogService;


    @Autowired
    private SysOperLogServiceImpl sysOperLogService;

    @Autowired
    private AlarmLogServiceImpl alarmLogService;


    /**
     * 分页查询登录日志
     */
    @PostMapping(value = "/querySysLoginLogPage")
    public JsonResult<IPage<SysLoginLog>> querySysLoginLogPage(@RequestBody SysLoginLogReqVO reqVO) {
        IPage<SysLoginLog> res = sysLoginLogService.querySysLoginLogPage(reqVO);
        return new JsonResult<>(res);
    }


    /**
     * 删除登录日志
     */
    @Log(title = "登录日志", businessType = BusinessType.DELETE)
    @PostMapping(value = "/batchRemove")
    public JsonResult<JSONObject> batchRemove(@RequestBody SysLoginLogReqVO reqVO) {
        sysLoginLogService.batchRemove(reqVO);
        return new JsonResult<>(new JSONObject());
    }


    /**
     * 分页查询操作日志
     */
    @PostMapping(value = "/querySysOperLogPage")
    public JsonResult<IPage<SysOperLog>> querySysOperLogPage(@RequestBody SysOperLogReqVO reqVO) {
        IPage<SysOperLog> res = sysOperLogService.querySysOperLogPage(reqVO);
        return new JsonResult<>(res);
    }

    /**
     * 分页查询告警日志
     */
    @PostMapping("queryAlarmLogPage")
    public JsonResult<IPage<AlarmLogDTO>> queryAlarmLogPage(@RequestBody AlarmLogReqVO reqVO) {
        IPage<AlarmLogDTO> result = alarmLogService.queryAlarmLogPage(reqVO);
        return new JsonResult<>(result);
    }


    /**
     * 导出告警日志
     */
    @PostMapping(value = "/exportAlarmLog")
    public void exportAlarmLog(HttpServletResponse response,@RequestBody AlarmLogReqVO reqVO) {
        alarmLogService.exportAlarmLog(response, reqVO);
    }

    /**
     * 处理日志
     */
    @Log(title = "登录日志", businessType = BusinessType.UPDATE)
    @PostMapping("handleAlarm")
    public JsonResult<AlarmLog> handleAlarm(@RequestBody UpdateAlarmLogDTO reqVO) {
        AlarmLog res = alarmLogService.handleAlarm(reqVO);
        return new JsonResult<>(res);
    }
}

