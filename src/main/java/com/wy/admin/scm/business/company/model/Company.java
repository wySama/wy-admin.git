package com.wy.admin.scm.business.company.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>
 * 单位表
 * </p>
 *
 * @author wuyong
 * @since 2022-02-16
 */
@Data
@TableName("admin.company")
public class Company extends Model<Company> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 单位名称
     */
    private String companyName;

    /**
     * 单位简称
     */
    private String companyAbbreviation;

    /**
     * 负责人名称
     */
    private String leaderName;

    /**
     * 负责人手机号
     */
    private String leaderPhone;

    /**
     * 单位地址
     */
    private String address;

    /**
     * 备注
     */
    private String remark;

    /**
     * 是否有效, Y:有效 N:无效
     */
    private String activeFlag;

    /**
     * 创建日期
     */
    private LocalDateTime creationDate;

    /**
     * 创建人
     */
    private Integer createdBy;

    /**
     * 修改日期
     */
    private LocalDateTime modifyDate;

    /**
     * 修改人
     */
    private Integer modifiedBy;


}
