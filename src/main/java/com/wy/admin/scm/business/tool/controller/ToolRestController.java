package com.wy.admin.scm.business.tool.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.wy.admin.scm.business.common.base.enums.BusinessType;
import com.wy.admin.scm.business.common.dto.DropdownDTO;
import com.wy.admin.scm.business.tool.dto.DictDataDTO;
import com.wy.admin.scm.business.tool.model.CommonDictData;
import com.wy.admin.scm.business.tool.service.impl.CommonDictDataServiceImpl;
import com.wy.admin.scm.business.tool.service.impl.CommonDictTypeServiceImpl;
import com.wy.admin.scm.business.tool.vo.request.AddCommonDictTypeReqVO;
import com.wy.admin.scm.business.tool.vo.request.QueryDictDataReqVO;
import com.wy.admin.scm.core.framework.aspect.Log;
import com.wy.admin.scm.core.framework.model.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/api/tool")
public class ToolRestController {

    @Autowired
    private CommonDictDataServiceImpl commonDictDataService;

    @Autowired
    private CommonDictTypeServiceImpl commonDictTypeService;


    /**
     * 分页查询数据字典
     */
    @PostMapping(value = "/queryCommonDictDataPage")
    public JsonResult<IPage<CommonDictData>> queryCommonDictDataPage(@RequestBody QueryDictDataReqVO reqVO) {
        IPage<CommonDictData> res = commonDictDataService.queryCommonDictDataPage(reqVO);
        return new JsonResult<>(res);
    }

    /**
     * 保存数据字典
     */
    @Log(title = "数据字典", businessType = BusinessType.INSERT)
    @PostMapping(value = "/saveDictData")
    public JsonResult<JSONObject> saveDictData(@RequestBody DictDataDTO reqVO) {
        commonDictDataService.saveDictData(reqVO);
        return new JsonResult<>(new JSONObject());
    }

    /**
     * 修改数据字典
     */
    @Log(title = "数据字典", businessType = BusinessType.UPDATE)
    @PostMapping(value = "/updateDictData")
    public JsonResult<JSONObject> updateDictData(@RequestBody DictDataDTO reqVO) {
        commonDictDataService.updateDictData(reqVO);
        return new JsonResult<>(new JSONObject());
    }

    /**
     * 获取数据字典详情
     */
    @GetMapping(value = "/getDictDataDetail")
    public JsonResult<DictDataDTO> getDictDataDetail(Integer id) {
        DictDataDTO res = commonDictDataService.getDictDataDetail(id);
        return new JsonResult<>(res);
    }

    /**
     * 删除数据字典详情
     */
    @Log(title = "数据字典", businessType = BusinessType.DELETE)
    @GetMapping(value = "/deleteDictData")
    public JsonResult<JSONObject> deleteDictData(Integer id) {
        commonDictDataService.deleteDictData(id);
        return new JsonResult<>(new JSONObject());
    }


    /**
     * 初始化DropdownDTO
     */
    @GetMapping(value = "/getDictTypeDropdownList")
    public JsonResult<Map<String, List<DropdownDTO<String>>>> getDictTypeDropdownList() {
        Map<String, List<DropdownDTO<String>>> res = commonDictTypeService.getDictTypeDropdownList();
        return new JsonResult<>(res);

    }


    /**
     * 保存数据字典类型
     */
    @Log(title = "字典类型", businessType = BusinessType.INSERT)
    @PostMapping(value = "/saveDictType")
    public JsonResult<JSONObject> saveDictType(@RequestBody AddCommonDictTypeReqVO reqVO) {
        commonDictTypeService.saveDictType(reqVO);
        return new JsonResult<>(new JSONObject());
    }



}
