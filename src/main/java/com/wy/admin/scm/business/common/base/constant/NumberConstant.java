package com.wy.admin.scm.business.common.base.constant;

public class NumberConstant {

    public static final Integer ZERO = 0;
    public static final Integer ONE = 1;
    public static final Integer TWO = 2;
    public static final Integer THREE = 3;
    public static final Integer FOUR = 4;
    public static final Integer TEN = 10;
    public static final Integer  ONE_HUNDRED = 100;
    public static final Integer  NINE_HUNDRED_AND_NINETY_NINE = 999;

    public static final Integer NEGATIVE_ONE = -1;
}
