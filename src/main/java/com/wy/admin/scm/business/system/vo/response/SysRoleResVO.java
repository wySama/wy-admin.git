package com.wy.admin.scm.business.system.vo.response;

import lombok.Data;

/**
 * 用户列表VO
 *
 * @author w
 * @date 2021-03-05
 */
@Data
public class SysRoleResVO {

    private Integer id;

    /**
     * 名称
     */
    private String roleName;

    /**
     * 角色标识
     */
    private String roleCode;


    /**
     * 备注
     */
    private String remark;

    /**
     * 是否有效, Y:有效 N:无效
     */
    private String activeFlag;

    /**
     * 创建日期
     */
    private String creationDate;


    /**
     * 修改日期
     */
    private String modifyDate;


}
