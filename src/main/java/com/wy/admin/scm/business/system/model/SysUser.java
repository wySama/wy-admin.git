package com.wy.admin.scm.business.system.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author wuyong
 * @since 2022-02-11
 */
@Data
@TableName("admin.sys_user")
public class SysUser extends Model<SysUser> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 登录名
     */
    private String loginName;

    /**
     * 密码
     */
    private String pwd;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 姓名
     */
    private String userName;

    /**
     * 性别(0女 1男)
     */
    private Integer sex;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 出生日期
     */
    private LocalDate birthDate;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 省
     */
    private String province;

    /**
     * 市
     */
    private String city;

    /**
     * 区
     */
    private String district;

    /**
     * 地址
     */
    private String address;

    /**
     * 单位ID
     */
    private Integer companyId;

    /**
     * 部门ID
     */
    private Integer deptId;

    /**
     * 用户类型(0内置用户 1注册用户)
     */
    private Integer userType;

    /**
     * 是否有效, Y:有效 N:无效
     */
    private String activeFlag;

    /**
     * 创建日期
     */
    private LocalDateTime creationDate;

    /**
     * 创建人
     */
    private Integer createdBy;

    /**
     * 修改日期
     */
    private LocalDateTime modifyDate;

    /**
     * 修改人
     */
    private Integer modifiedBy;


    public boolean isAdmin() {
        return isAdmin(this.id);
    }

    private static boolean isAdmin(Integer userId) {
        return userId != null && 1 == userId;
    }
}
