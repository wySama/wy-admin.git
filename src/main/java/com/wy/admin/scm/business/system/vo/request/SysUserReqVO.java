package com.wy.admin.scm.business.system.vo.request;

import com.wy.admin.scm.business.common.model.Pagination;
import lombok.Data;

/**
 * 用户查询DTO
 *
 * @author w
 * @date 2021-03-05
 */
@Data
public class SysUserReqVO {

    private String loginName;

    private String userName;

    private String phone;

    private String activeFlag;

    private Pagination pagination;
}
