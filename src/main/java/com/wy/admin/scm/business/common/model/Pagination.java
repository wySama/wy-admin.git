package com.wy.admin.scm.business.common.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


@Data
@ApiModel(value = "分页参数")
public class Pagination  implements Serializable {

    @ApiModelProperty(value = "每页大小")
    private long pageSize;

    @ApiModelProperty(value = "当前页")
    private long current;

    @ApiModelProperty(value = "总条数")
    private long total;

    public Pagination() {
    }

    public Pagination(long pageSize, long current, long total) {
        this.pageSize = pageSize;
        this.current = current;
        this.total = total;
    }
}
