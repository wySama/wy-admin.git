package com.wy.admin.scm.business.company.service;

import com.wy.admin.scm.business.company.model.Company;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 单位表 服务类
 * </p>
 *
 * @author wuyong
 * @since 2022-02-16
 */
public interface CompanyService extends IService<Company> {

}
