package com.wy.admin.scm.business.log.mapstruct;

import com.wy.admin.scm.business.common.component.LocalDateTimeFormat;
import com.wy.admin.scm.business.log.dto.AlarmLogDTO;
import com.wy.admin.scm.business.log.model.AlarmLog;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;


@Mapper(componentModel = "spring", uses = LocalDateTimeFormat.class)
public interface LogCovertMapper {

    @Mapping(target = "alarmDate", source = "alarmDate", qualifiedByName = "localDateToString")
    @Mapping(target = "modifyDate", source = "modifyDate", qualifiedByName = "localDateToString")
    AlarmLogDTO covertAlarmLogDTO(AlarmLog alarmLog);

    List<AlarmLogDTO> covertAlarmLogDTOList(List<AlarmLog> alarmLogList);

}
