package com.wy.admin.scm.business.system.vo.response;

import com.wy.admin.scm.business.system.dto.SysRoleDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 用户列表VO
 *
 * @author w
 * @date 2021-03-05
 */
@Data
public class SysUserResVO {
    private Integer id;

    /**
     * 登录名
     */
    private String loginName;

    /**
     * 密码
     */
    private String pwd;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 姓名
     */
    private String userName;

    /**
     * 性别(0女 1男)
     */
    private Integer sex;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 出生日期
     */
    private LocalDate birthDate;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 省
     */
    private String province;

    /**
     * 市
     */
    private String city;

    /**
     * 区
     */
    private String district;

    /**
     * 地址
     */
    private String address;

    /**
     * 单位ID
     */
    private Integer companyId;

    /**
     * 部门ID
     */
    private Integer deptId;

    /**
     * 用户类型(0内置用户 1注册用户)
     */
    private Integer userType;

    /**
     * 是否有效, Y:有效 N:无效
     */
    private String activeFlag;

    /**
     * 创建日期
     */
    private LocalDateTime creationDate;

    /**
     * 创建人
     */
    private Integer createdBy;

    /**
     * 修改日期
     */
    private LocalDateTime modifyDate;

    /**
     * 修改人
     */
    private Integer modifiedBy;






    private List<SysRoleDTO> roleList;

    private String roleNameList;
}
