package com.wy.admin.scm.business.common.component;


import com.wy.admin.scm.business.common.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.Named;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Component
@Named("LocalDateTimeFormat")
public class LocalDateTimeFormat {


    //绑定限定符
    @Named("localDateToString")
    public String localDateToString(LocalDate localDate) {
        if (null != localDate) {
            return DateUtil.getDateString(localDate);
        }
        return null;
    }




    //绑定限定符
    @Named("stringToLocalDateTime")
    public LocalDateTime stringToLocalDateTime(String localDateTime) {
        if (StringUtils.isNotBlank(localDateTime)) {
            return DateUtil.parseDateTime(localDateTime, "yyyy-MM-dd HH:mm:ss");
        }
        return null;
    }


    //绑定限定符
    @Named("stringToLocalDate")
    public LocalDate stringToLocalDate(String localDate) {
        if (StringUtils.isNotBlank(localDate)) {
            return DateUtil.parseDate(localDate, "yyyy-MM-dd");
        }
        return null;
    }


}
