package com.wy.admin.scm.business.server;

import com.wy.admin.scm.business.common.base.enums.DataFrameTypeEnum;
import com.wy.admin.scm.business.common.util.UdpUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.*;

@Service
@Slf4j
public class UDPService {

    /**
     * type
     * 0：设备信息查询；
     * 1：配置信息查询；
     * 2：工作状态查询；
     * 3：工作日志查询。
     * <p>
     * <p>
     * 帧头 + 帧长 + 帧内容 + 检验和
     */
    public String queryByType(String type) throws IOException {
        String request = DataFrameTypeEnum.QUERY_COMMAND.getCode() +"01" + type ;
        request = request +  UdpUtil.checkXor(request.getBytes());
        log.info("query request : " + request);

        DatagramPacket receiver;
        try (DatagramSocket socket = new DatagramSocket()) {
            byte[] data = request.getBytes();
            InetAddress address = InetAddress.getByName("localhost");
            int port = 8800;
            DatagramPacket packet = new DatagramPacket(data, data.length, address, port);
            socket.send(packet);

            byte[] buffer = new byte[1024];
            receiver = new DatagramPacket(buffer, buffer.length);
            socket.receive(receiver);
        }
        String res = new String(receiver.getData(), 0, receiver.getLength());
        log.info("query response : " + res);
        return res;
    }


}
