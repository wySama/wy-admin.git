package com.wy.admin.scm.business.common.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class DropdownDTO<T> implements Serializable {

    private static final long serialVersionUID = -5028964917688371763L;
    private String label;
    private T value;


}
