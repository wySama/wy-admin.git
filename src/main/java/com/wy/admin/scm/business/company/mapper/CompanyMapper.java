package com.wy.admin.scm.business.company.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wy.admin.scm.business.company.dto.CompanyDTO;
import com.wy.admin.scm.business.company.model.Company;
import com.wy.admin.scm.business.company.vo.request.CompanyReqVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 单位表 Mapper 接口
 * </p>
 *
 * @author wuyong
 * @since 2022-02-16
 */
public interface CompanyMapper extends BaseMapper<Company> {

    IPage<CompanyDTO> queryCompanyPage(@Param("page") Page<CompanyDTO> page, @Param("req") CompanyReqVO reqVO);

    List<CompanyDTO> queryCompanyList(@Param("req") CompanyReqVO reqVO);
}
