package com.wy.admin.scm.business.common.util;

public class UdpUtil {


    /**
     * 按字节异或求和
     *
     * @param bytes
     * @return int
     */
    public static int checkXor(byte[] bytes) {
        int checksum = 0;
        for (byte b : bytes) {
            checksum ^= b;
        }
        return checksum;
    }

    public static void main(String[] args) {
        String str = "$ZSHCXML000000000000000100000000";
        System.out.println(checkXor(str.getBytes()));

    }



}
