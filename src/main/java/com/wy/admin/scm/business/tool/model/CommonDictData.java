package com.wy.admin.scm.business.tool.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>
 * 字典数据表
 * </p>
 *
 * @author wuyong
 * @since 2022-02-17
 */
@Data
@TableName("admin.common_dict_data")
public class CommonDictData extends Model<CommonDictData> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * code
     */
    private String dictCode;

    /**
     * value
     */
    private String dictValue;

    /**
     * 类型
     */
    private String dictType;

    /**
     * 字典排序
     */
    private Integer dictSort;

    /**
     * 备注
     */
    private String remark;

    /**
     * 是否有效, Y:有效 N:无效
     */
    private String activeFlag;

    /**
     * 创建日期
     */
    private LocalDateTime creationDate;

    /**
     * 创建人
     */
    private Integer createdBy;

    /**
     * 修改日期
     */
    private LocalDateTime modifyDate;

    /**
     * 修改人
     */
    private Integer modifiedBy;


}
