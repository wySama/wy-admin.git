package com.wy.admin.scm.business.common.service.impl;

import com.alibaba.excel.EasyExcel;
import com.wy.admin.scm.business.common.base.constant.NumberConstant;
import com.wy.admin.scm.business.common.base.constant.TipsConstant;
import com.wy.admin.scm.business.system.model.SysUser;
import com.wy.admin.scm.business.system.service.impl.SysRoleServiceImpl;
import com.wy.admin.scm.core.framework.lang.enums.CommonReturnEnum;
import com.wy.admin.scm.core.framework.lang.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.Collection;
import java.util.Set;

@Service
@Slf4j
public class CommonService {


    @Autowired
    private SysRoleServiceImpl sysRoleService;


    /**
     * 判断当前是否为管理员
     *
     * @return
     */
    public Boolean isAdmin(SysUser user) {
        // 角色列表
        Set<String> roles = sysRoleService.getUserRoleListByUserId(user.getId());
        return user.getUserType().equals(NumberConstant.ZERO) || roles.contains("admin") || roles.contains("systemOperator");
    }


    public void commonExport(HttpServletResponse response, Class head, Collection<?> data) {
        // 这里 需要指定写用哪个class去写，然后写到第一个sheet，名字为模板 然后文件流会自动关闭
        try {
            // 这里注意 有同学反应使用swagger 会导致各种问题，请直接用浏览器或者用postman
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setCharacterEncoding("utf-8");
            // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系 .registerConverter(new ActiveFlagConverter())
            // String fileName = URLEncoder.encode("company", "UTF-8").replaceAll("\\+", "%20");
            // response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
            EasyExcel.write(response.getOutputStream(), head).sheet("sheet").doWrite(data);
        } catch (Exception e) {
            log.error("下载报表异常：{}", e);
            throw new BusinessException(CommonReturnEnum.SYSTEM_ERROR, TipsConstant.EXPORT_EXCEPTION);
        }
    }


}
