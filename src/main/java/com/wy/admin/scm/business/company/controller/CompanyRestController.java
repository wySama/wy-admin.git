package com.wy.admin.scm.business.company.controller;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.wy.admin.scm.business.common.base.enums.BusinessType;
import com.wy.admin.scm.business.common.dto.DropdownDTO;
import com.wy.admin.scm.business.company.dto.CompanyDTO;
import com.wy.admin.scm.business.company.model.Company;
import com.wy.admin.scm.business.company.service.impl.CompanyServiceImpl;
import com.wy.admin.scm.business.company.vo.request.CompanyReqVO;
import com.wy.admin.scm.core.framework.aspect.Log;
import com.wy.admin.scm.core.framework.model.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 单位表 前端控制器
 * </p>
 *
 * @author wuyong
 * @since 2022-02-16
 */
@RestController
@RequestMapping("/api/company")
public class CompanyRestController {

    @Autowired
    private CompanyServiceImpl companyService;

    /**
     * 分页获取单位
     */
    @PostMapping("queryCompanyPage")
    public JsonResult<IPage<CompanyDTO>> queryCompanyPage(@RequestBody CompanyReqVO reqVO) {
        IPage<CompanyDTO> resVOIPage = companyService.queryCompanyPage(reqVO);
        return new JsonResult<>(resVOIPage);
    }

    /**
     * 导出单位
     */
    @PostMapping("/exportCompany")
    public void exportCompany(HttpServletResponse response, @RequestBody CompanyReqVO reqVO) {
        companyService.exportCompany(response, reqVO);
    }

    @GetMapping("getCompanyDetail")
    public JsonResult<CompanyDTO> getCompanyDetail(Integer id) {
        CompanyDTO res = companyService.getCompanyDetail(id);
        return new JsonResult<>(res);
    }

    @Log(title = "单位管理", businessType = BusinessType.INSERT)
    @PostMapping("saveCompany")
    public JsonResult<Company> saveCompany(@RequestBody CompanyDTO companyDTO) {
        Company company = companyService.saveCompany(companyDTO);
        return new JsonResult<>(company);
    }


    @Log(title = "单位管理", businessType = BusinessType.UPDATE)
    @PostMapping("updateCompany")
    public JsonResult<Company> updateCompany(@RequestBody CompanyDTO companyDTO) {
        Company company = companyService.updateCompany(companyDTO);
        return new JsonResult<>(company);
    }

    @Log(title = "单位管理", businessType = BusinessType.DELETE)
    @GetMapping("deleteCompany")
    public JsonResult<JSONObject> deleteCompany(Integer id) {
        companyService.deleteCompany(id);
        return new JsonResult<>(new JSONObject());
    }

    /**
     * 获取所有角色下拉框选项
     */
    @GetMapping("/getCompanyDropdownList")
    public JsonResult<Map<String, List<DropdownDTO<Integer>>>> getCompanyDropdownList() {
        Map<String, List<DropdownDTO<Integer>>> res = companyService.getCompanyDropdownList();
        return new JsonResult<>(res);
    }

}

