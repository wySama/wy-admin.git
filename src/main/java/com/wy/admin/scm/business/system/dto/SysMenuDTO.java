package com.wy.admin.scm.business.system.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 菜单VO
 *
 * @author w
 * @date 2021-03-05
 */
@Data
public class SysMenuDTO implements Serializable {

    private static final long serialVersionUID = 581762180696140329L;
    @ApiModelProperty(value = "菜单ID")
    private Integer id;

    @ApiModelProperty(value = "名称")
    private String menuName;

    @ApiModelProperty(value = "父ID")
    private Integer parentId;

    @ApiModelProperty(value = "类型(1目录 2菜单 3按钮)")
    private Integer menuType;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "URL")
    private String url;

    @ApiModelProperty(value = "权限标识")
    private String perms;

    @ApiModelProperty(value = "菜单图标")
    private String icon;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "创建时间")
    private String creationDate;

    @ApiModelProperty(value = "是否有效, Y:有效 N:无效")
    private String activeFlag;

    @ApiModelProperty(value = "子权限")
    private List<SysMenuDTO> children;
}
