package com.wy.admin.scm.business.tool.vo.request;


import com.wy.admin.scm.business.common.model.Pagination;
import lombok.Data;

import java.io.Serializable;

@Data
public class QueryDictDataReqVO implements Serializable {
    private static final long serialVersionUID = -4286094453267158360L;

    private String dictCode;

    private String dictType;

    private String activeFlag;

    private Pagination pagination;
}
