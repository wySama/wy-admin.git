package com.wy.admin.scm.business.system.vo.request;

import lombok.Data;

/**
 * 用户查询DTO
 *
 * @author w
 * @date 2021-03-05
 */
@Data
public class SysMenuReqVO {

    private String menuName;

    /**
     * 是否有效, Y:有效 N:无效
     */
    private String activeFlag;
}
