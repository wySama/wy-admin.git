package com.wy.admin.scm.business.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wy.admin.scm.business.common.base.constant.NumberConstant;
import com.wy.admin.scm.business.common.base.constant.StringConstant;
import com.wy.admin.scm.business.common.base.constant.TipsConstant;
import com.wy.admin.scm.business.system.mapstruct.SystemCovertMapper;
import com.wy.admin.scm.business.system.dto.SysMenuDTO;
import com.wy.admin.scm.business.system.mapper.SysMenuMapper;
import com.wy.admin.scm.business.system.mapstruct.SystemCovertMapper;
import com.wy.admin.scm.business.system.model.SysMenu;
import com.wy.admin.scm.business.system.model.SysUser;
import com.wy.admin.scm.business.system.service.SysMenuService;
import com.wy.admin.scm.business.system.vo.request.SaveSysMenuReqVO;
import com.wy.admin.scm.business.system.vo.request.SysMenuReqVO;
import com.wy.admin.scm.business.system.vo.request.UpdateSysMenuReqVO;

import com.wy.admin.scm.core.framework.lang.enums.CommonReturnEnum;
import com.wy.admin.scm.core.framework.lang.exception.BusinessException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 菜单表 服务实现类
 * </p>
 *
 * @author wuyong
 * @since 2021-12-16
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {

    @Autowired
    private SysUserServiceImpl sysUserService;

    @Autowired
    private SystemCovertMapper systemCovertMapper;


    public Set<String> getPermsByUserId(Integer userId) {
        List<SysMenu> sysMenuList = getSysMenus(userId);
        if (CollectionUtils.isEmpty(sysMenuList)) {
            return new HashSet<>();
        }
        List<String> perms = sysMenuList.stream().map(SysMenu::getPerms).distinct().collect(Collectors.toList());
        return new HashSet<>(perms);
    }


    public List<SysMenuDTO> selectByUserId(Integer userId) {
        List<SysMenu> sysMenuList = getSysMenus(userId);

        List<SysMenuDTO> voList = new ArrayList<>();
        for (SysMenu sysMenu : sysMenuList) {
            if (sysMenu.getParentId().equals(NumberConstant.ZERO)) {
                SysMenuDTO sysMenuVo = systemCovertMapper.covertSysMenuVO(sysMenu);
                sysMenuVo.setChildren(getChildren(sysMenuList, sysMenu.getId()));
                voList.add(sysMenuVo);
            }
        }
        return voList;
    }

    private List<SysMenu> getSysMenus(Integer userId) {
        SysUser sysUser = sysUserService.selectById(userId);
        if (sysUser == null) {
            throw new BusinessException(CommonReturnEnum.CHECK_ERROR, TipsConstant.SYSUSER_NOT_EXIT);
        }
        List<SysMenu> list;
        if (sysUser.getUserType().equals(NumberConstant.ZERO)) {
            //内置用户拥有所有菜单权限、按钮权限
            list = this.getAll();
        } else {
            list = baseMapper.selectByUserId(userId);
        }
        return list;
    }

    public List<SysMenuDTO> getChildren(List<SysMenu> list, Integer id) {
        List<SysMenuDTO> voList = new ArrayList<>();
        for (SysMenu sysMenu : list) {
            if (sysMenu.getParentId() != null && sysMenu.getParentId().equals(id)) {
                SysMenuDTO sysMenuVO = systemCovertMapper.covertSysMenuVO(sysMenu);
                sysMenuVO.setChildren(getChildren(list, sysMenu.getId()));
                voList.add(sysMenuVO);
            }
        }
        return voList;
    }


    /**
     * 获取所有的菜单
     */
    private List<SysMenu> getAll() {
        return new LambdaQueryChainWrapper<>(baseMapper)
                .eq(SysMenu::getActiveFlag, StringConstant.Y)
                .list();

    }


    /**
     * 根据条件查询菜单列表
     */
    private List<SysMenu> getListSysMenuReqVO(SysMenuReqVO reqVO) {
        LambdaQueryWrapper<SysMenu> queryChainWrapper = new LambdaQueryWrapper<>();

        if (StringUtils.isNotBlank(reqVO.getMenuName())) {
            queryChainWrapper.eq(SysMenu::getMenuName, reqVO.getMenuName());
        }
        if (StringUtils.isNotBlank(reqVO.getActiveFlag())) {
            queryChainWrapper.eq(SysMenu::getActiveFlag, reqVO.getActiveFlag());
        }
        return this.baseMapper.selectList(queryChainWrapper);
    }


    /**
     * 根据条件查询菜单列表
     */
    public List<SysMenuDTO> getSysMenuDTOS(SysMenuReqVO reqVO) {
        List<SysMenu> sysMenuList = this.getListSysMenuReqVO(reqVO);
        return systemCovertMapper.covertSysMenuVOS(sysMenuList);
    }

    /**
     * 获取菜单下拉框选项
     */
    public List<SysMenuDTO> getMenuTreeData(Integer userId) {
        List<SysMenu> sysMenuList = this.getSysMenus(userId);

        List<SysMenuDTO> list = systemCovertMapper.covertSysMenuVOS(sysMenuList);
        //存储根节点的菜单，即一级菜单
        List<SysMenuDTO> rootlist = new ArrayList<>();
        //遍历所有数据，找到根节点菜单
        for (SysMenuDTO menuDTO : list) {
            if (menuDTO.getParentId().equals(0)) {
                //找到根节点菜单的时候，寻找这个根节点菜单下的子节点菜单。
                findChilds(menuDTO, list);
                //添加到根节点的列表中
                rootlist.add(menuDTO);
            }
        }
        return rootlist;
    }

    private void findChilds(SysMenuDTO root, List<SysMenuDTO> list) {
        List<SysMenuDTO> childlist = new ArrayList<>();
        //遍历所有数据，找到是入参父节点的子节点的数据，然后加到childlist集合中。
        for (SysMenuDTO menu : list) {
            if (root.getId().equals(menu.getParentId()))
                childlist.add(menu);
        }
        //若子节点不存在，那么就不必再遍历子节点中的子节点了 直接返回。
        if (childlist.size() == 0)
            return;
        //设置父节点的子节点列表
        root.setChildren(childlist);
        //若子节点存在，接着递归调用该方法，寻找子节点的子节点。
        for (SysMenuDTO childs : childlist) {
            findChilds(childs, list);
        }
    }

    /**
     * 新增菜单
     */
    public void addSysMenu(SaveSysMenuReqVO reqVO) {
        SysMenu sysMenu = checkMenuNameUnique(reqVO.getParentId(), reqVO.getMenuName());
        if (null != sysMenu) {
            throw new BusinessException(CommonReturnEnum.CHECK_ERROR, TipsConstant.SYSMENU_NAME_EXIT);
        }
        sysMenu = systemCovertMapper.covertSysMenu(reqVO);
        sysMenu.setCreatedBy(reqVO.getUserId());
        this.save(sysMenu);
    }


    /**
     * 校验菜单名称是否唯一
     */
    private SysMenu checkMenuNameUnique(Integer parentId, String menuName) {
        return new LambdaQueryChainWrapper<>(baseMapper)
                .eq(SysMenu::getParentId, parentId)
                .eq(SysMenu::getMenuName, menuName)
                .eq(SysMenu::getActiveFlag, StringConstant.Y)
                .one();
    }

    /**
     * 修改菜单
     */
    public void updateSysMenu(UpdateSysMenuReqVO reqVO) {
        SysMenu sysMenu = checkMenuNameUnique(reqVO.getParentId(), reqVO.getMenuName());
        if (null != sysMenu && !reqVO.getId().equals(sysMenu.getId())) {
            throw new BusinessException(CommonReturnEnum.CHECK_ERROR, TipsConstant.SYSMENU_NAME_EXIT);
        }
        sysMenu = systemCovertMapper.covertSysMenuByUpdateSysMenuReqVO(reqVO);
        this.updateById(sysMenu);
    }


    /**
     * 获取菜单详情
     */
    public UpdateSysMenuReqVO getSysMenuDetail(Integer id) {
        SysMenu sysMenu = this.getById(id);
        UpdateSysMenuReqVO res = systemCovertMapper.covertUpdateSysMenuReqVO(sysMenu);
        if (null != res.getParentId() && !NumberConstant.ZERO.equals(res.getParentId())) {
            SysMenu parentMenu = this.getById(res.getParentId());
            res.setParentMenuName(null != parentMenu ? parentMenu.getMenuName() : StringConstant.EMPTY);

        }
        return res;
    }

    /**
     * 删除菜单
     */
    @Transactional
    public void deleteSysMenu(Integer id) {
        List<SysMenu> sysMenus = this.getByParentId(id);
        if (CollectionUtils.isNotEmpty(sysMenus)) {
            throw new BusinessException(CommonReturnEnum.CHECK_ERROR, TipsConstant.PLEASE_DELETE_THE_CHILD_NODE_FIRST);
        }
        this.removeById(id);
    }


    private List<SysMenu> getByParentId(Integer parentId) {
        return new LambdaQueryChainWrapper<>(baseMapper)
                .eq(SysMenu::getParentId, parentId)
                .eq(SysMenu::getActiveFlag, StringConstant.Y)
                .list();

    }

}
