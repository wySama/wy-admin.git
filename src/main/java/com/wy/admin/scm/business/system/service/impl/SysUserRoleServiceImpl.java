package com.wy.admin.scm.business.system.service.impl;

import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.conditions.update.LambdaUpdateChainWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wy.admin.scm.business.system.mapper.SysUserRoleMapper;
import com.wy.admin.scm.business.system.model.SysUserRole;
import com.wy.admin.scm.business.system.service.SysUserRoleService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户关联角色表 服务实现类
 * </p>
 *
 * @author wuyong
 * @since 2021-12-16
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements SysUserRoleService {


    /**
     * 保存用户角色
     *
     * @param userId
     * @param roleIds
     */
    public void saveSysUserRole(Integer userId, List<Integer> roleIds) {
        if (CollectionUtils.isEmpty(roleIds)) {
            return;
        }
        List<SysUserRole> sysUserRoles = new ArrayList<>();
        roleIds.forEach(item -> {
            SysUserRole sysUserRole = new SysUserRole();
            sysUserRole.setUserId(userId);
            sysUserRole.setRoleId(item);
            sysUserRoles.add(sysUserRole);
        });
        this.saveBatch(sysUserRoles);

    }


    /**
     * 修改用户角色
     *
     * @param userId
     * @param roleIds
     */
    public void updateSysUserRole(Integer userId, List<Integer> roleIds) {
        //删除原来的用户角色
        this.removeUserRoleByUserId(userId);
        this.saveSysUserRole(userId, roleIds);
    }


    public void removeUserRoleByUserId(Integer userId) {
        new LambdaUpdateChainWrapper<>(baseMapper)
                .eq(SysUserRole::getUserId, userId)
                .remove();

    }

    /**
     * 获取用户角色
     *
     * @param userId
     * @return
     */
    public List<Integer> getRoleIdsByUserId(Integer userId) {
        List<Integer> roleIds = new ArrayList<>();
        List<SysUserRole> sysUserRoles = new LambdaQueryChainWrapper<>(baseMapper)
                .eq(SysUserRole::getUserId, userId)
                .list();

        if (CollectionUtils.isNotEmpty(sysUserRoles)) {
            roleIds = sysUserRoles.stream().map(SysUserRole::getRoleId).distinct().collect(Collectors.toList());
        }
        return roleIds;
    }



}
