package com.wy.admin.scm.business.log.service;

import com.wy.admin.scm.business.log.model.AlarmLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 告警日志表 服务类
 * </p>
 *
 * @author wuyong
 * @since 2022-02-25
 */
public interface AlarmLogService extends IService<AlarmLog> {

}
