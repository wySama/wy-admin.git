package com.wy.admin.scm.business.log.service;

import com.wy.admin.scm.business.log.model.SysOperLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 操作日志表 服务类
 * </p>
 *
 * @author wuyong
 * @since 2022-02-18
 */
public interface SysOperLogService extends IService<SysOperLog> {

}
