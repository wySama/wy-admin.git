package com.wy.admin.scm.business.system.vo.response;

import com.wy.admin.scm.business.system.dto.SysMenuDTO;
import com.wy.admin.scm.business.system.dto.SysUserDetailDTO;
import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class LoginResVO {

    private String token;

    private SysUserDetailDTO sysUser;

    private Set<String> roles;

    private List<SysMenuDTO> menus;
}
