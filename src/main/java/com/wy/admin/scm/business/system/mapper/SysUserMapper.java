package com.wy.admin.scm.business.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wy.admin.scm.business.system.model.SysUser;
import com.wy.admin.scm.business.system.vo.request.SysUserReqVO;
import com.wy.admin.scm.business.system.vo.response.SysUserResVO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author wuyong
 * @since 2021-12-16
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    IPage<SysUserResVO> selectUserList(@Param("page") Page<SysUserResVO> page, @Param("req") SysUserReqVO reqVO);
}
