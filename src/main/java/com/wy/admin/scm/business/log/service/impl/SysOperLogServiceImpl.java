package com.wy.admin.scm.business.log.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wy.admin.scm.business.common.model.Pagination;
import com.wy.admin.scm.business.log.mapper.SysOperLogMapper;
import com.wy.admin.scm.business.log.model.SysOperLog;
import com.wy.admin.scm.business.log.service.SysOperLogService;
import com.wy.admin.scm.business.log.vo.request.SysOperLogReqVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 操作日志表 服务实现类
 * </p>
 *
 * @author wuyong
 * @since 2022-02-18
 */
@Service
public class SysOperLogServiceImpl extends ServiceImpl<SysOperLogMapper, SysOperLog> implements SysOperLogService {


    /**
     * 分页查询登录日志
     */
    public IPage<SysOperLog> querySysOperLogPage(SysOperLogReqVO reqVO) {
        Pagination pagination = reqVO.getPagination();
        Page<SysOperLog> page = new Page<>(pagination.getCurrent(), pagination.getPageSize());

        LambdaQueryWrapper<SysOperLog> queryChainWrapper = new LambdaQueryWrapper<>();

        if (null != reqVO.getOperStatus()) {
            queryChainWrapper.eq(SysOperLog::getOperStatus, reqVO.getOperStatus());
        }
        if (StringUtils.isNotBlank(reqVO.getOperName())) {
            queryChainWrapper.eq(SysOperLog::getOperName, reqVO.getOperName());
        }

        if (StringUtils.isNotBlank(reqVO.getTitle())) {
            queryChainWrapper.eq(SysOperLog::getTitle, reqVO.getTitle());
        }
        if (StringUtils.isNotBlank(reqVO.getStartTime())) {
            queryChainWrapper.gt(SysOperLog::getOperDate, reqVO.getStartTime());
        }
        if (StringUtils.isNotBlank(reqVO.getEndTime())) {
            queryChainWrapper.lt(SysOperLog::getOperDate, reqVO.getEndTime());
        }

        queryChainWrapper.orderByDesc(SysOperLog::getId);
        return this.baseMapper.selectPage(page, queryChainWrapper);
    }


}
