package com.wy.admin.scm.business.system.service.impl;

import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wy.admin.scm.business.common.base.constant.StringConstant;
import com.wy.admin.scm.business.common.base.constant.TipsConstant;
import com.wy.admin.scm.business.common.dto.DropdownDTO;
import com.wy.admin.scm.business.common.model.PageResult;
import com.wy.admin.scm.business.common.model.Pagination;
import com.wy.admin.scm.business.system.dto.SysRoleDTO;
import com.wy.admin.scm.business.system.mapper.SysRoleMapper;
import com.wy.admin.scm.business.system.mapstruct.SystemCovertMapper;
import com.wy.admin.scm.business.system.model.SysRole;
import com.wy.admin.scm.business.system.service.SysRoleService;
import com.wy.admin.scm.business.system.vo.request.SaveSysRoleReqVO;
import com.wy.admin.scm.business.system.vo.request.SysRoleReqVO;
import com.wy.admin.scm.business.system.vo.request.UpdateSysRoleReqVO;
import com.wy.admin.scm.business.system.vo.response.SysRoleResVO;
import com.wy.admin.scm.core.framework.lang.enums.CommonReturnEnum;
import com.wy.admin.scm.core.framework.lang.exception.BusinessException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;


/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author wuyong
 * @since 2021-12-16
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

    @Autowired
    private SystemCovertMapper systemCovertMapper;

    @Autowired
    private SysRoleMenuServiceImpl sysRoleMenuService;

    @Autowired
    private SysUserRoleServiceImpl sysUserRoleService;


    /**
     * 分页获取角色
     */
    public PageResult<SysRoleResVO> querySysRolePage(SysRoleReqVO reqVO) {
        Pagination pagination = reqVO.getPagination();
        Page<SysRole> page = new Page<>(pagination.getCurrent(), pagination.getPageSize());


        LambdaQueryChainWrapper<SysRole> queryChainWrapper = new LambdaQueryChainWrapper<>(baseMapper)
                .eq(SysRole::getActiveFlag, StringConstant.Y);

        if (StringUtils.isNotBlank(reqVO.getRoleName())) {
            queryChainWrapper.eq(SysRole::getRoleName, reqVO.getRoleName());
        }

        Page<SysRole> sysRolePage = queryChainWrapper.page(page);
        PageResult<SysRoleResVO> result = new PageResult<>();
        result.setPagination(new Pagination(sysRolePage.getSize(), sysRolePage.getCurrent(), sysRolePage.getTotal()));
        if (CollectionUtils.isNotEmpty(sysRolePage.getRecords())) {
            List<SysRoleResVO> records = systemCovertMapper.covertSysRoleResVOList(sysRolePage.getRecords());
            result.setRecords(records);
        }

        return result;
    }


    public List<SysRole> getSysRoleList() {
        return new LambdaQueryChainWrapper<>(baseMapper)
                .eq(SysRole::getActiveFlag, StringConstant.Y)
                .list();
    }


    /**
     * 获取所有角色下拉框选项
     */
    public Map<String, List<DropdownDTO>> getSysRoleDropdownList() {
        Map<String, List<DropdownDTO>> res = new HashMap<>();
        List<DropdownDTO> sysRoleList = new ArrayList<>();
        List<SysRole> sysRoles = getSysRoleList();
        if (CollectionUtils.isNotEmpty(sysRoles)) {
            sysRoles.forEach(item -> {
                DropdownDTO<Integer> dropdownDTO = new DropdownDTO<>();
                dropdownDTO.setLabel(item.getRoleName());
                dropdownDTO.setValue(item.getId());
                sysRoleList.add(dropdownDTO);

            });
        }
        res.put("sysRoleList", sysRoleList);
        return res;
    }


    /**
     * 保存单个角色详情
     */
    @Transactional
    public void saveSysRole(SaveSysRoleReqVO reqVO) {
        SysRole sysRole = systemCovertMapper.covertSysMenuBySaveSysRoleReqVO(reqVO);
        sysRole.setCreatedBy(reqVO.getUserId());
        this.save(sysRole);
        sysRoleMenuService.saveSysRoleMenu(sysRole.getId(), reqVO.getMenuIds());
    }

    /**
     * 获取单个角色详情
     */
    public SysRoleDTO getSysRoleDetail(Integer id) {
        SysRole sysRole = this.getById(id);
        SysRoleDTO sysRoleDTO = systemCovertMapper.covertSysRoleDTO(sysRole);
        List<Integer> menuIds = sysRoleMenuService.getMenusByRoleId(id);

        sysRoleDTO.setMenuIds(menuIds);
        return sysRoleDTO;
    }

    /**
     * 更新单个角色详情
     */
    @Transactional
    public void updateSysRole(UpdateSysRoleReqVO reqVO) {
        SysRole sysRole = this.getById(reqVO.getId());
        if (sysRole == null) {
            throw new BusinessException(CommonReturnEnum.CHECK_ERROR, TipsConstant.SYSROLE_NOT_EXIT);
        }
        sysRole = systemCovertMapper.covertSysMenuByUpdateSysRoleReqVO(reqVO);
        this.updateById(sysRole);

        sysRoleMenuService.updateSysRoleMenu(sysRole.getId(), reqVO.getMenuIds());
    }


    /**
     * 删除单个角色
     */
    @Transactional
    public void deleteSysRole(Integer id) {
        this.removeById(id);
        sysRoleMenuService.removeRoleMenuByRoleId(id);
    }


    public Set<String> getUserRoleListByUserId(Integer userId) {
        List<Integer> roleIds = sysUserRoleService.getRoleIdsByUserId(userId);
        if (CollectionUtils.isEmpty(roleIds)) {
            return new HashSet<>();
        }

        List<SysRole> sysRoles = this.listByIds(roleIds);
        if (CollectionUtils.isEmpty(sysRoles)) {
            return new HashSet<>();
        }
        List<String> roleCodeList = sysRoles.stream().map(SysRole::getRoleCode).distinct().collect(Collectors.toList());
        return new HashSet<>(roleCodeList);

    }

}
