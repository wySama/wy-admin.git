package com.wy.admin.scm.business.common.base.enums;

public enum DataFrameTypeEnum {

    /**
     * 数据帧类型
     */
    QUERY_COMMAND("查询命令", "$ZSHCXML", "平台→服务器", "平台主动发起的查询信息"),
    ALARM_ACKNOWLEDGEMENT("告警确认", "$ZSHGJQR", "平台→服务器", "平台对异常告警或撤消告警信息的确认"),
    EQUIPMENT_INFO_REPORT("设备信息上报", "$ZSHSBXX", "服务器→平台", "服务器收到查询命令后上报的设备信息"),
    CONFIGURE_INFO_REPORT("配置信息上报", "$ZSHPZXX", "服务器→平台", "服务器收到查询命令后上报的配置信息"),
    WORKING_CONDITION_REPORT("工作状态上报", "$ZSHGZZT", "服务器→平台", "服务器收到查询命令后上报的工作状态信息"),
    WORKING_LOG_REPORT("工作日志上报", "$ZSHGZRZ", "服务器→平台", "服务器收到查询命令后上报的工作日志信息"),
    QUERY_RESPONSE("查询响应", "$ZSHCXXY", "服务器→平台", "服务器对不支持的查询命令的确认信息"),
    ABNORMAL_ALARM ("异常告警", "$ZSHYCGJ", "服务器→平台", "服务器主动发送的异常告警信息"),
    CANCEL_ALARM("撤消告警", "$ZSHCXGJ", "服务器→平台", "服务器主动发送的撤消告警信息"),

    ;

    // 帧头	传输方向	说明
    private String name;
    private String code;
    private String transmissionDirection;
    private String desc;

    DataFrameTypeEnum(String name, String code, String transmissionDirection, String desc) {
        this.name = name;
        this.code = code;
        this.transmissionDirection = transmissionDirection;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }
}
