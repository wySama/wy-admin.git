package com.wy.admin.scm.business.equipment.controller;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.wy.admin.scm.business.common.base.enums.BusinessType;
import com.wy.admin.scm.business.common.dto.DropdownDTO;
import com.wy.admin.scm.business.company.vo.request.CompanyReqVO;
import com.wy.admin.scm.business.equipment.dto.EquipmentDTO;
import com.wy.admin.scm.business.equipment.dto.SatellitePositionDTO;
import com.wy.admin.scm.business.equipment.model.Equipment;
import com.wy.admin.scm.business.equipment.service.impl.EquipmentServiceImpl;
import com.wy.admin.scm.business.equipment.vo.request.EquipmentReqVO;
import com.wy.admin.scm.core.framework.aspect.Log;
import com.wy.admin.scm.core.framework.model.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 设备信息表 前端控制器
 * </p>
 *
 * @author wuyong
 * @since 2022-02-16
 */
@RestController
@RequestMapping("/api/equipment")
public class EquipmentRestController {

    @Autowired
    private EquipmentServiceImpl equipmentService;

    /**
     * 分页获取设备
     */
    @PostMapping("queryEquipmentPage")
    public JsonResult<IPage<EquipmentDTO>> queryEquipmentPage(@RequestBody EquipmentReqVO reqVO) {
        IPage<EquipmentDTO> resVOIPage = equipmentService.queryEquipmentPage(reqVO);
        return new JsonResult<>(resVOIPage);
    }

    /**
     * 导出设备
     */
    @PostMapping("/exportEquipment")
    public void exportEquipment(HttpServletResponse response, @RequestBody EquipmentReqVO reqVO) {
        equipmentService.exportEquipment(response, reqVO);
    }


    @GetMapping("getEquipmentDetail")
    public JsonResult<EquipmentDTO> getEquipmentDetail(Integer id) {
        EquipmentDTO res = equipmentService.getEquipmentDetail(id);
        return new JsonResult<>(res);
    }

    @Log(title = "设备管理", businessType = BusinessType.INSERT)
    @PostMapping("saveEquipment")
    public JsonResult<Equipment> saveEquipment(@RequestBody EquipmentDTO equipmentDTO) {
        Equipment res = equipmentService.saveEquipment(equipmentDTO);
        return new JsonResult<>(res);
    }

    @Log(title = "设备管理", businessType = BusinessType.UPDATE)
    @PostMapping("updateEquipment")
    public JsonResult<Equipment> updateEquipment(@RequestBody EquipmentDTO equipmentDTO) {
        Equipment res = equipmentService.updateEquipment(equipmentDTO);
        return new JsonResult<>(res);
    }

    @Log(title = "设备管理", businessType = BusinessType.DELETE)
    @GetMapping("deleteEquipment")
    public JsonResult<JSONObject> deleteEquipment(Integer id) {
        equipmentService.deleteEquipment(id);
        return new JsonResult<>(new JSONObject());
    }

    @GetMapping("getSatellitePositionDTOList")
    public JsonResult<List<SatellitePositionDTO>> getSatellitePositionDTOList(Integer id) {
        List<SatellitePositionDTO> satellitePositionDTOS = equipmentService.getSatellitePositionDTOList(id);
        return new JsonResult<>(satellitePositionDTOS);
    }

    @GetMapping("getEquipmentDropdown")
    public JsonResult<Map<String, List<DropdownDTO<Integer>>>> getEquipmentDropdown() {
        Map<String, List<DropdownDTO<Integer>>> res = equipmentService.getEquipmentDropdown();
        return new JsonResult<>(res);
    }

    @GetMapping("alarm")
    public JsonResult<JSONObject> alarm(Integer userId) {
        equipmentService.alarm(userId);
        return new JsonResult<>(new JSONObject());
    }


}

