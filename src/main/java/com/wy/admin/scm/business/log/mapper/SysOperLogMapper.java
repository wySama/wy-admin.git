package com.wy.admin.scm.business.log.mapper;

import com.wy.admin.scm.business.log.model.SysOperLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 操作日志表 Mapper 接口
 * </p>
 *
 * @author wuyong
 * @since 2022-02-18
 */
public interface SysOperLogMapper extends BaseMapper<SysOperLog> {

}
