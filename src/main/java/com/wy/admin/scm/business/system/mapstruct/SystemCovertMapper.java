package com.wy.admin.scm.business.system.mapstruct;

import com.wy.admin.scm.business.common.component.LocalDateTimeFormat;
import com.wy.admin.scm.business.system.dto.SysMenuDTO;
import com.wy.admin.scm.business.system.dto.SysRoleDTO;
import com.wy.admin.scm.business.system.dto.SysUserDetailDTO;
import com.wy.admin.scm.business.system.model.SysMenu;
import com.wy.admin.scm.business.system.model.SysRole;
import com.wy.admin.scm.business.system.model.SysUser;
import com.wy.admin.scm.business.system.vo.request.SaveSysMenuReqVO;
import com.wy.admin.scm.business.system.vo.request.SaveSysRoleReqVO;
import com.wy.admin.scm.business.system.vo.request.UpdateSysMenuReqVO;
import com.wy.admin.scm.business.system.vo.request.UpdateSysRoleReqVO;
import com.wy.admin.scm.business.system.vo.response.SysRoleResVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = LocalDateTimeFormat.class)
public interface SystemCovertMapper {

    @Mapping(source = "creationDate", target = "creationDate", dateFormat = "yyyy-MM-dd HH:mm:ss")
    SysUserDetailDTO covertSysUserDetailVO(SysUser sysUser);

    @Mapping(target = "birthDate", source = "birthDate", qualifiedByName = "stringToLocalDate")
    @Mapping(target = "creationDate", source = "creationDate", qualifiedByName = "stringToLocalDateTime")
    SysUser covertSysUser(SysUserDetailDTO sysUser);


    @Mapping(source = "creationDate", target = "creationDate", dateFormat = "yyyy-MM-dd HH:mm:ss")
    SysMenuDTO covertSysMenuVO(SysMenu sysMenu);

    List<SysMenuDTO> covertSysMenuVOS(List<SysMenu> sysMenuList);

    @Mapping(target = "modifiedBy", source = "userId")
    SysMenu covertSysMenu(SaveSysMenuReqVO reqVO);


    @Mapping(target = "modifiedBy", source = "userId")
    SysMenu covertSysMenuByUpdateSysMenuReqVO(UpdateSysMenuReqVO reqVO);


    UpdateSysMenuReqVO covertUpdateSysMenuReqVO(SysMenu reqVO);


    @Mapping(source = "creationDate", target = "creationDate", dateFormat = "yyyy-MM-dd HH:mm:ss")
    @Mapping(source = "modifyDate", target = "modifyDate", dateFormat = "yyyy-MM-dd HH:mm:ss")
    SysRoleResVO covertSysRoleResVO(SysRole sysRole);

    List<SysRoleResVO> covertSysRoleResVOList(List<SysRole> sysRoleList);


    @Mapping(target = "modifiedBy", source = "userId")
    SysRole covertSysMenuBySaveSysRoleReqVO(SaveSysRoleReqVO reqVO);


    @Mapping(target = "modifiedBy", source = "userId")
    SysRole covertSysMenuByUpdateSysRoleReqVO(UpdateSysRoleReqVO reqVO);


    @Mapping(source = "creationDate", target = "creationDate", dateFormat = "yyyy-MM-dd HH:mm:ss")
    @Mapping(source = "modifyDate", target = "modifyDate", dateFormat = "yyyy-MM-dd HH:mm:ss")
    SysRoleDTO  covertSysRoleDTO(SysRole reqVO);



}
