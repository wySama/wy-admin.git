package com.wy.admin.scm.business.system.controller;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.wy.admin.scm.business.common.base.enums.BusinessType;
import com.wy.admin.scm.business.common.dto.DropdownDTO;
import com.wy.admin.scm.business.common.model.PageResult;
import com.wy.admin.scm.business.system.dto.SysMenuDTO;
import com.wy.admin.scm.business.system.dto.SysRoleDTO;
import com.wy.admin.scm.business.system.dto.SysUserDetailDTO;
import com.wy.admin.scm.business.system.model.SysUser;
import com.wy.admin.scm.business.system.service.impl.SysMenuServiceImpl;
import com.wy.admin.scm.business.system.service.impl.SysRoleServiceImpl;
import com.wy.admin.scm.business.system.service.impl.SysUserServiceImpl;
import com.wy.admin.scm.business.system.vo.request.*;
import com.wy.admin.scm.business.system.vo.response.SysRoleResVO;
import com.wy.admin.scm.business.system.vo.response.SysUserResVO;
import com.wy.admin.scm.core.framework.aspect.Log;
import com.wy.admin.scm.core.framework.model.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/sys")
public class SystemRestController {

    @Autowired
    private SysUserServiceImpl sysUserService;

    @Autowired
    private SysRoleServiceImpl sysRoleService;

    @Autowired
    private SysMenuServiceImpl sysMenuService;


    /**
     * 分页获取用户详情
     */
    @PostMapping("querySysUserPage")
    public JsonResult<IPage<SysUserResVO>> querySysUserPage(@RequestBody SysUserReqVO sysUserReqVO) {
        IPage<SysUserResVO> resVOIPage = sysUserService.querySysUserPage(sysUserReqVO);
        return new JsonResult<>(resVOIPage);
    }


    /**
     * 保存用户详情
     */
    @Log(title = "用户管理", businessType = BusinessType.INSERT)
    @PostMapping("/saveSysUser")
    public JsonResult<SysUser> saveSaveSysUserReqVO(@RequestBody SaveSysUserReqVO reqVO) {
        SysUser sysUser = sysUserService.saveSaveSysUserReqVO(reqVO);
        return new JsonResult<>(sysUser);
    }


    /**
     * 获取用户详情
     */
    @GetMapping("/getSysUserDetail")
    public JsonResult<SysUserDetailDTO> getSysUserDetail(Integer id) {
        SysUserDetailDTO res = sysUserService.getSysUserDetail(id);
        return new JsonResult<>(res);
    }


    /**
     * 修改用户详情
     */
    @Log(title = "用户管理", businessType = BusinessType.UPDATE)
    @PostMapping("/updateSysUser")
    public JsonResult<JSONObject> updateSysUser(@RequestBody SysUserDetailDTO reqVO) {
        sysUserService.updateSysUser(reqVO);
        return new JsonResult<>(new JSONObject());
    }

    /**
     * 删除用户
     */
    @Log(title = "用户管理", businessType = BusinessType.DELETE)
    @GetMapping("/deleteSysUser")
    public JsonResult<JSONObject> deleteSysUser(Integer id) {
        sysUserService.deleteSysUser(id);
        return new JsonResult<>(new JSONObject());
    }


    /**
     * 校验原密码
     */
    @PostMapping("/validateOldPass")
    public JsonResult<JSONObject> validateOldPass(@RequestBody UpdatePwdReqVO reqVO) {
        sysUserService.validateOldPass(reqVO);
        return new JsonResult<>(new JSONObject());
    }


    /**
     * 重置密码
     */
    @Log(title = "用户管理", businessType = BusinessType.RESET_PWD)
    @PostMapping("/resetPwd")
    public JsonResult<JSONObject> resetPwd(@RequestBody UpdatePwdReqVO reqVO) {
        sysUserService.resetPwd(reqVO);
        return new JsonResult<>(new JSONObject());
    }


    @Log(title = "用户管理", businessType = BusinessType.RESET_PWD)
    @GetMapping("/resetPwdByRandom/{id}/{userId}")
    public JsonResult<String> resetPwdByRandom(@PathVariable("id") Integer id, @PathVariable("userId") Integer userId) {
        String pwd = sysUserService.resetPwdByRandom(id, userId);
        return new JsonResult<>(pwd);
    }

    /**
     * 获取所有角色下拉框选项
     */
    @GetMapping("/getSysRoleDropdownList")
    public JsonResult<Map<String, List<DropdownDTO>>> getSysRoleDropdownList() {
        Map<String, List<DropdownDTO>> res = sysRoleService.getSysRoleDropdownList();
        return new JsonResult<>(res);
    }


    /**
     * 根据条件查询菜单列表
     */
    @PostMapping("/getSysMenuDTOS")
    public JsonResult<List<SysMenuDTO>> getSysMenuDTOS(@RequestBody SysMenuReqVO reqVO) {
        List<SysMenuDTO> sysMenuDTOS = sysMenuService.getSysMenuDTOS(reqVO);
        return new JsonResult<>(sysMenuDTOS);
    }

    /**
     * 获取菜单下拉框选项
     */
    @GetMapping("/getMenuTreeData")
    public JsonResult<List<SysMenuDTO>> getMenuTreeData(Integer userId) {
        List<SysMenuDTO> sysMenuDTOS = sysMenuService.getMenuTreeData(userId);
        return new JsonResult<>(sysMenuDTOS);
    }


    /**
     * 新增菜单
     */
    @Log(title = "菜单管理", businessType = BusinessType.INSERT)
    @PostMapping("/saveSysMenu")
    public JsonResult<JSONObject> addSysMenu(@RequestBody SaveSysMenuReqVO reqVO) {
        sysMenuService.addSysMenu(reqVO);
        return new JsonResult<>(new JSONObject());
    }

    /**
     * 获取菜单详情
     */
    @GetMapping("/getSysMenuDetail")
    public JsonResult<UpdateSysMenuReqVO> getSysMenuDetail(Integer id) {
        UpdateSysMenuReqVO result = sysMenuService.getSysMenuDetail(id);
        return new JsonResult<>(result);
    }

    /**
     * 修改菜单
     */
    @Log(title = "菜单管理", businessType = BusinessType.UPDATE)
    @PostMapping("/updateSysMenu")
    public JsonResult<JSONObject> updateSysMenu(@RequestBody UpdateSysMenuReqVO reqVO) {
        sysMenuService.updateSysMenu(reqVO);
        return new JsonResult<>(new JSONObject());
    }

    /**
     * 删除菜单
     */
    @Log(title = "菜单管理", businessType = BusinessType.DELETE)
    @GetMapping(value = "/deleteSysMenu")
    public JsonResult<JSONObject> deleteSysMenu(Integer id) {
        sysMenuService.deleteSysMenu(id);
        return new JsonResult<>(new JSONObject());
    }


    /**
     * 分页获取角色
     */
    @PostMapping("querySysRolePage")
    public JsonResult<PageResult<SysRoleResVO>> querySysRolePage(@RequestBody SysRoleReqVO reqVO) {
        PageResult<SysRoleResVO> result = sysRoleService.querySysRolePage(reqVO);
        return new JsonResult<>(result);
    }


    /**
     * 保存角色详情
     */
    @Log(title = "角色管理", businessType = BusinessType.INSERT)
    @PostMapping("/saveSysRole")
    public JsonResult<JSONObject> saveSysRole(@RequestBody SaveSysRoleReqVO reqVO) {
        sysRoleService.saveSysRole(reqVO);
        return new JsonResult<>(new JSONObject());
    }


    /**
     * 获取单个角色详情
     */
    @GetMapping(value = "/getSysRoleDetail")
    public JsonResult<SysRoleDTO> getSysRoleDetail(Integer id) {
        SysRoleDTO result = sysRoleService.getSysRoleDetail(id);
        return new JsonResult<>(result);
    }


    /**
     * 修改角色
     */
    @Log(title = "角色管理", businessType = BusinessType.UPDATE)
    @PostMapping("/updateSysRole")
    public JsonResult<JSONObject> updateSysRole(@RequestBody UpdateSysRoleReqVO reqVO) {
        sysRoleService.updateSysRole(reqVO);
        return new JsonResult<>(new JSONObject());
    }


    /**
     * 删除单个角色
     */
    @Log(title = "角色管理", businessType = BusinessType.DELETE)
    @GetMapping(value = "/deleteSysRole")
    public JsonResult<JSONObject> deleteSysRole(Integer id) {
        sysRoleService.deleteSysRole(id);
        return new JsonResult<>(new JSONObject());
    }


}
