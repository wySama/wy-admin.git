package com.wy.admin.scm.business.equipment.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class SatellitePositionDTO implements Serializable {

    private static final long serialVersionUID = -7375273032996765793L;
    private String type;
    private Integer num;
    private Integer elevation;
    private Integer azimuth;

    public SatellitePositionDTO() {
    }

    public SatellitePositionDTO(String type, Integer num, Integer elevation, Integer azimuth) {
        this.type = type;
        this.num = num;
        this.elevation = elevation;
        this.azimuth = azimuth;
    }
}
