package com.wy.admin.scm.business.equipment.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wy.admin.scm.business.common.base.constant.StringConstant;
import com.wy.admin.scm.business.common.dto.DropdownDTO;
import com.wy.admin.scm.business.common.model.Pagination;
import com.wy.admin.scm.business.common.service.impl.CommonService;
import com.wy.admin.scm.business.equipment.dto.EquipmentDTO;
import com.wy.admin.scm.business.equipment.dto.SatellitePositionDTO;
import com.wy.admin.scm.business.equipment.mapper.EquipmentMapper;
import com.wy.admin.scm.business.equipment.mapstruct.EquipmentCovertMapper;
import com.wy.admin.scm.business.equipment.model.Equipment;
import com.wy.admin.scm.business.equipment.service.EquipmentService;
import com.wy.admin.scm.business.equipment.vo.request.EquipmentReqVO;
import com.wy.admin.scm.business.system.model.SysUser;
import com.wy.admin.scm.core.framework.config.NettyConfig;
import com.wy.admin.scm.core.framework.util.ShiroUtils;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * <p>
 * 设备信息表 服务实现类
 * </p>
 *
 * @author wuyong
 * @since 2022-02-16
 */
@Service
public class EquipmentServiceImpl extends ServiceImpl<EquipmentMapper, Equipment> implements EquipmentService {

    @Autowired
    private EquipmentCovertMapper equipmentCovertMapper;

    @Autowired
    private CommonService commonService;

    /**
     * 分页获取设备
     */
    public IPage<EquipmentDTO> queryEquipmentPage(EquipmentReqVO reqVO) {
        Pagination pagination = reqVO.getPagination();
        Page<EquipmentDTO> page = new Page<>(pagination.getCurrent(), pagination.getPageSize());
        return this.baseMapper.queryEquipmentPage(page, reqVO);
    }

    /**
     * 获取导出数据
     */
    private List<EquipmentDTO> queryEquipmentList(EquipmentReqVO reqVO) {
        return this.baseMapper.queryEquipmentList(reqVO);
    }

    /**
     * 导出设备
     */
    public void exportEquipment(HttpServletResponse response, EquipmentReqVO reqVO) {
        List<EquipmentDTO> list = queryEquipmentList(reqVO);
        commonService.commonExport(response, EquipmentDTO.class, list);
    }


    public EquipmentDTO getEquipmentDetail(Integer id) {
        Equipment equipment = this.getById(id);
        return equipmentCovertMapper.covertEquipmentDTO(equipment);
    }

    public Equipment saveEquipment(EquipmentDTO equipmentDTO) {
        Equipment equipment = equipmentCovertMapper.covertEquipment(equipmentDTO);
        equipment.setCreatedBy(equipmentDTO.getUserId());
        this.save(equipment);
        return equipment;
    }


    public Equipment updateEquipment(EquipmentDTO equipmentDTO) {
        Equipment equipment = equipmentCovertMapper.covertEquipment(equipmentDTO);
        this.updateById(equipment);
        return equipment;
    }


    public void deleteEquipment(Integer id) {
        this.removeById(id);
    }


    public void queryEquipment(Integer id) {
        Equipment equipment = this.getById(id);


    }


    public List<SatellitePositionDTO> getSatellitePositionDTOList(Integer id) {
        //  Equipment equipment = this.getById(id);

        Integer s = getRandom(10, 20);

        List<SatellitePositionDTO> satellitePositionDTOS = new ArrayList<>();
        for (int i = 0; i < s; i++) {
            Integer type = getRandom(1, 2);
            Integer num = getRandom(1, 100);
            Integer elevation = getRandom(0, 90);
            Integer azimuth = getRandom(0, 360);
            SatellitePositionDTO dto = new SatellitePositionDTO(String.valueOf(type), num, elevation, azimuth);
            satellitePositionDTOS.add(dto);
        }

        return satellitePositionDTOS;

    }


    public static int getRandom(int min, int max) {
        Random random = new Random();
        int s = random.nextInt(max) % (max - min + 1) + min;
        return s;

    }


    /**
     * 设备信息DropdownDTO
     */
    public Map<String, List<DropdownDTO<Integer>>> getEquipmentDropdown() {
        // 取身份信息
        SysUser user = ShiroUtils.getSysUser();

        List<Equipment> equipmentList;
        Boolean isAdmin = commonService.isAdmin(user);
        if (Boolean.TRUE.equals(isAdmin)) {
            equipmentList = new LambdaQueryChainWrapper<>(baseMapper)
                    .eq(Equipment::getActiveFlag, StringConstant.Y)
                    .orderByDesc(Equipment::getId)
                    .list();
        } else {
            equipmentList = new LambdaQueryChainWrapper<>(baseMapper)
                    .eq(Equipment::getActiveFlag, user.getCompanyId())
                    .eq(Equipment::getCompanyId, StringConstant.Y)
                    .orderByDesc(Equipment::getId)
                    .list();
        }

        Map<String, List<DropdownDTO<Integer>>> res = new HashMap<>();

        List<DropdownDTO<Integer>> dropdownDTOList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(equipmentList)) {
            equipmentList.forEach(item -> {
                DropdownDTO<Integer> dropdownDTO = new DropdownDTO<>();
                dropdownDTO.setLabel(item.getIpAddress());
                dropdownDTO.setValue(item.getId());
                dropdownDTOList.add(dropdownDTO);
            });
        }
        res.put("equipmentList", dropdownDTOList);
        return res;

    }

    //告警
    public void alarm(Integer userId) {
        String uid = "home_" + userId;
        Channel channel = NettyConfig.getChannel(uid);
        if (null != channel) {
            JSONObject res = new JSONObject();
            res.put("code", true);
            res.put("type", 2);
            res.put("msg", "设备 XXXX 在2022-02-23 14:10:15 发生告警,请尽快处理。");
            channel.writeAndFlush(new TextWebSocketFrame(res.toJSONString()));
        }
    }

}
