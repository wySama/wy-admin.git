package com.wy.admin.scm.business.system.vo.request;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

/**
 * 用户查询DTO
 *
 * @author w
 * @date 2021-03-05
 */
@Data
public class SaveSysUserReqVO {

    private String loginName;

    private String userName;

    private String phone;

    private Integer sex;

    private LocalDate birthDate;

    private String address;

    private String province;

    private String city;

    private String district;

    private String activeFlag;

    private List<Integer> roleIds;
}
