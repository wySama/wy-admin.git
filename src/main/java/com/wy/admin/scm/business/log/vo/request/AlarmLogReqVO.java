package com.wy.admin.scm.business.log.vo.request;

import com.wy.admin.scm.business.common.model.Pagination;
import lombok.Data;

@Data
public class AlarmLogReqVO {

    /**
     * 单位ID
     */
    private Integer companyId;

    /**
     * 设备ID
     */
    private Integer equipmentId;

    /**
     * 告警的序号
     */
    private Integer alarmId;

    /**
     * 告警类型（0:撤销告警 1:产生告警）
     */
    private Integer alarmType;

    /**
     * 开始时间
     */
    private String startTime;

    /**
     * 结束时间
     */
    private String endTime;


    private Pagination pagination;

}
