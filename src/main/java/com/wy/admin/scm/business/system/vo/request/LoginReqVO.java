package com.wy.admin.scm.business.system.vo.request;

import lombok.Data;

@Data
public class LoginReqVO {

    private String loginName;
    private String pwd;
    private String captcha;

}
