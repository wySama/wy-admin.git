package com.wy.admin.scm.business.company.dto;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.wy.admin.scm.business.common.component.ActiveFlagConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode
@ColumnWidth(25)
public class CompanyDTO implements Serializable {

    private static final long serialVersionUID = 6197904719105341295L;
    @ExcelIgnore
    private Integer userId;

    @ExcelIgnore
    private Integer id;

    /**
     * 单位名称
     */
    @ExcelProperty("单位名称")
    private String companyName;

    /**
     * 单位简称
     */
    @ExcelProperty("单位简称")
    private String companyAbbreviation;

    /**
     * 负责人名称
     */
    @ExcelProperty("负责人名称")
    private String leaderName;

    /**
     * 负责人手机号
     */
    @ExcelProperty("负责人手机号")
    private String leaderPhone;

    /**
     * 单位地址
     */
    @ExcelProperty("单位地址")
    private String address;

    /**
     * 备注
     */
    @ExcelProperty("备注")
    private String remark;

    /**
     * 是否有效, Y:有效 N:无效
     */
    @ExcelProperty(value = "是否启用", converter = ActiveFlagConverter.class)
    private String activeFlag;

    /**
     * 创建日期
     */
    @ExcelProperty("创建日期")
    private String creationDateStr;

    /**
     * 创建人名称
     */
    @ExcelProperty("创建人名称")
    private String createdByName;

    /**
     * 修改日期
     */
    @ExcelProperty("修改日期")
    private String modifyDateStr;

    /**
     * 修改人名称
     */
    @ExcelProperty("修改人名称")
    private String modifiedByName;

}
