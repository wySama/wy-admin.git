package com.wy.admin.scm.business.tool.mapper;

import com.wy.admin.scm.business.tool.model.CommonDictType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 字典类型表 Mapper 接口
 * </p>
 *
 * @author wuyong
 * @since 2022-02-17
 */
public interface CommonDictTypeMapper extends BaseMapper<CommonDictType> {

}
