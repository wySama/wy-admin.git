package com.wy.admin.scm.business.log.vo.request;

import com.wy.admin.scm.business.common.model.Pagination;
import lombok.Data;

import java.util.List;

@Data
public class SysLoginLogReqVO {
    /**
     * 登录状态：0失败 1成功
     */
    private Integer loginStatus;

    /**
     * 登录名
     */
    private String loginName;

    /**
     * 登录IP地址
     */
    private String loginIp;

    /**
     * 开始时间
     */
    private String startTime;

    /**
     * 结束时间
     */
    private String endTime;

    private Pagination pagination;


    private List<String> ids;
}
