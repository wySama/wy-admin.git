package com.wy.admin.scm.business.tool.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class DictDataDTO implements Serializable {
    private static final long serialVersionUID = -6955256993948647574L;
    private Integer id;
    private Integer userId;

    /**
     * common_dict_data主键
     */
    private Integer parentId;

    /**
     * code
     */
    private String dictCode;

    /**
     * value
     */
    private String dictValue;

    /**
     * 类型
     */
    private String dictType;

    /**
     * 字典排序
     */
    private Integer dictSort;

    /**
     * 备注
     */
    private String remark;

    /**
     * 是否有效, Y:有效 N:无效
     */
    private String activeFlag;
}
