package com.wy.admin.scm.core.framework.lang.util;

import org.springframework.util.Assert;

import java.lang.reflect.ParameterizedType;
import java.util.stream.Stream;


public class KanfitUtil {

    private static KanfitUtil instance;

    private KanfitUtil() {
    }

    public static KanfitUtil instance() {
        if (instance == null) {
            synchronized (KanfitUtil.class) {
                if (instance == null) {
                    instance = new KanfitUtil();
                }
            }
        }
        return instance;
    }



    /**
     * 获取指定对象申明的第一个泛型类型
     *
     * @param obj
     * @return
     */
    public Class<?> get1stGenericCls(Object obj) {
        return getGenericCls(obj, 1, null);
    }

    /**
     * 获取指定对象申明的第一个泛型类型
     *
     * @param obj
     * @param parentClassOrInterface 指定是哪个父类或接口的第一个泛型类型
     * @return
     */
    public Class<?> get1stGenericCls(Object obj, Class<?> parentClassOrInterface) {
        return getGenericCls(obj, 1, parentClassOrInterface);
    }

    /**
     * 获取指定对象申明的泛型类型
     *
     * @param obj
     * @param idx                    需要获取的第几个泛型，base on 1
     * @param parentClassOrInterface 指定是哪个父类或接口的泛型类型
     * @return
     */
    public Class<?> getGenericCls(Object obj, int idx, Class<?> parentClassOrInterface) {
        return getGenericClses(obj, parentClassOrInterface)[idx - 1];
    }

    private Class<?>[] getGenericClses(Object obj) {
        ParameterizedType pType = getParameterizedType(obj.getClass());
        if (pType != null) {
            return Stream.of(pType.getActualTypeArguments()).map(t -> (Class<?>) t).toArray(Class[]::new);
        }
        return null;
    }

    public Class<?>[] getGenericClses(Object obj, Class<?> parentClassOrInterface) {
        validate(obj, parentClassOrInterface);
        if (parentClassOrInterface == null) {
            return getGenericClses(obj);
        }
        ParameterizedType pType = getParameterizedType(parentClassOrInterface, obj.getClass());
        if (pType != null) {
            return Stream.of(pType.getActualTypeArguments()).map(t -> t instanceof ParameterizedType ? ((ParameterizedType) t).getRawType() : (Class<?>) t).toArray(Class[]::new);
        }
        return null;
    }

    private void validate(Object obj, Class<?> parentClassOrInterface) {
        Assert.notNull(obj, "param: [obj] must not be null.");
        Class<?> cls = obj.getClass();
        // cls不能是Object的实例
        Assert.isTrue(!Object.class.equals(cls), "param: [obj].class must not be Object.class");
        Assert.isTrue(!(cls.getInterfaces().length == 0 && Object.class.equals(cls.getSuperclass())), "obj在没有实现任何接口时不能是Object的直接子类。");
        if (parentClassOrInterface != null) {
            // parentClassOrInterface不能是Object
            Assert.isTrue(!Object.class.equals(parentClassOrInterface), "param: [" + parentClassOrInterface + "] must not be Object.");
            // obj是parentClassOrInterface的子类实现
            Assert.isAssignable(parentClassOrInterface, cls, "param: obj(" + obj + ") must be subClass of [" + parentClassOrInterface + "].");
        }
    }

    private ParameterizedType getParameterizedType(Class<?> cls) {
        ParameterizedType pType = null;
        if (!cls.isInterface()) {
            // cls是类
            pType = (ParameterizedType) cls.getGenericSuperclass();
            // cls是Object的直接子类（即没有继承任何其他类），需要查询是否实现了接口（Object不是泛型类）
            if (Object.class.equals(pType.getRawType())) {
                // cls实现了至少一个接口
                if (cls.getGenericInterfaces().length > 0) {
                    pType = (ParameterizedType) cls.getGenericInterfaces()[0];
                }
            }
        } else {
            // cls是接口
            //cls实现了至少一个接口
            if (cls.getGenericInterfaces().length > 0) {
                pType = (ParameterizedType) cls.getGenericInterfaces()[0];
            }
        }
        return pType;
    }

    private ParameterizedType getParameterizedType(Class<?> parentClassOrInterface, Class<?> cls) {
        ParameterizedType pType = null;
        if (!parentClassOrInterface.isInterface()) {
            // parentClassOrInterface是类
            pType = (ParameterizedType) cls.getGenericSuperclass();
        } else {
            // parentClassOrInterface是接口
            pType = (ParameterizedType) Stream.of(cls.getGenericInterfaces()).filter(t -> parentClassOrInterface.equals(((ParameterizedType) t).getRawType())).findFirst().get();
        }
        return pType;
    }
}
