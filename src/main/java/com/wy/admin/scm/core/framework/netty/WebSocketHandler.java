package com.wy.admin.scm.core.framework.netty;


import com.alibaba.fastjson.JSONObject;
import com.wy.admin.scm.business.system.service.impl.SysUserServiceImpl;
import com.wy.admin.scm.core.framework.config.NettyConfig;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.util.AttributeKey;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@ChannelHandler.Sharable
@Slf4j
public class WebSocketHandler extends SimpleChannelInboundHandler<TextWebSocketFrame> {


    @Autowired
    private SysUserServiceImpl sysUserService;

    /**
     * 一旦连接，第一个被执行
     */
    @Override
    public void handlerAdded(ChannelHandlerContext ctx) {
        log.info("有新的客户端链接：[{}]", ctx.channel().id().asLongText());
        // 添加到channelGroup 通道组
        NettyConfig.getChannelGroup().add(ctx.channel());
    }

    /**
     * 读取数据
     */
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, TextWebSocketFrame msg) {
        log.info("服务器收到消息：{}", msg.text());
        // 获取用户ID,关联channel
        JSONObject jsonObject = JSONObject.parseObject(msg.text());
        String userId = jsonObject.getString("userId");
        String type = jsonObject.getString("type");
        String uid = type + "_" + userId;

        Channel channel = NettyConfig.getChannelMap().get(uid);
        if (null != channel) {
            //说明之前登录过
            JSONObject res = new JSONObject();
            res.put("code", true);
            res.put("type", 1);
            res.put("msg", "该账号异地登录，您被强制下线");
            sendMsg(channel, res.toJSONString());
        }

        //存放请求ID与channel的对应关系
        NettyConfig.getChannelMap().put(uid, ctx.channel());

        //AttributeKey: 可以理解为每个channel的标签 通过userId获取对应的channel
        AttributeKey<String> key = AttributeKey.valueOf("userId");
        ctx.channel().attr(key).setIfAbsent(uid);

        if (StringUtils.equalsIgnoreCase("home", type)) {
            processHome(ctx);
        } else if (StringUtils.equalsIgnoreCase("updateCompany", type)) {
            processCompany(ctx, jsonObject);
        } else if (StringUtils.equalsIgnoreCase("closeUpdateCompany", type)) {
            processCloseUpdateCompany(jsonObject);
        }

    }


    /**
     * 退出编辑页面
     *
     * @param jsonObject
     */
    private void processCloseUpdateCompany(JSONObject jsonObject) {
        Integer bussinessId = jsonObject.getInteger("bussinessId");
        Integer userId = jsonObject.getInteger("userId");
        String key = "updateCompany" + bussinessId;
        Integer id = NettyConfig.getBusinessMap().get(key);
        if (userId.equals(id)) {
            NettyConfig.getBusinessMap().remove(key);
        }
    }

    /**
     * 进入编辑页面
     *
     * @param ctx
     * @param jsonObject
     */
    private void processCompany(ChannelHandlerContext ctx, JSONObject jsonObject) {
        Integer userId = jsonObject.getInteger("userId");
        String type = jsonObject.getString("type");
        Integer bussinessId = jsonObject.getInteger("bussinessId");
        String key = type + bussinessId;

        Integer id = NettyConfig.getBusinessMap().get(key);
        if (null == id) {
            NettyConfig.getBusinessMap().put(key, userId);
            JSONObject res = new JSONObject();
            res.put("code", true);
            sendMsg(ctx.channel(), res.toJSONString());
        } else if (id.equals(userId)) {
            JSONObject res = new JSONObject();
            res.put("code", true);
            sendMsg(ctx.channel(), res.toJSONString());
        } else {
            String uid = type + "_" + id;
            Channel channel = NettyConfig.getChannelMap().get(uid);
            if (null != channel) {
                //通知当前页面不能操作
                String loginName = sysUserService.getLoginName(id);

                JSONObject res = new JSONObject();
                res.put("code", false);
                res.put("msg", loginName + " 正在当前页面编辑");
                sendMsg(ctx.channel(), res.toJSONString());
            } else {
                //通知当前页面能操作
                sendMsg(ctx.channel(), "true");
                NettyConfig.getBusinessMap().remove(key);
                NettyConfig.getBusinessMap().put(key, userId);
            }
        }
    }


    private void processHome(ChannelHandlerContext ctx) {
        JSONObject res = new JSONObject();
        res.put("msg", "服务器收到消息啦");
        sendMsg(ctx.channel(), res.toJSONString());
    }

    // 回复消息
    private void sendMsg(Channel channe, String msg) {
        channe.writeAndFlush(new TextWebSocketFrame(msg));
    }


    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        log.info("用户下线了:{}", ctx.channel().id().asLongText());
        // 删除通道
        NettyConfig.getChannelGroup().remove(ctx.channel());
        removeUserId(ctx);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        log.info("异常：{}", cause.getMessage());
        // 删除通道
        NettyConfig.getChannelGroup().remove(ctx.channel());
        removeUserId(ctx);
        ctx.close();
    }

    /**
     * 删除用户与channel的对应关系
     */
    private void removeUserId(ChannelHandlerContext ctx) {
        String channelId = ctx.channel().id().asLongText();
        AttributeKey<String> key = AttributeKey.valueOf("userId");
        String uid = ctx.channel().attr(key).get();
        Channel channel = NettyConfig.getChannelMap().get(uid);
        if (null != channel && channelId.equals(channel.id().asLongText())) {
            NettyConfig.getChannelMap().remove(uid);
        }
    }
}
