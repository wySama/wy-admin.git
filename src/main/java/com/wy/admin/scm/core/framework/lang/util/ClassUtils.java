package com.wy.admin.scm.core.framework.lang.util;

import org.reflections.Reflections;

import java.util.Set;

public class ClassUtils extends org.springframework.util.ClassUtils {
    private static ClassUtils instance = null;

    public static ClassUtils instance() {
        if (instance == null) {
            synchronized (ClassUtils.class) {
                if (instance == null) {
                    instance = new ClassUtils();
                }
            }
        }
        return instance;
    }

    /**
     * 获取指定父类/接口的所有后代类/实现类<br>
     * <span style="color: red;">全jvm扫描，可能会有性能问题，建议调用带[packagePrefix]参数的重载方法，并尽量缩小包范围以尽可能小的消耗性能</span>
     *
     * @param parentCls parentCls
     * @return Set
     */
    public <T> Set<Class<? extends T>> getDescendantClasses(Class<T> parentCls) {
        return new Reflections().getSubTypesOf(parentCls);
    }

    /**
     * 获取指定父类/接口的所有后代类/实现类
     *
     * @param parentCls
     * @return
     */
    public <T> Set<Class<? extends T>> getDescendantClasses(Class<T> parentCls, String packagePrefix) {
        return new Reflections(packagePrefix).getSubTypesOf(parentCls);
    }
}