package com.wy.admin.scm.core.framework.util;

import com.wy.admin.scm.business.system.model.SysUser;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;

public class ShiroUtils {
    /**
     * 获取当前用户Session
     */
    public static Session getSession() {
        return SecurityUtils.getSubject().getSession();
    }


    /**
     * 获取当前用户信息
     *
     * @return
     */
    public static SysUser getAdminInfo() {
        return (SysUser) SecurityUtils.getSubject().getPrincipal();
    }


    public static String getIp() {
        return getSubject().getSession().getHost();
    }

    public static Subject getSubject() {
        return SecurityUtils.getSubject();
    }

    public static SysUser getSysUser() {
        return (SysUser) getSubject().getPrincipal();
    }

    public static String getSessionId() {
        return String.valueOf(getSubject().getSession().getId());
    }
}
