package com.wy.admin.scm.core.framework.lang.response;

import org.springframework.context.support.ApplicationObjectSupport;

import java.util.Map;
import java.util.TreeMap;

public abstract class AbstractResponseUtils extends ApplicationObjectSupport {
    public static final String KEY_UUID = "uuid";
    public static final String KEY_RETURN_CODE = "code";
    public static final String KEY_STATUS = "status";
    public static final String KEY_MSG = "message";
    public static final String KEY_RESULT = "data";
    public static final String KEY_EXCEPTION = "exception";

    protected static <K, V> Map<K, V> buildResult(K key, V value) {
        Map<K, V> map = new TreeMap<>();
        if (key != null) {
            map.put(key, value);
        }
        return map;
    }
}