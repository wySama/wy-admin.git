package com.wy.admin.scm.core.framework.lang.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.io.CharStreams;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;

public class HttpServletUtils {

    private static final Logger logger = LoggerFactory.getLogger(HttpServletUtils.class);

    private HttpServletUtils() {
    }

    public static void setRequestIsViewAttribute(HttpServletRequest request, Object handler) {
        if (!(handler instanceof HandlerMethod)) {
            return;
        }

        // 判断是否为rest请求
        HandlerMethod hm = (HandlerMethod) handler;
        Method method = hm.getMethod();
        // 判断返回值是否为ModelAndView
        boolean isModelAndView = method.getReturnType().equals(ModelAndView.class);
        // 判断方法是否使用 @ResponseBody
        boolean isResponseBody = method.isAnnotationPresent(ResponseBody.class);
        // 判断方法是否使用 @RestController注解
        boolean isRestController = hm.getBeanType().isAnnotationPresent(RestController.class);
        request.setAttribute("method_return_is_view", isModelAndView && !isResponseBody && !isRestController);
    }

    public static void setRequestUuidAttribute(HttpServletRequest request) {
        request.setAttribute("uuid", getUuidFromRequest(request));
    }

    public static String getUuidFromRequestAttribute() {
        String uuid = "";
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (attributes != null) {
            uuid = (String) attributes.getRequest().getAttribute("uuid");
        }
        return uuid;
    }

    public static String getUuidFromRequestAttribute(HttpServletRequest request) {
        return (String) request.getAttribute("uuid");
    }

    public static String getUuidFromRequest(HttpServletRequest request) {
        String uuid = request.getParameter("uuid");
        if (StringUtils.isBlank(uuid)) {
            JSONObject requestMap = new JSONObject();
            try {
                ServletInputStream inputStream = request.getInputStream();
                String requestStr = CharStreams.toString(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
                if (StringUtils.isNotBlank(requestStr)) {
                    requestMap = JSON.parseObject(requestStr);
                }
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
            }
            uuid = requestMap.get("uuid") == null ? "" : requestMap.get("uuid").toString();
        }
        return uuid;
    }

    public static String getBodyString(ServletRequest request) {
        StringBuilder sb = new StringBuilder();
        InputStream inputStream = null;
        BufferedReader reader = null;
        try {
            inputStream = request.getInputStream();
            reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
            String line = "";
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }
}
