package com.wy.admin.scm.core.framework.lang.exception;

import com.wy.admin.scm.core.framework.lang.enums.CommonReturnEnum;

import java.util.Map;

public class BusinessException extends AbstractException {
    private static final long serialVersionUID = 9073997622233296951L;

    private ReturnCode code = CommonReturnEnum.SYSTEM_ERROR;

    public BusinessException(ReturnCode code, String msg) {
        super(msg);
        this.code = code;
    }

    public BusinessException(ReturnCode code) {
        super(code.getMsg());
        this.code = code;
    }

    public BusinessException(ReturnCode code, Exception ex) {
        super(ex);
        this.code = code;
    }

    public BusinessException(ReturnCode code, String msg, Throwable e) {
        super(msg, e);
        this.code = code;
    }

    public BusinessException(ReturnCode code, Map<String, Object> resultMap) {
        super(code.getMsg(), resultMap);
    }

    @Override
    public int getCode() {
        return code.getCode();
    }
}