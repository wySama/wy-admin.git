package com.wy.admin.scm.core.framework.lang.enums;

import com.alibaba.fastjson.annotation.JSONCreator;
import com.alibaba.fastjson.annotation.JSONField;
import com.wy.admin.scm.core.framework.lang.enumeration.EnumFinder;
import com.wy.admin.scm.core.framework.lang.exception.ReturnCode;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;



public enum CommonReturnEnum implements ReturnCode {
    /**
     * 0
     */
    SUCCESS(0, "Success"),
    /**
     * 1
     */
    FAIL(1, "Failed"),
    /**
     * 200
     */
    HTTP_SUCCESS(200, SUCCESS.msg),
    /**
     * 201
     */
    HTTP_SUCCESS_WITH_MSG(201, SUCCESS.msg),
    /**
     * 404
     */
    HTTP_FAIL(404, FAIL.msg),
    /**
     * 403
     */
    AUTH_ILLEGAL_OPERATION(405, "Illegal Operation"),
    /**
     * 403
     */
    AUTH_SESSION_EXPIRED(406, "Session Expired"),
    /**
     * 500
     */
    SYSTEM_ERROR(500, "System Error"),
    /**
     * 501
     */
    REQUEST_METHOD_NOT_SUPPORTED(501, "Request method not supported"),
    /**
     * 502
     */
    NET_NETWORK_INTERRUPTION_OR_CONNECTION_TIMEOUT(502, "Network Interruption Or Connection Timeout"),
    /**
     * 503
     */
    NET_INTERFACE_CALL_FAILED(503, "Api Call Failed"),

    /**
     * 530
     */
    NET_SMS_SENDING_FAILED(530, "Sms Sending Failed"),
    /**
     * 531
     */
    NET_FILE_UPLOAD_FAILED(531, "File Upload Failed"),
    /**
     * 550
     */
    DATA_NO_RECORD_FOUND(550, "No Record Found"),
    /**
     * 551
     */
    DATA_RECORD_EXISTS(551, "Record Exists"),
    /**
     * 552
     */
    DATA_EXPIRED(552, "Data Expired"),
    /**
     * 553
     */
    DATA_ENCRYPTION_AND_DECRYPTION_FAILED(553, "Encryption And Decryption Failed"),
    /**
     * 554
     */
    DATA_ILLEGAL_FORMAT(554, "Illegal Data Format"),
    /**
     * 580
     */
    CHECK_ERROR(580, "Validation Failed");

    private int code = 0;

    private String msg;

    private CommonReturnEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private static final EnumFinder<Integer, CommonReturnEnum> BY_CODE_FINDER = new EnumFinder<Integer, CommonReturnEnum>() {
        @Override
        public Integer getKey(CommonReturnEnum enumObj) {
            return enumObj.code;
        }
    };

    /**
     * 返回指定code对应的Code枚举
     *
     * @param code code
     * @return 如找不到则返回null
     */
    @JsonCreator
    @JSONCreator
    public static CommonReturnEnum findByCode(int code) {
        return BY_CODE_FINDER.get(code);
    }

    @Override
    @JsonValue
    @JSONField
    public int getCode() {
        return code;
    }

    @Override
    public boolean isSuccess() {
        return this.code == 0;
    }

    @Override
    public String getMsg() {
        return this.msg;
    }
}