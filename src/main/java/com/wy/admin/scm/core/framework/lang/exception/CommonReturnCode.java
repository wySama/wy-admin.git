package com.wy.admin.scm.core.framework.lang.exception;


import com.alibaba.fastjson.annotation.JSONCreator;
import com.alibaba.fastjson.annotation.JSONField;
import com.wy.admin.scm.core.framework.lang.enumeration.EnumFinder;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum CommonReturnCode implements ReturnCode {
    SUCCESS(0, "Success"),
    FAIL(1, "Failed"),
    HTTP_SUCCESS(200, SUCCESS.msg),
    HTTP_SUCCESS_WITH_MSG(201, SUCCESS.msg),
    HTTP_FAIL(404, FAIL.msg),
    AUTH_ILLEGAL_OPERATION(405, "Illegal Operation"),
    AUTH_SESSION_EXPIRED(406, "Session Expired"),
    SYSTEM_ERROR(500, "System Error"),
    REQUEST_METHOD_NOT_SUPPORTED(501, "Request method not supported"),
    NET_NETWORK_INTERRUPTION_OR_CONNECTION_TIMEOUT(502, "Network Interruption Or Connection Timeout"),
    NET_INTERFACE_CALL_FAILED(503, "Api Call Failed"),
    NET_SMS_SENDING_FAILED(530, "Sms Sending Failed"),
    NET_FILE_UPLOAD_FAILED(531, "File Upload Failed"),
    DATA_NO_RECORD_FOUND(550, "No Record Found"),
    DATA_RECORD_EXISTS(551, "Record Exists"),
    DATA_EXPIRED(552, "Data Expired"),
    DATA_ENCRYPTION_AND_DECRYPTION_FAILED(553, "Encryption And Decryption Failed"),
    DATA_ILLEGAL_FORMAT(554, "Illegal Data Format"),
    CHECK_ERROR(580, "Validation Failed");

    private int code = 0;
    private String msg;
    private static final EnumFinder<Integer, CommonReturnCode> BY_CODE_FINDER = new EnumFinder<Integer, CommonReturnCode>() {
        public Integer getKey(CommonReturnCode enumObj) {
            return enumObj.code;
        }
    };

    private CommonReturnCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @JsonCreator
    @JSONCreator
    public static CommonReturnCode findByCode(int code) {
        return (CommonReturnCode) BY_CODE_FINDER.get(code);
    }

    @JsonValue
    @JSONField
    public int getCode() {
        return this.code;
    }

    public boolean isSuccess() {
        return this.code == 0;
    }

    public String getMsg() {
        return this.msg;
    }
}

