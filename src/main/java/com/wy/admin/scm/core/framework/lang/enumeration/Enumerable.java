package com.wy.admin.scm.core.framework.lang.enumeration;

public interface Enumerable<V> {
    V value();

    static <E extends Enumerable<?>> E findByValue(Class<E> enumCls, Object value) {
        return EnumFinder.findByValue(enumCls, value);
    }
}