package com.wy.admin.scm.core.framework.exception;

import com.alibaba.fastjson.JSONObject;
import com.wy.admin.scm.core.framework.lang.exception.BusinessException;
import com.wy.admin.scm.core.framework.model.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author wuyong
 * @version 1.0
 * @date 2020/10/21 10:21
 * @desc: 全局异常拦截 返回json数据响应
 */
@Slf4j
@RestControllerAdvice
public class ExceptionsHandler {

    /**
     * 运行时异常
     */
    @ExceptionHandler(RuntimeException.class)
    public JsonResult runtimeExceptionHandler(RuntimeException ex) {
        log.error("运行时异常：{}", ex.getMessage(), ex);
        return new JsonResult<>(500, "运行时异常", new JSONObject());
    }

    @ExceptionHandler(BusinessException.class)
    public JsonResult businessExceptionHandler(BusinessException exception) {
        log.error("公共异常抛出code:{}", exception.getCode());
        log.error("公共异常抛出message:{}", exception.getMessage());
        return new JsonResult<>(exception.getCode(), exception.getMsg(), new JSONObject());
    }


//    /** 空指针异常 */
//    @ExceptionHandler(NullPointerException.class)
//    public AjaxResult nullPointerExceptionHandler(NullPointerException ex) {
//        log.error("空指针异常：{} ", ex.getMessage(), ex);
//        return AjaxResult.error("空指针异常");
//    }
//
//    /** 类型转换异常 */
//    @ExceptionHandler(ClassCastException.class)
//    public AjaxResult classCastExceptionHandler(ClassCastException ex) {
//        log.error("类型转换异常：{} ", ex.getMessage(), ex);
//        return AjaxResult.error("类型转换异常");
//    }
//    /** 文件未找到异常 */
//    @ExceptionHandler(FileNotFoundException.class)
//    public AjaxResult FileNotFoundException(FileNotFoundException ex) {
//        log.error("文件未找到异常：{} ", ex.getMessage(), ex);
//        return AjaxResult.error("文件未找到异常");
//    }
//    /** 数字格式异常 */
//    @ExceptionHandler(NumberFormatException.class)
//    public AjaxResult NumberFormatException(NumberFormatException ex) {
//        log.error("数字格式异常：{} ", ex.getMessage(), ex);
//        return AjaxResult.error("数字格式异常");
//    }
//    /** 安全异常 */
//    @ExceptionHandler(SecurityException.class)
//    public AjaxResult SecurityException(SecurityException ex) {
//        log.error("安全异常：{} ", ex.getMessage(), ex);
//        return AjaxResult.error("安全异常");
//    }
//    /** sql异常 */
//    @ExceptionHandler(SQLException.class)
//    public AjaxResult SQLException(SQLException ex) {
//        log.error("sql异常：{} ", ex.getMessage(), ex);
//        return AjaxResult.error("sql异常");
//    }
//    /** 类型不存在异常 */
//    @ExceptionHandler(TypeNotPresentException.class)
//    public AjaxResult TypeNotPresentException(TypeNotPresentException ex) {
//        log.error("类型不存在异常：{} ", ex.getMessage(), ex);
//        return AjaxResult.error("类型不存在异常");
//    }
//
//    /** IO异常 */
//    @ExceptionHandler(IOException.class)
//    public AjaxResult iOExceptionHandler(IOException ex) {
//        log.error("IO异常：{} ", ex.getMessage(), ex);
//        return AjaxResult.error("IO异常");
//    }
//
//    /** 未知方法异常 */
//    @ExceptionHandler(NoSuchMethodException.class)
//    public AjaxResult noSuchMethodExceptionHandler(NoSuchMethodException ex) {
//        log.error("未知方法异常：{} ", ex.getMessage(), ex);
//        return AjaxResult.error("未知方法异常");
//    }
//
//    /** 数组越界异常 */
//    @ExceptionHandler(IndexOutOfBoundsException.class)
//    public AjaxResult indexOutOfBoundsExceptionHandler(IndexOutOfBoundsException ex) {
//        log.error("数组越界异常：{} ", ex.getMessage(), ex);
//        return AjaxResult.error("数组越界异常");
//    }
//    /** sql语法错误异常 */
//    @ExceptionHandler(BadSqlGrammarException.class)
//    public AjaxResult BadSqlGrammarException(BadSqlGrammarException ex) {
//        log.error("sql语法错误异常：{} ", ex.getMessage(), ex);
//        return AjaxResult.error("sql语法错误异常");
//    }
//
//    /** 无法注入bean异常 */
//    @ExceptionHandler(NoSuchBeanDefinitionException.class)
//    public AjaxResult NoSuchBeanDefinitionException(NoSuchBeanDefinitionException ex) {
//        log.error("无法注入bean异常 ：{} ", ex.getMessage(), ex);
//        return AjaxResult.error("无法注入bean");
//    }
//
//    /** Http消息不可读异常 */
//    @ExceptionHandler({HttpMessageNotReadableException.class})
//    public AjaxResult requestNotReadable(HttpMessageNotReadableException ex) {
//        log.error("400错误..requestNotReadable：{} ", ex.getMessage(), ex);
//        return AjaxResult.error("Http消息不可读");
//    }
//
//    /** 400错误 */
//    @ExceptionHandler({TypeMismatchException.class})
//    public AjaxResult requestTypeMismatch(TypeMismatchException ex) {
//        log.error("400错误..TypeMismatchException：{} ", ex.getMessage(), ex);
//        return AjaxResult.error("服务器异常");
//    }
//
//    /** 500错误 */
//    @ExceptionHandler({ConversionNotSupportedException.class, HttpMessageNotWritableException.class})
//    public AjaxResult server500(RuntimeException ex) {
//        log.error("500错误：{} ", ex.getMessage(), ex);
//        return AjaxResult.error("服务器异常");
//    }
//
//    /** 栈溢出 */
//    @ExceptionHandler({StackOverflowError.class})
//    public AjaxResult requestStackOverflow(StackOverflowError ex) {
//        log.error("栈溢出：{} ", ex.getMessage(), ex);
//        return AjaxResult.error("栈溢出异常");
//    }
//
//    /** 除数不能为0 */
//    @ExceptionHandler({ArithmeticException.class})
//    public AjaxResult arithmeticException(ArithmeticException ex) {
//        log.error("除数不能为0：{} ", ex.getMessage(), ex);
//        return AjaxResult.error("除数不能为0异常");
//    }
//
//    /** 其他错误 */
//    @ExceptionHandler({Exception.class})
//    public AjaxResult exception(Exception ex) {
//        log.error("其他错误：{} ", ex.getMessage(), ex);
//        return AjaxResult.error("网络连接失败，请退出后再试");
//    }
}

