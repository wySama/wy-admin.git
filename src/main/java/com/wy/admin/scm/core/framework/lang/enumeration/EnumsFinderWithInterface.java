package com.wy.admin.scm.core.framework.lang.enumeration;


import com.wy.admin.scm.core.framework.lang.util.ClassUtils;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 实现接口的枚举反向查找工具类
 */
public class EnumsFinderWithInterface {
    private static Map<Class<? extends Enumerable<?>>, Map<Object, ? extends Enumerable<?>>> rcMap = new ConcurrentHashMap<>();

    @SuppressWarnings("unchecked")
    public static <I extends Enumerable<?>> I findByValue(Class<I> enumInterfaceCls, Object value) {
        if (!enumInterfaceCls.isInterface()) {
            throw new IllegalArgumentException("enumInterfaceCls must be interface.");
        }
        if (!rcMap.containsKey(enumInterfaceCls)) {
            rcMap.put(enumInterfaceCls, new ConcurrentHashMap<>());
        }
        Map<Object, I> eMap = (Map<Object, I>) rcMap.get(enumInterfaceCls);
        if (!eMap.containsKey(value)) {
            Iterator<Class<? extends I>> it = ClassUtils.instance().getDescendantClasses(enumInterfaceCls, "com.genscript").stream().filter(Class::isEnum).iterator();
            while (it.hasNext()) {
                Class<? extends I> eCls = it.next();
                I[] es = eCls.getEnumConstants();
                for (I e : es) {
                    eMap.put(e.value(), e);
                }
            }
        }
        return eMap.get(value);
    }
}