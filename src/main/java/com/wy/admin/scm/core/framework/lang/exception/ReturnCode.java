package com.wy.admin.scm.core.framework.lang.exception;

import com.alibaba.fastjson.annotation.JSONCreator;
import com.wy.admin.scm.core.framework.lang.enumeration.Enumerable;
import com.wy.admin.scm.core.framework.lang.enumeration.EnumsFinderWithInterface;


public interface ReturnCode extends Enumerable<Integer> {
    @Override
    default Integer value() {
        return getCode();
    }

    boolean isSuccess();

    int getCode();

    String getMsg();

    @JSONCreator
    static ReturnCode findByCode(int value) {
        return EnumsFinderWithInterface.findByValue(ReturnCode.class, value);
    }
}