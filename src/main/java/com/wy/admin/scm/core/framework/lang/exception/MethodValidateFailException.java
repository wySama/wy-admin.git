package com.wy.admin.scm.core.framework.lang.exception;

import com.wy.admin.scm.core.framework.lang.enums.CommonReturnEnum;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;

import java.util.Collection;

/**
 * 方法验证失败异常
 */
public class MethodValidateFailException extends BusinessException {
    private static final long serialVersionUID = -7334864986176524592L;

    private Collection<FieldError> es;

    private BindException e;

    private String field;

    public MethodValidateFailException(BindException e) {
        super(CommonReturnEnum.CHECK_ERROR);
        this.e = e;
        this.es = e.getFieldErrors();
    }

    public MethodValidateFailException(Collection<FieldError> fieldErrors) {
        super(CommonReturnEnum.CHECK_ERROR);
        this.es = fieldErrors;
    }

    public MethodValidateFailException(String field, String msg) {
        super(CommonReturnEnum.CHECK_ERROR, msg);
        this.field = field;
    }

    public String get1stFieldName() {
        if (StringUtils.isNotBlank(field)) {
            return field;
        }
        return es.stream().findFirst().get().getField();
    }

    @Override
    public String getMessage() {
        if (StringUtils.isNotBlank(field)) {
            return String.format("field: [%s], msg: [%s]", field, super.getMessage());
        }
        if (CollectionUtils.isNotEmpty(es)) {
            FieldError fe = es.stream().findFirst().get();
            return fe.isBindingFailure() ?
                    String.format("invalid value(%s) for field[%s]", fe.getRejectedValue(), fe.getField()) : String.format("field: [%s], msg: [%s]", fe.getField(), fe.getDefaultMessage());
        }
        if (e != null) {
            if (e.getFieldError() != null) {
                return String.format("invalid value(%s) for field[%s]", e.getFieldError().getRejectedValue(), e.getFieldError().getField());
            } else {
                return String.format("field: [%s], msg: [%s]", field, e.getLocalizedMessage());
            }
        }
        return "invalid value for arguments";
    }
}