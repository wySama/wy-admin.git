package com.wy.admin.scm.core.framework.lang.exception;

import java.util.Map;
import java.util.TreeMap;

public abstract class AbstractException extends RuntimeException {
    private static final long serialVersionUID = 8898526093410545677L;

    private Map<String, Object> resultMap;

    public AbstractException(String msg) {
        super(msg);
    }

    public AbstractException(Exception ex) {
        super(ex);
    }

    public AbstractException(String msg, Throwable e) {
        super(msg, e);
    }

    public AbstractException(String msg, Map<String, Object> resultMap) {
        super(msg);
        this.resultMap = resultMap;
    }

    public AbstractException(String msg, Throwable e, Map<String, Object> resultMap) {
        super(msg, e);
        this.resultMap = resultMap;
    }

    public String getMsg() {
        return getMessage();
    }

    public abstract int getCode();

    public Map<String, ?> getResultMap() {
        return resultMap;
    }

    public AbstractException addResult(String key, Object value) {
        if (this.resultMap == null) {
            resultMap = new TreeMap<>();
        }
        resultMap.put(key, value);
        return this;
    }
}