package com.wy.admin.scm.core.framework.shiro.service;


import com.wy.admin.scm.core.framework.constant.ShiroConstants;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.shiro.cache.Cache;
import org.crazycake.shiro.RedisCacheManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Deque;

@Service
@Slf4j
public class SysUserOnlineService {


    @Autowired
    private RedisCacheManager cacheManager;


    /**
     * 清理用户缓存
     *
     * @param loginName 登录名称
     * @param sessionId 会话ID
     */

    public void removeUserCache(String loginName, String sessionId) {
        Cache<String, Deque<Serializable>> cache = cacheManager.getCache(ShiroConstants.SYS_USERCACHE);
        Deque<Serializable> deque = cache.get(loginName);
        if (CollectionUtils.isEmpty(deque)) {
            return;
        }
        deque.remove(sessionId);
        cache.put(loginName, deque);
    }

}
