package com.wy.admin.scm.core.framework.init;


import com.wy.admin.scm.business.tool.service.impl.CommonDictDataServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
public class DictDataCommandLineRunner implements CommandLineRunner {

    @Autowired
    private CommonDictDataServiceImpl commonDictDataService;

    @Override
    public void run(String... args) throws Exception {
        commonDictDataService.getDictDataMap("ALARM_CONTENT");
    }
}
