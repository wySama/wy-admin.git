package com.wy.admin.scm.core.framework.filter;

import com.wy.admin.scm.business.system.model.SysUser;
import com.wy.admin.scm.core.framework.constant.ShiroConstants;
import com.wy.admin.scm.core.framework.util.ShiroUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.DefaultSessionKey;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.apache.shiro.web.util.WebUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.Serializable;
import java.util.ArrayDeque;
import java.util.Deque;

@Slf4j
public class KickoutSessionFilter extends AccessControlFilter {
    /**
     * 同一个用户最大会话数
     **/
    private int maxSession = 1;
    private Cache<String, Deque<Serializable>> cache;
    private SessionManager sessionManager;
    /**
     * 踢出后到的地址
     **/
    private String kickoutUrl;

    /**
     * 踢出之前登录的/之后登录的用户 默认踢出之前登录的用户
     */
    private boolean kickoutAfter = false;

    @Override
    protected boolean isAccessAllowed(ServletRequest servletRequest, ServletResponse servletResponse, Object o) throws Exception {
        return false;
    }

    @Override
    protected boolean onAccessDenied(ServletRequest servletRequest, ServletResponse servletResponse) throws Exception {
        Subject subject = getSubject(servletRequest, servletResponse);
        //如果没有登录，直接进行登录的流程
        if (!subject.isAuthenticated() && !subject.isRemembered() || maxSession == -1) {
            return true;
        }
        Session session = subject.getSession();
        //获取当前用户的SessionID
        Serializable sessionId = session.getId();

        // 当前登录用户
        SysUser user = ShiroUtils.getSysUser();
        String loginName = user.getLoginName();

        Deque<Serializable> deque = cache.get(loginName);
        //第一次登录则队列中为空，把自己存进去
        if (deque == null) {
            // 初始化队列
            deque = new ArrayDeque<>();
        }

        // 如果队列里没有此sessionId，且用户没有被踢出；放入队列
        if (!deque.contains(sessionId) && session.getAttribute("kickout") == null) {
            // 将sessionId存入队列
            deque.push(sessionId);
            // 将用户的sessionId队列缓存
            cache.put(loginName, deque);
        }


        //如果队列里的sessionId数超出最大会话数，开始踢人
        while (deque.size() > maxSession) {
            Serializable kickoutSessionId;
            if (kickoutAfter) { //如果踢出后者
                kickoutSessionId = deque.removeFirst();
            } else { //否则踢出前者
                kickoutSessionId = deque.removeLast();
            }
            cache.put(loginName, deque);
            try {
                Session kickoutSession = sessionManager.getSession(new DefaultSessionKey(kickoutSessionId));
                if (kickoutSession != null) {
                    //设置会话的kickout属性表示踢出了
                    kickoutSession.setAttribute("kickout", true);
                }
            } catch (Exception e) {
                log.error("踢出异常未踢出",e);
            }
        }


        //如果被踢出了，直接退出，重定向到踢出后的地址
        if (session.getAttribute("kickout") != null) {
            //会话被踢出了
            subject.logout();
            //重定向退出到登入页面
            servletRequest.setAttribute("kickMsg", "该账号异地登录，您被强制下线");
            WebUtils.issueRedirect(servletRequest, servletResponse, kickoutUrl);
            return false;
        }
        return true;
    }


    public void setSessionManager(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }


    // 设置Cache的key的前缀
    public void setCacheManager(CacheManager cacheManager) {
        // 必须和ehcache缓存配置中的缓存name一致
        this.cache = cacheManager.getCache(ShiroConstants.SYS_USERCACHE);
    }

    public void setKickoutAfter(boolean kickoutAfter) {
        this.kickoutAfter = kickoutAfter;
    }


    public void setMaxSession(int maxSession) {
        this.maxSession = maxSession;
    }

    public void setKickoutUrl(String kickoutUrl) {
        this.kickoutUrl = kickoutUrl;
    }
}
