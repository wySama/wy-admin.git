package com.wy.admin.scm.core.framework.shiro.service;


import com.wy.admin.scm.business.common.base.constant.NumberConstant;
import com.wy.admin.scm.business.common.base.constant.StringConstant;
import com.wy.admin.scm.business.common.base.constant.TipsConstant;
import com.wy.admin.scm.business.common.util.CommonUtils;
import com.wy.admin.scm.business.common.util.ServletUtils;
import com.wy.admin.scm.business.system.model.SysUser;
import com.wy.admin.scm.business.system.service.impl.SysUserServiceImpl;
import com.wy.admin.scm.core.framework.constant.ShiroConstants;
import com.wy.admin.scm.core.framework.lang.enums.CommonReturnEnum;
import com.wy.admin.scm.core.framework.lang.exception.BusinessException;
import com.wy.admin.scm.core.framework.manager.AsyncManager;
import com.wy.admin.scm.core.framework.manager.factory.AsyncFactory;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class LoginService {


    @Autowired
    private SysUserServiceImpl sysUserService;

    /**
     * 登录
     */
    public SysUser login(String username, String password) {
        // 验证码校验
        if (ShiroConstants.CAPTCHA_ERROR.equals(ServletUtils.getRequest().getAttribute(ShiroConstants.CURRENT_CAPTCHA))) {
            AsyncManager.me().execute(AsyncFactory.recordSysLoginLog(username, NumberConstant.ZERO, TipsConstant.USER_JCAPTCHA_ERROR));
            throw new BusinessException(CommonReturnEnum.CHECK_ERROR, TipsConstant.USER_JCAPTCHA_ERROR);
        }
        // 用户名或密码为空 错误
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
            AsyncManager.me().execute(AsyncFactory.recordSysLoginLog(username, NumberConstant.ZERO, TipsConstant.USRE_NAME_AND_PASSWARD_IS_NOT_NULL));
            throw new BusinessException(CommonReturnEnum.CHECK_ERROR, TipsConstant.USRE_NAME_AND_PASSWARD_IS_NOT_NULL);
        }

        SysUser user = this.sysUserService.selectByLoginName(username);
        //检验密码
        if (user == null || !user.getPwd().equals(CommonUtils.password(password))) {
            AsyncManager.me().execute(AsyncFactory.recordSysLoginLog(username, NumberConstant.ZERO, TipsConstant.USRE_NAME_OR_PASSWARD_ERROR));
            throw new BusinessException(CommonReturnEnum.CHECK_ERROR, TipsConstant.USRE_NAME_OR_PASSWARD_ERROR);
        }

        AsyncManager.me().execute(AsyncFactory.recordSysLoginLog(username, NumberConstant.ONE, StringConstant.LOGIN_SUCCESS));
        return user;
    }

    public void userLogin(String username, String password) {
        // 验证码校验
        if (ShiroConstants.CAPTCHA_ERROR.equals(ServletUtils.getRequest().getAttribute(ShiroConstants.CURRENT_CAPTCHA))) {
            throw new BusinessException(CommonReturnEnum.CHECK_ERROR, TipsConstant.USER_JCAPTCHA_ERROR);
        }
        // 用户名或密码为空 错误
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
            throw new BusinessException(CommonReturnEnum.CHECK_ERROR, TipsConstant.USRE_NAME_AND_PASSWARD_IS_NOT_NULL);
        }

        //用户认证信息
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(username, password);
        try {
            //进行验证，这里可以捕获异常，然后返回对应信息
            subject.login(usernamePasswordToken);
        } catch (UnknownAccountException e) {
            log.error("用户名不存在！", e);
            throw new BusinessException(CommonReturnEnum.CHECK_ERROR, TipsConstant.SYSUSER_NOT_EXIT);
        } catch (AuthenticationException e) {
            log.error("账号或密码错误！", e);
            throw new BusinessException(CommonReturnEnum.CHECK_ERROR, TipsConstant.USRE_NAME_OR_PASSWARD_ERROR);

        } catch (AuthorizationException e) {
            log.error("没有权限！", e);
            throw new BusinessException(CommonReturnEnum.CHECK_ERROR, TipsConstant.NO_PERMISSION);
        }
    }


}
