package com.wy.admin.scm.core.framework.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
@Builder
@ApiModel(value = "返回结果模型")
public class JsonResult<T> {
    @ApiModelProperty(value = "响应码200成功,内部服务器错误500,token无效401,403 无权限访问")
    private Integer code;

    @ApiModelProperty(value = "返回说明信息")
    private String msg;

    @ApiModelProperty(value = "返回结果集")
    private T data;


    public JsonResult() {
        this.code = 200;
        this.msg = "success";
    }

    public JsonResult(T data) {
        this.code = 200;
        this.msg = "success";
        this.data = data;
    }


    public JsonResult(T data, String msg) {
        this.code = 200;
        this.msg = msg;
        this.data = data;
    }

    public JsonResult(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public JsonResult(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public JsonResult(Integer type, T msg) {
        if (type == 0) {
            this.code = 0;
            this.msg = "success";
            this.data = msg;
        }

    }


    public Object error(HttpStatus httpStatus, String msg) {
        this.code = httpStatus.value();
        this.msg = msg;
        return this;
    }

    public static boolean success(JsonResult jsonResult) {
        boolean flag = false;
        if (null != jsonResult && 200 == jsonResult.getCode()) {
            flag = true;
        }
        return flag;
    }

}
