package com.wy.admin.scm.core.framework.lang.response;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.wy.admin.scm.core.framework.lang.enums.CommonReturnEnum;
import com.wy.admin.scm.core.framework.lang.exception.AbstractException;
import com.wy.admin.scm.core.framework.lang.exception.MethodValidateFailException;
import com.wy.admin.scm.core.framework.lang.exception.ReturnCode;
import com.wy.admin.scm.core.framework.lang.util.HttpServletUtils;
import org.apache.commons.collections4.MapUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;

import java.io.Serializable;
import java.util.*;


public class ResponseUtils extends AbstractResponseUtils {
    public static Map<String, Object> buildRespMap() {
        return buildRespMap(CommonReturnEnum.SUCCESS);
    }

    public static Map<String, Object> buildRespMap(String key, Object value) {
        return buildRespMap(CommonReturnEnum.SUCCESS, key, value);
    }

    public static Map<String, Object> buildRespMap(ReturnCode returnCode, String key, Object value) {
        Map<String, Object> map = new TreeMap<>();
        map.put(KEY_RETURN_CODE, returnCode.getCode());
        map.put(KEY_MSG, returnCode.getMsg());
        map.put(KEY_RESULT, buildResult(key, value));
        return reBuildRespMap(map);
    }

    public static Map<String, Object> buildRespMap(ReturnCode returnCode, String key, Collection<?> objects) {
        Map<String, Object> map = new TreeMap<>();
        map.put(KEY_RETURN_CODE, returnCode.getCode());
        map.put(KEY_MSG, returnCode.getMsg());
        map.put(KEY_RESULT, buildResult(key, objects));
        return reBuildRespMap(map);
    }

    public static Map<String, Object> buildRespMap(AbstractException e, String key, Collection<?> objects) {
        Map<String, Object> map = new TreeMap<>();
        if (e instanceof MethodValidateFailException) {
            map.put("field", ((MethodValidateFailException) e).get1stFieldName());
        }
        map.put(KEY_RETURN_CODE, e.getCode());
        map.put(KEY_MSG, e.getMsg());
        Map<String, Object> resultMap = buildResult(key, objects);
        map.put(KEY_RESULT, handlerForException(resultMap, e));
        return reBuildRespMap(map);
    }

    public static Map<String, Object> buildRespMapForMethodValidateFailException(MethodValidateFailException e) {
        Map<String, Object> map = new TreeMap<>();
        map.put(KEY_RETURN_CODE, e.getCode());
        map.put(KEY_MSG, e.getMessage());
        map.put("field", e.get1stFieldName());
        return reBuildRespMap(map);
    }

    public static Map<String, Object> buildRespMap(AbstractException e, Map<String, Object> objectMap) {
        if (MapUtils.isEmpty(objectMap)) {
            objectMap = new TreeMap<>();
        }
        Map<String, Object> map = new TreeMap<>();
        if (e instanceof MethodValidateFailException) {
            map.put("field", ((MethodValidateFailException) e).get1stFieldName());
        }
        map.put(KEY_RETURN_CODE, e.getCode());
        map.put(KEY_MSG, e.getMsg());
        map.put(KEY_RESULT, handlerForException(objectMap, e));
        return reBuildRespMap(map);
    }

    public static Map<String, Object> buildRespMap(Enum<? extends ReturnCode> returnCode) {
        ReturnCode code = (ReturnCode) returnCode;
        Map<String, Object> map = new TreeMap<>();
        map.put(KEY_RETURN_CODE, code.getCode());
        map.put(KEY_MSG, code.getMsg());
        return reBuildRespMap(map);
    }

    public static Map<String, Object> buildRespMap(Errors errors) {
        FieldError fieldError = errors.getFieldError();
        String msg = fieldError == null ? CommonReturnEnum.CHECK_ERROR.getMsg() : fieldError.getField() + fieldError.getDefaultMessage();

        Map<String, Object> map = new TreeMap<>();
        map.put(KEY_RETURN_CODE, CommonReturnEnum.CHECK_ERROR.getCode());
        map.put(KEY_MSG, msg);
        return reBuildRespMap(map);
    }

    public static Map<String, Object> buildRespMap(Serializable bean) {
        return buildRespMap(CommonReturnEnum.SUCCESS, bean);
    }

    public static Map<String, Object> buildRespMap(ReturnCode returnCode, String key, boolean result) {
        Map<String, Object> map = new TreeMap<>();
        map.put(KEY_RETURN_CODE, returnCode.getCode());
        map.put(KEY_MSG, returnCode.getMsg());
        map.put(KEY_RESULT, buildResult(key, result));
        return reBuildRespMap(map);
    }

    public static Map<String, Object> buildRespMap(ReturnCode returnCode, Serializable bean) {
        Map<String, Object> map = new TreeMap<>();
        map.put(KEY_RETURN_CODE, returnCode.getCode());
        map.put(KEY_MSG, returnCode.getMsg());
        map.put(KEY_RESULT, bean);
        return reBuildRespMap(map);
    }

    public static Map<String, Object> buildRespMap(AbstractException e, Serializable bean) throws Exception {
        Map<String, Object> map = new TreeMap<>();
        if (e instanceof MethodValidateFailException) {
            map.put("field", ((MethodValidateFailException) e).get1stFieldName());
        }
        map.put(KEY_RETURN_CODE, e.getCode());
        map.put(KEY_MSG, e.getMsg());
        Map<String, Object> returnMap = JSON.parseObject(JSON.toJSONString(bean), new TypeReference<Map<String, Object>>() {
        });
        map.put(KEY_RESULT, handlerForException(returnMap, e));
        return reBuildRespMap(map);
    }

    public static Map<String, Object> buildRespMap(AbstractException e) {
        Map<String, Object> map = new TreeMap<>();
        if (e instanceof MethodValidateFailException) {
            map.put("field", ((MethodValidateFailException) e).get1stFieldName());
        }
        map.put(KEY_RETURN_CODE, e.getCode());
        map.put(KEY_MSG, e.getMsg());
        map.put(KEY_RESULT, handlerForException(e));

        /*StringWriter stringWriter = new StringWriter();
        e.printStackTrace(new PrintWriter(stringWriter));
        map.put(KEY_EXCEPTION, stringWriter.toString());*/

        return reBuildRespMap(map);
    }

    private static Map<String, Object> handlerForException(AbstractException e) {
        Map<String, Object> map = new TreeMap<>();
        if (MapUtils.isNotEmpty(e.getResultMap())) {
            map.putAll(e.getResultMap());
        }
        return map;
    }

    private static Map<String, Object> handlerForException(Map<String, Object> map, AbstractException e) {
        if (map == null) {
            map = new TreeMap<>();
        }
        if (MapUtils.isNotEmpty(e.getResultMap())) {
            map.putAll(e.getResultMap());
        }
        return map;
    }

    public static Map<String, Object> buildRespMap(Map<String, ?> objectMap) {
        Map<String, Object> map = new TreeMap<>();
        if (MapUtils.isEmpty(objectMap)) {
            objectMap = new TreeMap<>();
        }
        map.put(KEY_RETURN_CODE, CommonReturnEnum.SUCCESS.getCode());
        map.put(KEY_MSG, CommonReturnEnum.SUCCESS.getMsg());
        map.put(KEY_RESULT, objectMap);
        return reBuildRespMap(map);
    }

    public static Map<String, Object> buildRespMap(ReturnCode returnCode, Map<String, ?> objectMap) {
        Map<String, Object> map = new TreeMap<>();
        if (MapUtils.isEmpty(objectMap)) {
            objectMap = new TreeMap<>();
        }
        map.put(KEY_RETURN_CODE, returnCode.getCode());
        map.put(KEY_MSG, returnCode.getMsg());
        map.put(KEY_RESULT, objectMap);
        return reBuildRespMap(map);
    }

    public static Map<String, Object> buildRespMap(ReturnCode returnCode, Collection<?> beans) {
        Map<String, Object> map = new TreeMap<>();
        map.put(KEY_RETURN_CODE, returnCode.getCode());
        map.put(KEY_MSG, returnCode.getMsg());
        map.put(KEY_RESULT, beans);
        return reBuildRespMap(map);
    }

    public static Map<String, Object> buildRespMap(ResponseBean response) {
        Map<String, Object> map = new TreeMap<>();
        map.put(KEY_RETURN_CODE, response.getReturnCode().getCode());
        if (response.getMsg() != null && !"".equals(response.getMsg())) {
            map.put(KEY_MSG, response.getMsg());
        } else {
            map.put(KEY_MSG, response.getReturnCode().getMsg());
        }
        map.put(KEY_RESULT, response.getResult());
        return reBuildRespMap(map);
    }

    public static Map<String, Object> reBuildRespMap(Map<String, Object> map) {
        map.put(KEY_UUID, HttpServletUtils.getUuidFromRequestAttribute());
        List<Integer> successCodeArr = Arrays.asList(
                CommonReturnEnum.SUCCESS.getCode(),
                CommonReturnEnum.HTTP_SUCCESS.getCode(),
                CommonReturnEnum.HTTP_SUCCESS_WITH_MSG.getCode());
        Integer returnCode = (Integer) map.get(KEY_RETURN_CODE);
        if (successCodeArr.contains(returnCode)) {
            map.put(KEY_STATUS, "success");
        } else {
            map.put(KEY_STATUS, "failed");
        }
        return map;
    }

    public static class ResponseBean implements Serializable {

        private CommonReturnEnum returnCode;

        private String msg;

        private Object result;

        public ResponseBean(Object result) {
            this.returnCode = CommonReturnEnum.SUCCESS;
            this.msg = this.returnCode.getMsg();
            this.result = result;
        }

        public ResponseBean(CommonReturnEnum returnCode, Object result) {
            this.returnCode = returnCode;
            this.msg = returnCode.getMsg();
            this.result = result;
        }

        public ResponseBean(CommonReturnEnum returnCode, String msg, Object result) {
            this.returnCode = returnCode;
            this.msg = msg;
            this.result = result;
        }

        public CommonReturnEnum getReturnCode() {
            return returnCode;
        }

        public void setReturnCode(CommonReturnEnum returnCode) {
            this.returnCode = returnCode;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public Object getResult() {
            return result;
        }

        public void setResult(Object result) {
            this.result = result;
        }
    }
}