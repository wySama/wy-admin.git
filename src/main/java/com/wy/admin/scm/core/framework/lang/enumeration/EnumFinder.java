package com.wy.admin.scm.core.framework.lang.enumeration;




import com.wy.admin.scm.core.framework.lang.util.KanfitUtil;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 反向查找枚举工具类
 */
public abstract class EnumFinder<K, T extends Enum<T>> {
    protected Map<K, T> map = new HashMap<>();

    @SuppressWarnings("unchecked")
    public EnumFinder() {
        EnumSet.allOf((Class<T>) KanfitUtil.instance().getGenericCls(this, 2, EnumFinder.class)).forEach(e -> map.put(getKey(e), e));
    }

    public T get(K key) {
        return get(key, null);
    }

    public T get(K key, T defaultValue) {
        return map.getOrDefault(key, defaultValue);
    }

    public abstract K getKey(T enumObj);

    private static Map<Class<? extends Enumerable<?>>, Map<? extends Object, ? extends Enumerable<?>>> valEnumMap = new ConcurrentHashMap<>();

    /**
     * 反向查找枚举对象必须实现Enumerable<V>接口
     *
     * @param enumCls enumCls
     * @param value   value
     * @return enum
     */
    public static <E extends Enumerable<?>> E findByValue(Class<E> enumCls, Object value) {
        if (value == null) {
            return null;
        }

        if (!valEnumMap.containsKey(enumCls)) {
            valEnumMap.put(enumCls, new ConcurrentHashMap<Object, E>());
        }

        @SuppressWarnings("unchecked")
        Map<Object, E> eMap = (Map<Object, E>) valEnumMap.get(enumCls);
        if (!eMap.containsKey(value)) {
            E[] es = enumCls.getEnumConstants();
            for (E e : es) {
                eMap.put(e.value(), e);
            }
        }
        return eMap.get(value);
    }
}