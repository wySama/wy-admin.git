package com.wy.admin.scm.core.framework.manager.factory;


import com.wy.admin.scm.business.common.util.ServletUtils;
import com.wy.admin.scm.business.log.model.SysLoginLog;
import com.wy.admin.scm.business.log.model.SysOperLog;
import com.wy.admin.scm.business.log.service.impl.SysLoginLogServiceImpl;
import com.wy.admin.scm.business.log.service.impl.SysOperLogServiceImpl;
import com.wy.admin.scm.core.framework.util.ShiroUtils;
import com.wy.admin.scm.core.framework.util.spring.SpringUtils;
import eu.bitwalker.useragentutils.UserAgent;
import lombok.extern.slf4j.Slf4j;

import java.util.TimerTask;

/**
 * 异步工厂（产生任务用）
 *
 * @author liuhulu
 */
@Slf4j
public class AsyncFactory {

//    /**
//     * 同步session到数据库
//     *
//     * @param session 在线用户会话
//     * @return 任务task
//     */
//    public static TimerTask syncSessionToDb(final OnlineSession session)
//    {
//        return new TimerTask()
//        {
//            @Override
//            public void run()
//            {
//                SysUserOnline online = new SysUserOnline();
//                online.setSessionId(String.valueOf(session.getId()));
//                online.setDeptName(session.getDeptName());
//                online.setLoginName(session.getLoginName());
//                online.setStartTimestamp(session.getStartTimestamp());
//                online.setLastAccessTime(session.getLastAccessTime());
//                online.setExpireTime(session.getTimeout());
//                online.setIpaddr(session.getHost());
//                online.setLoginLocation(AddressUtils.getRealAddressByIP(session.getHost()));
//                online.setBrowser(session.getBrowser());
//                online.setOs(session.getOs());
//                online.setStatus(session.getStatus());
//                SpringUtils.getBean(ISysUserOnlineService.class).saveOnline(online);
//
//            }
//        };
//    }
//

    /**
     * 操作日志记录
     *
     * @param operLog 操作日志信息
     * @return 任务task
     */
    public static TimerTask recordOper(final SysOperLog operLog) {
        return new TimerTask() {
            @Override
            public void run() {
                SpringUtils.getBean(SysOperLogServiceImpl.class).save(operLog);
            }
        };
    }

    /**
     * 记录登录信息
     *
     * @param username 用户名
     * @param status   状态
     * @param message  消息
     * @return 任务task
     */
    public static TimerTask recordSysLoginLog(final String username, final Integer status, final String message) {
        final UserAgent userAgent = UserAgent.parseUserAgentString(ServletUtils.getRequest().getHeader("User-Agent"));
        final String ip = ShiroUtils.getIp();
        return new TimerTask() {
            @Override
            public void run() {
                // 获取客户端操作系统
                String os = userAgent.getOperatingSystem().getName();
                // 获取客户端浏览器
                String browser = userAgent.getBrowser().getName();
                // 封装对象
                SysLoginLog sysLoginLog = new SysLoginLog();
                sysLoginLog.setLoginName(username);
                sysLoginLog.setLoginIp(ip);
                sysLoginLog.setLoginLocation("内网IP");
                sysLoginLog.setBrowser(browser);
                sysLoginLog.setOs(os);
                sysLoginLog.setLoginStatus(status);
                sysLoginLog.setMsg(message);

                // 插入数据
                SpringUtils.getBean(SysLoginLogServiceImpl.class).save(sysLoginLog);
            }
        };
    }
}
